<?php
include('system_load.php');
//This loads system.

//user Authentication.
authenticate_user($dBlink,'subscriber');
//creating company object.
$new_company = new Company;
//new account object/
$new_account = new Account;
//new Journal Voucher.
$new_jv = new Jvs;

if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
    $_SESSION['company_name'] = $new_company->company_name($dBlink ,$_SESSION['company_id']);
} else {
    HEADER('LOCATION: company.php?message=Please select a company.');
}
if(isset($_POST['delete_jv']) && $_POST['delete_jv'] != '') {
    $message = $new_jv->delete_jvs($dBlink ,$_POST['delete_jv']);
}
//getting messages from URL if any.
if(isset($_GET['message']) && !isset($message)) {
    $message = $_GET['message'];
}

$page_title = $_SESSION['company_name']; //You can edit this to change your page title.
require_once("includes/header.php"); //including header file.
?>
    <div class="admin_wrap">
        <?php require_once('includes/sidebar.php'); ?>
        <div class="alignleft rightcontent">
            <?php
            //display message if exist.
            if(isset($message) && $message != '') {
                echo '<div class="alert-box">';
                echo $message;
                echo '</div>';
            }
            ?>
            <h2 class="alignleft"> Cash Receipt Vouchers</h2>
            <a href="manage_crv.php" class="alignleft addnew">Add New</a>
            <div class="clear"></div><!--clear float-->
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="wc_table" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Manual ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Posted By</th>
                    <th>Amount</th>
                    <th>View</th>
                    <?php if(partial_access($dBlink,'admin')) { ?>
                        <th>Delete</th><?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php $new_jv->list_crv($dBlink ); ?>
                </tbody>
            </table>
        </div>
        <div class="clear"></div><!--clear Float-->
    </div><!--admin wrap ends here.-->

<?php
require_once("includes/footer.php");
?>