<?php
	include('system_load.php');
	//This loads system.
	
	if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') { 
		HEADER('LOCATION: dashboard.php');
	} //If user is loged in redirect to specific page.

	$page_title = "Registeration Page!"; //You can edit this to change your page title.
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<style type="text/css" title="currentStyle">
	@import "css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#wc_table').dataTable();
	} );
	function confirm_delete() { 
		var del = confirm('Do you really want to delete this record?');
		if(del == true) { 
			return true;
		} else { 
			return false;
		}
	}//delete_confirmation ends here.
</script>
</head>
<body>
    
    <?php
		if(isset($message) && $message != '') { 
			echo '<div class="alert-box">';
			echo $message;
			echo '</div>';
		}
		if(isset($_GET['message']) && $_GET['message'] != '') { 
			echo '<div class="alert-box">';
			echo $_GET['message'];
			echo '</div>';
		}
	?>
    	<!-- you can copy following form in your registration page!-->
    	<div class="form_wrapper">
        	<h1><?php echo $page_title; ?></h1>
            <form action="register-user.php" id="register_form" name="register" method="post">
            <table cellpadding="10" width="100%" cellspacing="0" border="0">
                <tr>
                    <td><input type="text" name="first_name" placeholder="First Name*" required="required" /></td>
                </tr>
                <tr>
                    <td><input type="text" name="last_name" placeholder="Last Name" /></td>
                </tr>
            	<tr>
                    <td><input type="text" name="email" placeholder="Email*" required="required"/></td>
                </tr>
                <tr>
                    <td><input type="password" name="password" placeholder="Password*" required="required"/></td>
                </tr>
             <input type="hidden" value="1" name="add_user" />
                <tr>
                    <td><input type="checkbox" name="privacy_policy" value="1" required="required" /> You agree with our Privacy Policy</td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
   
            </table>
            </form>
            <script>
				$(document).ready(function() {
					// validate the register form
//					$("#register_form").validate();
				});
            </script>

        </div><!--//form_wrapper-->
        <div class="aligncenter">
        	Already have an account? <a href="login.php">Sign in</a>
        </div>
</div><!--//wc_wrapper--> 
</body>
</html>