<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user($dBlink,'subscriber');
	
	$new_company = new Company;
	//new account object/
	$new_account = new Account;
	
	if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
		$_SESSION['company_name'] = $new_company->company_name($dBlink ,$_SESSION['company_id']);
	} else { 
		HEADER('LOCATION: company.php?message=Please select a company.');
	}//check if company is selected.
	
//add account processing.
	if(isset($_POST['add_account']) && $_POST['add_account'] == '1') { 
		extract($_POST);
		if($account_title == '') { 
			$message = 'Account title is required!';
		} else if($account_number == '') { 
			$message = 'Account Number is required!';
		} else if($account_type == '') { 
			$message = 'Account Type is required.';
		}  else {
			extract($_POST);
		$message = $new_account->add_account($dBlink , $account_number, $account_title, $account_type, $memo,$cnic,$contact,$address);
		}
	}//add Company processing ends here.
	
//Account update submiss.
if(isset($_POST['edit_account']) && $_POST['edit_account'] != '') { 
	if(isset($_POST['update_account']) && $_POST['update_account'] == '1') {
	extract($_POST);
	if($account_title == '') { 
			$message = 'Account title is required!';
		} else if($account_number == '') { 
			$message = 'Account Number is required!';
		} else if($account_type == '') { 
			$message = 'Account Type is required.';
		}  else {
		$message = $new_account->update_account($dBlink ,$edit_account, $account_number, $account_title, $account_type, $memo,$cnic,$contact,$address);
		}
	}
}//update Account ends here.
	
	if(isset($_POST['edit_account']) && $_POST['edit_account'] != '') { 
		$new_account->set_account($dBlink ,$_POST['edit_account']);
	}//setting company data if editing.
	
	$page_title = $_SESSION['company_name']; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft"><?php if(isset($_POST['edit_account'])){ echo 'Edit Account'; } else { echo 'Add New Account';} ?></h2>
                	<div class="clear"></div><!--clear float-->
                    <form action="<?php $_SERVER['PHP_SELF']?>" id="add_account" name="user" method="post">
                    <table cellpadding="10" cellspacing="0" width="100%" border="0">
                    	<tr>
                        	<td width="150">Account Number*</td>
                            <td><input type="text" name="account_number" placeholder="Account Number" value="<?php echo $new_account->account_number; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Account Title*</td>
                            <td><input type="text" name="account_title" placeholder="Account Title" value="<?php echo $new_account->account_title; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Account Type*</td>
                            <td>
                            	<select required="required" name="account_type">
                                	<option value="">Account Type</option>
                                    <option <?php if($new_account->account_type == 'Asset'){echo 'selected="selected"';} ?> value="Asset">Asset</option>
                                    <option <?php if($new_account->account_type == 'Liability'){echo 'selected="selected"';} ?> value="Liability">Liability</option>
                                    <option <?php if($new_account->account_type == 'Revenue'){echo 'selected="selected"';} ?> value="Revenue">Revenue</option>
                                    <option <?php if($new_account->account_type == 'Expense'){echo 'selected="selected"';} ?> value="Expense">Expense</option>
                                    <option <?php if($new_account->account_type == 'Equity'){echo 'selected="selected"';} ?> value="Equity">Equity</option>
                                </select>
                            </td>
                        </tr>
                      
                        <tr>
                        	<td width="150">Memo</td>
                            <td><textarea name="memo" placeholder="Account Description/memo"><?php echo $new_account->memo; ?></textarea></td>
                        </tr>
                        <tr>
                        	<td width="150">CNIC : #</td>
                            <td><textarea name="cnic" placeholder=""><?php echo $new_account->cnic; ?></textarea></td>
                        </tr>
                        <tr>
                        	<td width="150">Contact :#</td>
                            <td><textarea name="contact" placeholder=""><?php echo $new_account->contact; ?></textarea></td>
                        </tr>
                        <tr>
                        	<td width="150">Address : </td>
                            <td><textarea name="address" placeholder=""><?php echo $new_account->address; ?></textarea></td>
                        </tr>
                          
					  <?php 
						if(isset($_POST['edit_account'])){ 
							echo '<input type="hidden" name="edit_account" value="'.$_POST['edit_account'].'" />';
							echo '<input type="hidden" name="update_account" value="1" />'; 
						} else { 
							echo '<input type="hidden" name="add_account" value="1" />';
						} ?>
                        <tr>
                        	<td>&nbsp;</td>
                            <td><input type="submit" value="<?php if(isset($_POST['edit_account'])){ echo 'Update Account'; } else { echo 'Add Account';} ?>" /></td>
                        </tr>
                    </table>
                    </form>
                    <script type="text/javascript">
						$(document).ready(function() {
						// validate the register form
//					$("#add_account").validate();
						});
                    </script>
                </div>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php require_once("includes/footer.php"); ?>