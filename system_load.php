<?php
	session_start();
	/*This file loads system to do basic functions on the site, Please do not change anything here if you dont know what you are doing.*/
	include('includes/db_connect.php');
	include('classes/users.php');
	include('classes/userlevel.php');
	include('classes/company.php');
	include('classes/company_access.php');
	include('classes/account.php');
	include('classes/jvs.php');
	require_once('includes/functions.php');
	
	//Checks if options exist and installation is complete.
	$val = mysqli_query($dBlink ,'SELECT 1 from options');
	if($val == FALSE) {
	  HEADER("LOCATION: install.php");
	}
?>