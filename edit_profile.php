<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user('all');
	
	//User object.
	$new_user = new Users;
	//user level object
	$new_userlevel = new Userlevel;
	
	//Profile Image Processing.
	if(isset($_FILES['profile_image']) && $_FILES['profile_image'] != '') { 
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["profile_image"]["name"]);
		$extension = end($temp);
		
		if ((($_FILES["profile_image"]["type"] == "image/gif")
		|| ($_FILES["profile_image"]["type"] == "image/jpeg")
		|| ($_FILES["profile_image"]["type"] == "image/jpg")
		|| ($_FILES["profile_image"]["type"] == "image/pjpeg")
		|| ($_FILES["profile_image"]["type"] == "image/x-png")
		|| ($_FILES["profile_image"]["type"] == "image/png"))
		&& ($_FILES["profile_image"]["size"] < 2048000)
		&& in_array($extension, $allowedExts)) {
 			 if ($_FILES["profile_image"]["error"] > 0) {
    			$message = "Return Code: " . $_FILES["profile_image"]["error"];
    	} else 	{
			$phrase = substr(md5(uniqid(rand(), true)), 16, 16);
	  if (file_exists("upload/" .$phrase.$_FILES["profile_image"]["name"])) {
	      $message = $_FILES["profile_image"]["name"] . " already exists. ";
      } else {
		  move_uploaded_file($_FILES["profile_image"]["tmp_name"],
		  "upload/".$phrase.str_replace(' ', '-',$_FILES["profile_image"]["name"]));
		  $profile_image = "upload/".$phrase.str_replace(' ', '-', $_FILES["profile_image"]["name"]);
	  } //if file not exist already.
	  
    } //if file have no error
  }//if file type is alright.
} //if image was uploaded processing.
/*Image processing ends here.*/

//User update submission image processing edit user password setting if not changed.
if(isset($_GET['user_id']) && $_GET['user_id'] != '') { 
	if(isset($profile_image)) { 
		$profile_image = $profile_image;
	} else { 
		if(isset($_POST['already_img'])) { 
			$profile_image = $_POST['already_img'];
		} else { 
			$profile_image = '';
		}
	}
	if(isset($_POST['password']) && $_POST['password'] != '') { 
		if($_POST['password'] == $_POST['confirm_password']) { 
			$password_set = $_POST['password'];
		} else { 
			$message = "Password does not match.";
		}
	} else { 
		$password_set = '';
	}
	if(isset($_POST['update_user']) && $_POST['update_user'] == '1') {
	extract($_POST);
	if($password != $confirm_password){ 
		$message = 'Password does not match!';
	} else {
	$message = $new_user->edit_profile($dBlink ,$_SESSION['user_id'], $first_name, $last_name, $date_of_birth, $address1, $address2, $city, $state, $country, $zip_code, $mobile, $phone, $email, $password_set, $profile_image, $description);
		}
	}
}//update user submission.
	
	if(isset($_GET['user_id']) && $_GET['user_id'] != '') { 
		$new_user->set_user($dBlink ,$_GET['user_id'], $_SESSION['user_type'], $_SESSION['user_id']);
	}//setting user data if editing. 	
	
	$page_title = "Administrator"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
	?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft">Edit your profile</h2>
                	<div class="clear"></div><!--clear float-->
                    <form action="<?php $_SERVER['PHP_SELF']?>" id="add_user" name="user" method="post" enctype="multipart/form-data">
                    <table cellpadding="10" cellspacing="0" width="100%" border="0">
                    	<tr>
                        	<td width="150">First Name*</td>
                            <td><input type="text" name="first_name" placeholder="Enter first name" value="<?php echo $new_user->first_name; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Last Name*</td>
                            <td><input type="text" name="last_name" placeholder="Enter last name" value="<?php echo $new_user->last_name; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Date of birth</td>
                            <td><input type="text" name="date_of_birth" placeholder="Date of birth Format 2013-12-03" value="<?php echo $new_user->date_of_birth; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Address 1</td>
                            <td><textarea name="address1" placeholder="Address line 1"><?php echo $new_user->address1; ?></textarea></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Address 2</td>
                            <td><textarea name="address2" placeholder="Address line 2"><?php echo $new_user->address2; ?></textarea></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">City</td>
                            <td><input type="text" name="city" placeholder="City" value="<?php echo $new_user->city; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">State/Province</td>
                            <td><input type="text" name="state" placeholder="State or Province" value="<?php echo $new_user->state; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Country</td>
                            <td><input type="text" name="country" placeholder="Your Country" value="<?php echo $new_user->country; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Zip code</td>
                            <td><input type="text" name="zip_code" placeholder="Your zip code" value="<?php echo $new_user->zip_code; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Mobile</td>
                            <td><input type="text" name="mobile" placeholder="Your Mobile Number" value="<?php echo $new_user->mobile; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Phone</td>
                            <td><input type="text" name="phone" placeholder="Your phone number" value="<?php echo $new_user->phone; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Email*</td>
                            <td><input type="text" name="email" placeholder="Your email address" value="<?php echo $new_user->email; ?>" required="required" /></td>
                        </tr>
                       
                        <tr>
                        	<td width="150">Password</td>
                            <td><input type="password" name="password" placeholder="Password" value="" /><small>Leave blank if you don't want to change password</small>                </td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Confirm Password</td>
                            <td><input type="password" name="confirm_password" placeholder="Confirm Password" value="" /></td>
                        </tr>
                       
                        <tr>
                        	<td width="150">Profile Image</td>
                            <td>
                            	<input type="file" name="profile_image" placeholder="Your profile image" />
                            	<?php
									if(isset($new_user->profile_image) && $new_user->profile_image != '') {
										echo '<a href="'.$new_user->profile_image.'" target="_blank"><img src="'.$new_user->profile_image.'" height="80" /></a><input type="hidden" name="already_img" value="'.$new_user->profile_image.'">';	
									}
								?>
                            </td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Description</td>
                            <td><textarea name="description" placeholder="User Description"><?php echo $new_user->description; ?></textarea></td>
                        </tr>
                       	<input type="hidden" name="update_user" value="1" /> 
						<tr>
                        	<td>&nbsp;</td>
                            <td><input type="submit" value="Update User" /></td>
                        </tr>
                    </table>
                    </form>
                    <script type="text/javascript">
						$(document).ready(function() {
						// validate the register form
					$("#add_user").validate();
						});
                    </script>
                </div>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php
	require_once("includes/footer.php");
?>