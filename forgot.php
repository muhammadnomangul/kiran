<?php
	include('system_load.php');
	//This loads system.
	
	if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') { 
		HEADER('LOCATION: dashboard.php');
	} //If user is loged in redirect to specific page.
	
	$new_user = new Users; //creating user object.
	
	if(isset($_POST['forgot_pass'])) {
		$forgot_pass = $_POST['forgot_pass'];
		if($forgot_pass == 1){
			extract($_POST);
			$message = $new_user->forgot_user($dBlink ,$email);
		}//processing forgot password Email sending.
	}//if isset forgot pass
	
	if(isset($_POST['reset_form'])) {
		$reset_form = $_POST['reset_form'];
		if($reset_form == 1){
			extract($_POST);
			if($password!=$match_password){
				$message = 'Passwords doesnt match. Enter again';	
			} else {
			   $confirmation_code = $_GET['confirmation_code'];
			   $message = $new_user->reset_pass_user($dBlink ,$_GET['user_id'],$confirmation_code,$password);
			}
		}//reset Form reset password processing.
	}//isset reset_ form
	
	$page_title = "Forgot Password!"; //You can edit this to change your page title.
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<style type="text/css" title="currentStyle">
	@import "css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#wc_table').dataTable();
	} );
	function confirm_delete() { 
		var del = confirm('Do you really want to delete this record?');
		if(del == true) { 
			return true;
		} else { 
			return false;
		}
	}//delete_confirmation ends here.
</script>
</head>
<body>
	<?php
		if(isset($message) && $message != '') { 
			echo '<div class="alert-box">';
			echo $message;
			echo '</div>';
		}
	?>

    	<div class="form_wrapper">
        	<h1><?php echo $page_title; ?></h1>
         <?php if(!isset($_GET['confirmation_code']) || $_GET['confirmation_code'] == '') : ?>   
        <form action="<?php $_SERVER['PHP_SELF']?>" id="forgot_form" name="forgot" method="post">
            <table width="100%" cellpadding="10" cellspacing="0" border="0">
                <tr>
                    <td>Please enter your email address to recover your password.</td>
                </tr>
                <tr>
                    <td><input type="text" placeholder="Email*" name="email" required="required" /></td>
                </tr>
              
                <tr>
                    <td><input type="submit" value="Submit" /></td>
 	             </tr>
   						<input type="hidden" name="forgot_pass" value="1" />
            </table>
            </form>
            <script>
				$(document).ready(function() {
					// validate the register form
					$("#forgot_form").validate();
				});
            </script>
			<?php else: ?>
            <form action="<?php $_SERVER['PHP_SELF']?>" id="reset_form" name="reset_form" method="post">
            <table cellpadding="10" width="100%" cellspacing="0" border="0">
                <tr>
                    <td><input type="password" name="password" placeholder="New Password" required="required" /></td>
                </tr>
                <tr>
                    <td><input type="password" name="match_password" placeholder="Confirm Password" required="required"/></td>
                </tr>
                
	             <input type="hidden" value="1" name="reset_form" />
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
   
            </table>
            </form>
            <script>
				$(document).ready(function() {
					// validate the register form
					$("#reset_form").validate();
				});
            </script>
            <?php endif; ?>
        </div><!--//form_wrapper-->
        <div class="aligncenter">
        	Already have an account? <a href="login.php">Sign in</a><br />
            Not a member yet? <a href="register.php">Sign up</a>
        </div>
</div><!--//wc_wrapper--> 
</body>
</html>