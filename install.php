<?php
	//Page display settings.
	$page_title = "Installation!"; //You can edit this to change your page title.
	require_once("includes/functions.php"); //option functions file.
	require_once("includes/db_connect.php"); //Database connection file.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from options') == FALSE) {
		$query = 'CREATE TABLE options (
			`option_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`option_name` varchar(500) NOT NULL,
			`option_value` varchar(500) NOT NULL,
  			PRIMARY KEY (`option_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating options table.
	
	if(mysqli_query($dBlink ,'SELECT 1 from users') == FALSE) { 
		$query = 'CREATE TABLE users (
			`user_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`first_name` varchar(100) NOT NULL,
			`last_name` varchar(100) NOT NULL,
			`date_of_birth` date NOT NULL,
			`address1` varchar(200) NOT NULL,
			`address2` varchar(200) NOT NULL,
			`city` varchar(100) NOT NULL,
			`state` varchar(100) NOT NULL,
			`country` varchar(100) NOT NULL,
			`zip_code` varchar(100) NOT NULL,
			`mobile` varchar(200) NOT NULL,
			`phone` varchar(200) NOT NULL,
			`email` varchar(200) NOT NULL,
			`password` varchar(200) NOT NULL,
			`profile_image` varchar(500) NOT NULL,
			`description` varchar(600) NOT NULL,
			`status` varchar(100) NOT NULL,
			`activation_key` varchar(100) NOT NULL,
			`date_register` date NOT NULL,
			`user_type` varchar(100) NOT NULL,
  			PRIMARY KEY (`user_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	}  //Creating users table ends here.
	
	if(mysqli_query($dBlink ,'SELECT 1 from companies') == FALSE) { 
		$query = 'CREATE TABLE companies (
			`company_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`company_manual_id` varchar(100) NOT NULL,
			`company_name` varchar(100) NOT NULL,
			`business_type` varchar(100) NOT NULL,
			`address1` varchar(200) NOT NULL,
			`address2` varchar(200) NOT NULL,
			`city` varchar(100) NOT NULL,
			`state` varchar(100) NOT NULL,
			`country` varchar(100) NOT NULL,
			`zip_code` varchar(100) NOT NULL,
			`phone` varchar(200) NOT NULL,
			`email` varchar(200) NOT NULL,
			`company_logo` varchar(500) NOT NULL,
			`description` varchar(600) NOT NULL,
			`user_id` varchar(100) NOT NULL,
			`currency_symbol` varchar(100) NOT NULL,
		PRIMARY KEY (`company_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	}  //Creating users table ends here.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from user_level') == FALSE) {
		$query = 'CREATE TABLE user_level (
			`level_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`level_name` varchar(200) NOT NULL,
			`level_description` varchar(600) NOT NULL,
			`level_page` varchar(100) NOT NULL,
  			PRIMARY KEY (`level_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating user level table ends.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from journal_voucher') == FALSE) {
		$query = 'CREATE TABLE journal_voucher (
			`jv_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`date` date NOT NULL,
			`jv_id_manual` varchar(100) NULL,
			`jv_title` varchar(100) NULL,
			`jv_description` varchar(200) NULL,
			`user_id` bigint(20) NULL,
			`company_id` bigint(20) NULL,
  			PRIMARY KEY (`jv_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating Journal Voucher table ends.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from transactions') == FALSE) {
		$query = 'CREATE TABLE transactions (
			`tr_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`jv_id` bigint(20) NULL,
			`account_id` bigint(20) NULL,
			`date` date NULL,
			`memo` varchar(400) NULL,
			`debit` decimal(10,2),
			`credit` decimal(10,2),
  			PRIMARY KEY (`tr_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating Journal Voucher table ends.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from accounts') == FALSE) {
		$query = 'CREATE TABLE accounts (
			`account_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`account_number` varchar(200) NOT NULL,
			`account_title` varchar(200) NOT NULL,
			`account_type` varchar(200) NOT NULL,
			`memo` varchar(600) NOT NULL,
			`company_id` bigint(20) NOT NULL,
			PRIMARY KEY (`account_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating user level table ends.
	
	//if database tables does not exist already create them.
	if(mysqli_query($dBlink ,'SELECT 1 from company_access') == FALSE) {
		$query = 'CREATE TABLE company_access (
			`access_id` bigint(20) NOT NULL AUTO_INCREMENT,
			`user_id` bigint(20) NOT NULL,
			`company_id` bigint(20) NOT NULL,
  			PRIMARY KEY (`access_id`)
		)';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	} //creating user level table ends.
	
	//Check if installation is already complete.
	$installation = get_option('installation');
	if($installation == 'Yes') { 
		HEADER('LOCATION: index.php');
		exit();
	}
	//installation form processing when submits.
	if(isset($_POST['install_submit']) && $_POST['install_submit'] == 'Yes') {
		extract($_POST);
		//validation to check if fields are empty!
		if($site_url == '') { 
			echo 'Site url cannot be empty!';
		} else if($email_from == '') { 
			echo 'Email from cannot be empty!';
		} else if($email_to == '') { 
			echo 'Reply to cannot be empty!';
		} else if($email == '') { 
			echo 'Admin email cannot be empty!';
		} else if($password == '') { 
			echo 'Admin Password cannot be empty!';
		} else {
			//adding site url
			set_option('site_url', $site_url);
			set_option('site_name', $site_name);
			set_option('email_from', $email_from);
			set_option('email_to', $email_to);
			set_option('installation', 'Yes');
			install_admin($first_name, $last_name, $email, $password);
			HEADER('LOCATION: index.php');
		}//form validations
	}//form processing.
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<style type="text/css" title="currentStyle">
	@import "css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#wc_table').dataTable();
	} );
	function confirm_delete() { 
		var del = confirm('Do you really want to delete this record?');
		if(del == true) { 
			return true;
		} else { 
			return false;
		}
	}//delete_confirmation ends here.
</script>
</head>
<body>
	<div class="wc_wrapper">
    	<!-- you can copy following form in your registration page!-->
    	<div class="form_wrapper" style="width:600px; padding:20px;">
        	<h2><?php echo $page_title; ?></h2>
            <hr />
            <h3>Note: You can delete install.php once your installation is complete and working fine.</h3><br /><br />
            <p>Here you can setup basic values for this login script. Please make sure you give correct values cause you could not edit them later without using PHPmyADMIn options table.</p>
            <form name="set_install" id="set_install" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
            	<table width="100%" cellpadding="10" cellspacing="0" border="0">
                	<tr>
                    	<td>Site URL*:</td>
                        <td><input type="text" name="site_url" required /><small>Please include / at end of site url e.g http://localhost/</small></td>
                    </tr>
                    
                    <tr>
                    	<td>Site Name:</td>
                        <td><input type="text" name="site_name" /></td>
                    </tr>
                    <tr>
                    	<td colspan="2">
                        	Email settings used for the system will send Emails to users.
                        </td>
                    </tr>
                    <tr>
                    	<td>Email From*:</td>
                        <td><input type="text" name="email_from" required /></td>
                    </tr>
                    <tr>
                    	<td>Reply To*:</td>
                        <td><input type="text" name="email_to" required /></td>
                    </tr>
                     <tr>
                    	<td colspan="2">
                        	Administrator Settings this user will able to handle all users their levels their status active/ban.
                        </td>
                    </tr>
                    <tr>
                    	<td>First Name:</td>
                        <td><input type="text" name="first_name" /></td>
                    </tr>
                    <tr>
                    	<td>Last Name:</td>
                        <td><input type="text" name="last_name" /></td>
                    </tr>
                    <tr>
                    	<td>Email*:</td>
                        <td><input type="text" name="email" required /></td>
                    </tr>
                    <tr>
                    	<td>Password*:</td>
                        <td><input type="password" name="password" required /></td>
                    </tr>
                    <input type="hidden" name="install_submit" value="Yes" />
                    <tr>
                    	<td>&nbsp;</td>
                        <td><input type="submit" value="Submit" /></td>
                    </tr>
                </table>
            </form>
            <script>
				$(document).ready(function() {
					// validate the Installation form
					$("#set_install").validate();
				});
            </script>

        </div><!--//form_wrapper-->
    </div><!--//wc_wrapper--> 
</body>
</html>