<?php
	include('system_load.php');
	//This loads system.
	//user Authentication.
	authenticate_user('subscriber','all');
	
	$new_company = new Company;
	//new account object/
	$new_account = new Account;
	//new journal voucher object
	$new_jvs = new Jvs;
	
	if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
		$_SESSION['company_name'] = $new_company->company_name($dBlink ,$_SESSION['company_id']);
	} else { 
		HEADER('LOCATION: company.php?message=Please select a company.');
	}//check if company is selected.
	
	//if user not coming directly.
	if(isset($_POST['view_jv']) && $_POST['view_jv'] != '') { 
		//you can access this form.
		$new_jvs->set_jvs($dBlink ,$_POST['view_jv']);
		$new_company->set_company($dBlink ,$_SESSION['company_id']);
	} else { 
		HEADER('LOCATION: index.php');
	}
	
	$page_title = $_SESSION['company_name']; //You can edit this to change your page title.
?>
<!DOCTYPE  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
</body>
<style type="text/css">
.transactions table { border-collapse: collapse; }
.transactions table td, .transactions table th { border: 1px solid #CCCCCC; padding: 5px; }

.transactions textarea, .transactions input[type=text] {height:28px;}

.transactions {margin-top:30px;}
.transactions table {margin-top:8px;}
.transactions strong small {margin-left:345px; color:green;}
#items th { background: #eee; }

#debit_amnt, #credit_amnt, #balance_amnt {font-weight:bold;}
textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delme:hover { background-color:#EEFF88; }
</style>
		        <div align="center" class="company_info">
                	<?php if($new_company->company_logo != '') { 
						echo "<img src='".$new_company->company_logo."' height='50px' class='company_logo' align='left'>";
					} ?>
                    <h2><?php echo $new_company->company_name; ?></h2>
                    <p><?php echo $new_company->address1; ?> <?php echo $new_company->address2; ?> <?php echo $new_company->city; ?> <?php echo $new_company->state; ?> <?php echo $new_company->country; ?> <?php echo $new_company->zip_code; ?><br />Email: <?php echo $new_company->email; ?> Phone: <?php echo $new_company->phone; ?></p>	
                <div style="clear:both;"></div>
                </div><!--company_info ends here.-->

                <div class="journal" align="center">
                	<table width="680px" border="0" cellpadding="5">
                    	<tr>
                        	<th width="95px">Date</th>
                            <td><?php echo $new_jvs->date; ?></td>
                        	<th width="95px">JV Manual ID</th>
                            <td><?php echo $new_jvs->jv_manual_id; ?></td>
                        </tr>
                        <tr>
                        	<th>Posted By</th>
                            <td><?php echo $new_jvs->jv_poster; ?></td>
                            <th>JV Unique ID</th>
                            <td><?php echo $new_jvs->jv_id; ?></td>
                        </tr>
                        <tr>
                        	<th>JV Title</th>
                            <td><?php echo $new_jvs->jv_title; ?></td>
                            <th>JV Description</th>
                            <td><?php echo $new_jvs->jv_description; ?></td>
                        </tr>
                    </table>
                </div><!--journal Ends here.-->
         <div class="transactions" align="center"> 
         <strong>Transactions</strong>          
		<table id="items" width="680px">
		 <tr>
		      <th width="150">Account</th>
		      <th width="380">Memo</th>
		      <th>Amount</th>
		  </tr>
		 <?php $new_jvs->transaction_row($dBlink ,$new_jvs->jv_id); ?>
	</table><br />
<br />

    <p align="center">This is computer generated statement does not need signature.</p>
    </div><!--transactions div-->
            </div><!--admin wrap ends here.-->
                        
<?php require_once("includes/footer.php"); ?>