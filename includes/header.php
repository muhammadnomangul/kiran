<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link href="css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<script src="js/jquery-1.10.2.min.js"></script>

<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<style type="text/css" title="currentStyle">
	@import "css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#wc_table').dataTable();
	} );
	function confirm_delete() { 
		var del = confirm('Do you really want to delete this record?');
		if(del == true) { 
			return true;
		} else { 
			return false;
		}
	}//delete_confirmation ends here.
	$(function() {
		$("#datepicker" ).datepicker({
			inline: true,
			dateFormat: 'yy-mm-dd',
		});
	});
</script>
</head>
<body>

<div class="wc_wrapper">
    <div class="title_wrap">
        <div class="alignleft">
            <h1><?php echo $page_title; ?></h1>
        </div>
        <div class="alignright">
            Welcom <?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?> | <a href="edit_profile.php?user_id=<?php echo $_SESSION['user_id']; ?>">Edit Profile</a> | <a href="dashboard.php?logout=1">Logout</a>
        </div>
        <div class="clear"></div><!--clear float effects.-->
    </div>