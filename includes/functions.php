<?php
	/*This file have all functions to handle options.
	1) Set Option
	2) Get Option
	3) Install Admin
	4) Authentication
	*/
 function current_balance($dBlink ,$account_id, $from, $to) {
    $query_op_bal = "SELECT SUM(debit), SUM(credit) from transactions WHERE account_id='".$account_id."' AND date < '".$from."'";
    $result_op_bal = mysqli_query($dBlink ,$query_op_bal) or die(mysql_error());
    $row_op_bal = mysqli_fetch_array($result_op_bal);
    $opening_balance = $row_op_bal['SUM(debit)']+$row_op_bal['SUM(credit)'];
    //opening balance ends here.

//		$content = '<tr>
//
//  				<td>&nbsp;</td>
//    			<td>&nbsp;</td>
//			    <td>Opening balance</td>
//    			<td class="align_right">&nbsp;</td>
//    			<td class="align_right">&nbsp;</td>
//			    <td class="align_right">'.number_format($opening_balance).'</td>
//			    </tr>';
    $balance = $opening_balance;
    //opening balance row ends here.

    //getting transactions.
    $query = "SELECT * from transactions WHERE account_id='".$account_id."' AND date between '".$from."' AND '".$to."' ORDER by tr_id ASC";
    $result = mysqli_query($dBlink ,$query) or die(mysql_error());
    while($row = mysqli_fetch_array($result)) {
        $balance = $balance+$row['debit']+$row['credit'];
//			$content .= '<tr>';
//
//			$content .= '<td>'.$row['jv_id'].'</td>';
//			$content .= '<td>'.$row['date'].'</td>';
//			$content .= '<td>'.$row['memo'].'</td>';
//			$content .= '<td class="align_right">'.number_format($row['debit']).'</td>';
//			$content .= '<td class="align_right">'.number_format($row['credit']).'</td>';
//			$content .= '<td class="align_right">'.number_format($balance).'</td>';
//			$content .= '</tr>';
    }//transactions loop ends.

    echo $balance;
}//trasnactions summary ends here.
	
	function set_option($dBlink ,$option_name, $option_value) {
			$query = "SELECT * from options WHERE 'option_name'='".$option_name."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$num_rows = mysqli_num_rows($result);
			if($result > 0) { 
				$query = "DELETE from options WHERE option_name='".$option_name."'";
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			}//This will delete record
			$query = "INSERT into options VALUES(NULL, '".$option_name."', '".$option_value."')";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			//this function do not return anything!
	}//set option function ends here.
	
	function get_option($dBlink ,$option_name) { 
		$query = "SELECT * from options WHERE option_name='".$option_name."'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysqli_fetch_array($result);
		$option_value = stripslashes($row['option_value']);//this will remove database slashes from values
		return $option_value; //This function returns option value.
	}//get option value function ends here.

function get_id($dBlink) {
    $query = "SELECT * FROM journal_voucher ORDER BY jv_id DESC LIMIT 1";
    $result = mysqli_query($dBlink ,$query) or die(mysql_error());
    $row = mysqli_fetch_array($result);
    $option_value = stripslashes($row['jv_id']);//this will remove database slashes from values
    return $option_value; //This function returns option value.
}//get option value function ends here.
	
	function install_admin($dBlink ,$first_name, $last_name, $email, $password) {
		$password = md5($password);
		//check if admin already exist.
		$query = "SELECT * from users WHERE user_type='admin'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$num_rows = mysql_num_rows($result);
		
		if($num_rows>0) { 
			echo 'Admin already exist cannot be addedd!';
		} else { 
			//adding admin
			$query = "INSERT into users (user_id, first_name, last_name, email, password, status, date_register, user_type)
					VALUES(NULL, '".$first_name."', '".$last_name."', '".$email."', '".$password."', 'activate', '".date('Y-m-d')."', 'admin')";
		    $result = mysqli_query($dBlink ,$query) or die(mysql_error());
		}
		
		//adding deafult user level subscriber.
		$query = "SELECT * from user_level WHERE level_name='subscriber'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$num_rows = mysql_num_rows($result);
		
		if($num_rows > 0) { 
			//do nothing already subscriber level
		} else { 
			$query = "INSERT into user_level VALUES(NULL, 'subscriber', 'Default user level given access to dashboard.php', 'dashboard.php')";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		}
		
	}//Function checkes if admin does not exist this will create admin.
	
	function redirect_user($dBlink ,$user_type) { 
		if(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'admin') { 
				HEADER('LOCATION: dashboard.php');
			} else {
				$query = "SELECT * from user_level WHERE level_name='".$_SESSION['user_type']."'";
					$result = mysqli_query($dBlink ,$query) or die(mysql_error());
					$num_rows = mysql_num_rows($result);
					
					if($num_rows > 0) { 
						$row = mysql_fetch_array($result);
						$page = $row['level_page'];
						HEADER('LOCATION:'.$page);
					} else { 
						//If you are not admin and not given access user. You will be redirected to index.php
						HEADER('LOCATION: index.php');
					}

			}
	}	
	function authenticate_user($dBlink ,$access_level) { 
		if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
			//check user level
			if(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'admin') { 
				//admin can access all pages.
			} else if($access_level == 'all') { 
				//all user types can access here but only when signed in.
			} else { 
				if(isset($_SESSION['user_type']) && $_SESSION['user_type'] == $access_level) { 
					//You can access this page now.
				} else { 
					$query = "SELECT * from user_level WHERE level_name='".$_SESSION['user_type']."'";
					$result = mysqli_query($dBlink ,$query) or die(mysql_error());
					$num_rows = mysql_num_rows($result);
					
					if($num_rows > 0) { 
						$row = mysqli_fetch_array($result);
						$page = $row['level_page'];
						HEADER('LOCATION:'.$page);
					} else { 
						//If you are not admin and not given access user. You will be redirected to index.php
						HEADER('LOCATION: index.php');
					}
					
				} //if user level is accessable.
			}
		} else { 
			HEADER('LOCATION: index.php');
		}//this is loged in user.
	}//authenticate user ends here.
	
	function partial_access($dBlink ,$access_type) {
        if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
            if($access_type == 'admin' && $_SESSION['user_type'] == 'admin') {
                return TRUE;
            } else if($access_type == 'all') {
				return TRUE;
			} else if($access_type == $_SESSION['user_type']) { 
				return TRUE;
			} else { 
				return FALSE;
			}
		} else { 
			return FALSE;
		}
	}//partial access function ends here.