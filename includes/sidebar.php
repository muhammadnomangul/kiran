<div class="sidebar">
    <ul>
        <li><a href="dashboard.php">Dashboard</a></li>
        <?php if(partial_access($dBlink,'admin')) { ?><li><a href="users.php">Users</a></li><?php } ?>
        <li><a href="company.php">Companies</a>
        <?php if(partial_access($dBlink,'admin')) { ?><ul><li><a href="company_access.php">Company Access</a></li></ul><?php } ?>
        </li>
        <li><a href="heads.php">heads</a></li>
        <li><a href="accounts.php">Accounts</a></li>
        <li><a href="#">Reports</a>
            <ul>
                <li><a href="#">Vouchers</a>
                    <ul>
                        <li><a href="reports/detail.php" target="_blank">Detail Vouchers</a></li>
                        <li><a href="reports/cpv.php" target="_blank">Cash Payment Vouchers</a></li>
                        <li><a href="reports/crv.php">Cash Receipt Vouchers</a></li>
                        <li><a href="reports/bpv.php" target="_blank">Bank Payment Vouchers</a></li>
                        <li><a href="reports/avs.php">Adjustment Vouchers</a></li>
                        <li><a href="reports/bcv.php" target="_blank">Bank Receipt vouchers</a></li>
                        <li><a href="reports/jv.php" target="_blank">Journal Vouchers</a></li>
                    </ul>
                </li>

                <li><a href="reports/ps.php">Posting Summary</a></li>
                <li><a href="jvs.php?message=Please select a voucher.">Single Voucher</a></li>
                <li><a href="reports/cp.php">cash </a></li>
                <li><a href="reports/balancesheet.php">Balancesheet </a></li>
                <li><a href="reports/ledger_summary.php" target="_blank">General Ledger summary</a></li>
                <li><a href="accounts.php?message=Please select an account.">Ledger by account</a></li>
                <li><a href="reports/trial_balance.php" target="_blank">Trial Balance</a></li>
<!--                    <ul>-->
<!--                        <li><a href="reports/jv.php" target="_blank">Cash Payment Vouchers</a></li>-->
<!--                        <li><a href="jvs.php?message=Please select a voucher.">Cash Receive Vouchers</a></li>-->
<!--                        <li><a href="reports/ledger_summary.php" target="_blank">Bank Payment Vouchers</a></li>-->
<!--                        <li><a href="accounts.php?message=Please select an account.">Adjsstment Vouchers</a></li>-->
<!--                        <li><a href="reports/trial_balance.php" target="_blank">bank receive voture</a></li>-->
<!--                    </ul>-->
            </ul>

        </li>

        <li><a href="cpv.php">Cash Payment Vouchers</a></li>
        <li><a href="crv.php">Cash Receipt Vouchers</a></li>
        <li><a href="bpv.php">Bank Payment Vouchers</a></li>
        <li><a href="bcv.php">Bank Receipt vouchers</a></li>
        <li><a href="avs.php">Adjustment Vouchers</a></li>
        <li><a href="jvs.php">Journal Vouchers</a></li>
        <li><a href="dashboard.php?logout=1">Logout</a></li>
    </ul>
</div><!--sidebar Ends here.-->
