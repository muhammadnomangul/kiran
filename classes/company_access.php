<?php
//CompanyAccess Class

class CompanyAccess {
	
	function add_company_access($dBlink ,$user_id, $company_id) { 
		if($_SESSION['user_type'] == 'admin') {
			$query = "SELECT * from company_access WHERE user_id='".$user_id."' AND company_id='".$company_id."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$rows = mysqli_num_rows($result);
			if($rows > 0) { 
				return 'User already have access to this company.';
			} else { 
				$query = "INSERT into company_access(user_id, company_id) VALUES('".$user_id."', '".$company_id."')";
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
				return 'Access granted successfuly.';
			}
		} else { 
			return 'You cannot access this feature.';
		}
	}//add company acces ends here,.
	
	function list_company_access($dBlink) { 
		if($_SESSION['user_type'] != 'admin') {
			echo 'You cannot view this list.';	
		} else {
			$query = "SELECT * from company_access";
			$result = mysqli_query($dBlink ,$query) or die(mysqli_error());
			$options = '';
			while($row = mysqli_fetch_array($result)) {
				$query_user = "SELECT * from users WHERE user_id='".$row['user_id']."'";
				$result_user = mysqli_query($dBlink ,$query_user) or die(mysqli_error());
				$row_user = mysqli_fetch_array($result_user);
				//user info query ends here.
				$query_company = "SELECT * from companies WHERE company_id='".$row['company_id']."'";
				$result_company = mysqli_query($dBlink ,$query_company) or die(mysqli_error());
				$row_company = mysqli_fetch_array($result_company);
				//company info ends here.
				
				$options .= '<tr>';
				$options .= '<td>'.$row['user_id'].'</td>';
				$options .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
				$options .= '<td>'.$row_user['email'].'</td>';
				$options .= '<td>'.$row_company['company_name'].'</td>';
				$options .= '<td><form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$options .= '<input type="hidden" name="delete_access" value="'.$row['access_id'].'">';
				$options .= '<input type="submit" value="Delete Access">';
				$options .= '</form></td>';
				$options .= '</tr>';
			}//while loop ends here.
			echo $options;	
		}
	}//list_company_access function ends here.
	
	function delete_access($dBlink ,$access_id) { 
		if($_SESSION['user_type'] == 'admin') { 
			$query = "DELETE from company_access WHERE access_id='".$access_id."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			return 'Access deleted successfuly!';
		}//if admin
	}//delete acces function ends here.
}//company access class ends here.