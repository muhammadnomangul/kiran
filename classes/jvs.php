<?php
class Jvs{
	public $date;
	public $jv_manual_id;
	public $jv_title;
	public $jv_description;
	public $jv_poster;
	public $jv_id;
	
	function delete_jvs($dBlink ,$jv_id) {
		if($_SESSION['user_type'] == 'admin') { 
			//delete transactions.
			$del_tr = "DELETE from transactions WHERE jv_id='".$jv_id."'";
			$result_tr = mysqli_query($dBlink ,$del_tr) or die(mysql_error());
			//delete journal voucher.
			$query = "DELETE from journal_voucher WHERE jv_id='".$jv_id."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			
			return 'Journal Voucher and related transactions removed successfuly.';
		} else { 
			return 'You cannot delete this JV.';
		}	
	}//jv delete Ends here.
	
	function transaction_row($dBlink ,$jv_id) { 
		$query = "SELECT * from transactions WHERE jv_id='".$jv_id."' ORDER by tr_id DESC";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$credit = 0;
		$debit = 0;
		$balance = 0;
		$content = '';
		
		while($row = mysqli_fetch_array($result)) {
			$content .= '<tr class="item-row">';
			//account query starts here to get account title.
			$account_query = "SELECT * from accounts WHERE account_id='".$row['account_id']."' AND company_id='".$_SESSION['company_id']."'";
			$account_result = mysqli_query($dBlink ,$account_query) or die(mysqli_error());
//
			$account_row = mysqli_fetch_array($account_result);
			//account query endsh ere.
		    $content .= '<td class="item-name">'.$account_row['account_title'].'</td>';
		    $content .= '<td class="description">'.$row['memo'].'</td>';
			if($row['debit'] == 0) {
		    	$content .= '<td>'.number_format($row['credit']).' '.$_SESSION['currency'].'</td>';
			} else { 
				$content .= '<td>'.number_format($row['debit']).' '.$_SESSION['currency'].'</td>';
			}
		  $content .= '</tr>';
		  $debit = $debit+$row['debit'];
		  $credit = $credit+$row['credit'];
		  $balance = $balance+$row['credit']+$row['debit'];
		}
		$content .= '<tr>
         	<td colspan="3" align="right">
            	Debit: <span id="debit_amnt">'.number_format($debit).' '.$_SESSION['currency'].'</span><br />
                Credit: <span id="credit_amnt">'.number_format($credit).' '.$_SESSION['currency'].'</span> <br />
                Balance:  <span id="balance_amnt">'.number_format($balance).' '.$_SESSION['currency'].'</span><br />
            </td>
         </tr>';
		 echo $content;
	}//transaction row ends here.
	
	function set_jvs($dBlink ,$jv_id) {
		$query = "SELECT * from journal_voucher WHERE jv_id='".$jv_id."' AND company_id='".$_SESSION['company_id']."'";		
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		if(mysqli_num_rows($result) < 0) {
			echo 'There is no such Voucher in this company.';
			exit();
		}
		$row = mysqli_fetch_array($result);
		
		$this->jv_id = $row['jv_id'];
		$this->date = $row['date'];
		$this->jv_manual_id = $row['jv_id_manual'];
		$this->jv_title = $row['jv_title'];
		$this->jv_description = $row['jv_description'];
		//getting user name first and last.
			$query_user = "SELECT * from users WHERE user_id='".$row['user_id']."'";
			$result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
			$row_user = mysqli_fetch_array($result_user);
			//end of getting user.
		$this->jv_poster = $row_user['first_name'].' '.$row_user['last_name']; 
	}//set jvs ends here.

	function list_jvs($dBlink) { 
		$query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$content = '';
		while($row = mysqli_fetch_array($result)) {
			extract($row);
			$content .= '<tr>';
			$content .= '<td>'.$date.'</td>';
			$content .= '<td>'.$jv_id_manual.'</td>';
			$content .= '<td>'.$jv_title.'</td>';
			$content .= '<td>'.$jv_description.'</td>';
			//getting user name first and last.
			$query_user = "SELECT * from users WHERE user_id='".$user_id."'";
			$result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
			$row_user = mysqli_fetch_array($result_user);
			//end of getting user.
			$content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
			//getting balance
			$amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
			$result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
			$row = mysqli_fetch_array($result_amount);
			//end of getting balance of vouchar
			$content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
			$content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
			$content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
			$content .= '<input type="submit" value="View">';
			$content .= '</form></td>';
			if(partial_access($dBlink,  'admin')) {
			$content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
			$content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
			$content .= '<input type="submit" value="Delete">';
			$content .= '</form></td>';
			}
			$content .= '</tr>';
		}
		echo $content;
	}//list_jv ends here.

    function list_cpv($dBlink) {
	    $type = 'cpv';
//        $query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
        $query = "SELECT * from journal_voucher WHERE jv_type='".$type."' AND company_id='".$_SESSION['company_id']."'  ORDER by jv_id DESC";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $content = '';
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content .= '<tr>';
            $content .= '<td>'.$date.'</td>';
            $content .= '<td>'.$jv_id_manual.'</td>';
            $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td>'.$jv_description.'</td>';
            //getting user name first and last.
            $query_user = "SELECT * from users WHERE user_id='".$user_id."'";
            $result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
            $row_user = mysqli_fetch_array($result_user);
            //end of getting user.
            $content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
            //getting balance
            $amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
            $result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
            $row = mysqli_fetch_array($result_amount);
            //end of getting balance of vouchar
            $content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
            $content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
            $content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
            $content .= '<input type="submit" value="View">';
            $content .= '</form></td>';
            if(partial_access($dBlink,  'admin')) {
                $content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
                $content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
                $content .= '<input type="submit" value="Delete">';
                $content .= '</form></td>';
            }
            $content .= '</tr>';
        }
        echo $content;
    }//list_jv ends here.

    function list_avs($dBlink) {
        $type = 'avs';
//        $query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
        $query = "SELECT * from journal_voucher WHERE jv_type='".$type."' AND company_id='".$_SESSION['company_id']."'  ORDER by jv_id DESC";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $content = '';
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content .= '<tr>';
            $content .= '<td>'.$date.'</td>';
            $content .= '<td>'.$jv_id_manual.'</td>';
            $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td>'.$jv_description.'</td>';
            //getting user name first and last.
            $query_user = "SELECT * from users WHERE user_id='".$user_id."'";
            $result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
            $row_user = mysqli_fetch_array($result_user);
            //end of getting user.
            $content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
            //getting balance
            $amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
            $result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
            $row = mysqli_fetch_array($result_amount);
            //end of getting balance of vouchar
            $content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
            $content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
            $content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
            $content .= '<input type="submit" value="View">';
            $content .= '</form></td>';
            if(partial_access($dBlink,  'admin')) {
                $content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
                $content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
                $content .= '<input type="submit" value="Delete">';
                $content .= '</form></td>';
            }
            $content .= '</tr>';
        }
        echo $content;
    }//list_jv ends here.

    function list_bcv($dBlink) {
        $type = 'bcv';
//        $query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
        $query = "SELECT * from journal_voucher WHERE jv_type='".$type."' AND company_id='".$_SESSION['company_id']."'  ORDER by jv_id DESC";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $content = '';
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content .= '<tr>';
            $content .= '<td>'.$date.'</td>';
            $content .= '<td>'.$jv_id_manual.'</td>';
            $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td>'.$jv_description.'</td>';
            //getting user name first and last.
            $query_user = "SELECT * from users WHERE user_id='".$user_id."'";
            $result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
            $row_user = mysqli_fetch_array($result_user);
            //end of getting user.
            $content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
            //getting balance
            $amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
            $result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
            $row = mysqli_fetch_array($result_amount);
            //end of getting balance of vouchar
            $content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
            $content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
            $content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
            $content .= '<input type="submit" value="View">';
            $content .= '</form></td>';
            if(partial_access($dBlink,  'admin')) {
                $content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
                $content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
                $content .= '<input type="submit" value="Delete">';
                $content .= '</form></td>';
            }
            $content .= '</tr>';
        }
        echo $content;
    }//list_jv ends here.

    function list_bpv($dBlink) {
        $type = 'bpv';
//        $query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
        $query = "SELECT * from journal_voucher WHERE jv_type='".$type."' AND company_id='".$_SESSION['company_id']."'  ORDER by jv_id DESC";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $content = '';
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content .= '<tr>';
            $content .= '<td>'.$date.'</td>';
            $content .= '<td>'.$jv_id_manual.'</td>';
            $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td>'.$jv_description.'</td>';
            //getting user name first and last.
            $query_user = "SELECT * from users WHERE user_id='".$user_id."'";
            $result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
            $row_user = mysqli_fetch_array($result_user);
            //end of getting user.
            $content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
            //getting balance
            $amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
            $result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
            $row = mysqli_fetch_array($result_amount);
            //end of getting balance of vouchar
            $content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
            $content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
            $content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
            $content .= '<input type="submit" value="View">';
            $content .= '</form></td>';
            if(partial_access($dBlink,  'admin')) {
                $content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
                $content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
                $content .= '<input type="submit" value="Delete">';
                $content .= '</form></td>';
            }
            $content .= '</tr>';
        }
        echo $content;
    }//list_jv ends here.

    function list_crv($dBlink) {
        $type = 'crv';
//        $query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' ORDER by jv_id DESC";
        $query = "SELECT * from journal_voucher WHERE jv_type='".$type."' AND company_id='".$_SESSION['company_id']."'  ORDER by jv_id DESC";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $content = '';
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content .= '<tr>';
            $content .= '<td>'.$date.'</td>';
            $content .= '<td>'.$jv_id_manual.'</td>';
            $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td>'.$jv_description.'</td>';
            //getting user name first and last.
            $query_user = "SELECT * from users WHERE user_id='".$user_id."'";
            $result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
            $row_user = mysqli_fetch_array($result_user);
            //end of getting user.
            $content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
            //getting balance
            $amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
            $result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
            $row = mysqli_fetch_array($result_amount);
            //end of getting balance of vouchar
            $content .= '<td>'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
            $content .= '<td><form target="_blank" method="post" name="view_jvs" action="view_jv.php">';
            $content .= '<input type="hidden" name="view_jv" value="'.$jv_id.'">';
            $content .= '<input type="submit" value="View">';
            $content .= '</form></td>';
            if(partial_access($dBlink,  'admin')) {
                $content .= '<td><form method="post" onsubmit="return confirm_delete();" name="delete_jvs" action="">';
                $content .= '<input type="hidden" name="delete_jv" value="'.$jv_id.'">';
                $content .= '<input type="submit" value="Delete">';
                $content .= '</form></td>';
            }
            $content .= '</tr>';
        }
        echo $content;
    }//list_jv ends here.

	function jv_summary_old($dBlink ,$from, $to) {
		$query = "SELECT * from journal_voucher WHERE company_id='".$_SESSION['company_id']."' AND date between '".$from."' AND '".$to."' ORDER by jv_id ASC";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$content = '';
		$total = 0;
		while($row = mysqli_fetch_array($result)) {
			extract($row);
			$content .= '<tr>';
			$content .= '<td>'.$jv_id.'</td>';
			$content .= '<td>'.$date.'</td>';
			$content .= '<td>'.$jv_id_manual.'</td>';
			$content .= '<td>'.$jv_title.'</td>';
			$content .= '<td>'.$jv_description.'</td>';
			$content .= '<td>'.$jv_type.'</td>';
			//getting user name first and last.
			$query_user = "SELECT * from users WHERE user_id='".$user_id."'";
			$result_user = mysqli_query($dBlink ,$query_user) or die(mysql_error());
			$row_user = mysqli_fetch_array($result_user);
			//end of getting user.
			$content .= '<td>'.$row_user['first_name'].' '.$row_user['last_name'].'</td>';
			//getting balance
			$amount_query = "SELECT SUM(debit) from transactions WHERE jv_id='".$jv_id."'";
			$result_amount = mysqli_query($dBlink ,$amount_query) or die(mysql_error());
			$row = mysqli_fetch_array($result_amount);
			//end of getting balance of vouchar
			$content .= '<td class="align_right">'.number_format($row['SUM(debit)']).' '.$_SESSION['currency'].'</td>';
			$content .= '</tr>';
			$total = $total+$row['SUM(debit)'];
		}
		$content .= '<tr>';
		$content .= '<th class="align_right" colspan="6">Total Amount</th>';
		$content .= '<th class="align_right">'.number_format($total).' '.$_SESSION['currency'].'</th>';
		$content .= '</tr>';
		echo $content;
	}//jv_summary ends here.

    function jv_debit($dBlink){
        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id  ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo $content;

    }

    function jv_credit($dBlink){
        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id  ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo $content;
    }

    function jv_summary($dBlink ,$from, $to) {
        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id  ORDER BY jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'jv' ORDER BY jv.jv_id";
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'bcv' ORDER BY jv.jv_id
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.

    function avs_debit($dBlink){
        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo $content;

    }

    function avs_credit($dBlink){
        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo $content;
    }
    function avs_summary($dBlink ,$from, $to) {
        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'avs' ORDER BY jv.jv_id";
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'avs' ORDER BY jv.jv_id
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.
    function bcv_debit($dBlink){
        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo $content;

    }
    function bcv_credit($dBlink){
        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo $content;
    }
    function bcv_summary($dBlink ,$from, $to) {
        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'bcv' ORDER BY jv.jv_id";
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'bcv' ORDER BY jv.jv_id
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//bcv_summary ends here.
    function bpv_debit($dBlink){
        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo $content;

    }
    function bpv_credit($dBlink){
        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo $content;
    }
    function bpv_summary($dBlink ,$from, $to) {
        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'bpv' ORDER BY jv.jv_id";
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'bpv' ORDER BY jv.jv_id
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.
    function cpv_debit($dBlink ,$from, $to){
//        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id";
        $query = "SELECT
                        SUM(tr.credit) AS total
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv'
                        AND tr.date <= '$from' AND TR.date >= '$to'
                    ORDER BY
                        jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo number_format($content);

    }
    function jv_total($dBlink ,$from, $to){
//        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id";
        $query = "SELECT
                        SUM(tr.credit) AS total
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'jv'
                        AND tr.date <= '$from' AND TR.date >= '$to'
                    ORDER BY
                        jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;


        }

        echo number_format($content);

    }
    function cpv_credit($dBlink ,$from, $to){
//        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";
        $query = "SELECT
                        SUM(tr.credit) AS total
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv'
                        AND tr.date <= '$from' AND TR.date >= '$to'
                    ORDER BY
                        jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo number_format($content);
    }
    function cp_summary($dBlink ,$from, $to) {
//       SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND jv.jv_type = 'cpv'   ORDER BY jv.jv_id";
        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type IN ('cpv', 'crv') ORDER BY jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";-->
//      $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";
//      $query = "SELECT * FROM `journal_voucher` WHERE jv_type IN ('cpv','crv')";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
//            echo($debit);
//            exit;
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td>'.$jv_type.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.

    function balancesheet_summary($dBlink ,$from, $to) {
//       SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND jv.jv_type = 'cpv'   ORDER BY jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type IN ('cpv', 'crv') ORDER BY jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";-->
//      $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";
      $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type NOT IN ('cpv', 'crv') ORDER BY jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td>'.$jv_type.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.
    function cpv_summary($dBlink ,$from, $to) {
        $query = "SELECT
            jv.jv_id,
            ac.account_number,
            ac.account_title,
            jv.date,
            jv.jv_title,
            tr.debit,
            tr.credit
        FROM
            transactions tr,
            accounts ac,
            journal_voucher jv
        WHERE
            jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' AND
            jv.date >= '$from' AND jv.date <= '$to'
        ORDER BY
            jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.

    /**
     * @param $dBlink
     */
    function crv_debit($dBlink,$from,$to){
//        $query = "SELECT SUM(tr.debit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";
        $query = "SELECT
                        SUM(tr.debit) AS total
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv'
                        AND tr.date <= '$from' AND TR.date >= '$to'
                    ORDER BY
                        jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
            }

        echo $content;

    }
    function crv_credit($dBlink,$from,$to){
//        $query = "SELECT SUM(tr.credit) as total FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'cpv' ORDER BY jv.jv_id";
        $query = "SELECT
                        SUM(tr.credit) AS total
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv'
                        AND tr.date <= '$from' AND TR.date >= '$to'
                    ORDER BY
                        jv.jv_id";

        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';
        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            $content = $total;
        }
        echo $content;
    }
    function crv_summary($dBlink ,$from, $to) {
//        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
       // $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv'  ORDER BY jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";

        $query = "SELECT
            jv.jv_id,
            ac.account_number,
            ac.account_title,
            jv.date,
            jv.jv_title,
            tr.debit,
            tr.credit
        FROM
            transactions tr,
            accounts ac,
            journal_voucher jv
        WHERE
            jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND
            jv.date >= '$from' AND jv.date <= '$to'
        ORDER BY
            jv.jv_id";
        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td>'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<td class="align_right">'.$debit.'</td>';
                $content .= '<td class="align_right">'.$credit.'</td>';
            }


        }

        echo $content;
    }//jv_summary ends here.
//
////    function crv_summary($dBlink ,$from, $to) {
////        SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND jv.jv_type = 'cpv'   ORDER BY jv.jv_id";
////        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";
////        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";
//          $query = "SELECT * FROM journal_voucher WHERE jv_type IN ('cpv','crv')";
//
//        $result = mysqli_query($dBlink ,$query) or die(mysql_error());
//
//        $content = '';
//
//        $total = 0;
//        while($row = mysqli_fetch_array($result)) {
//            extract($row);
//            if($date >= $from ||   $date <= $to){
//                $content .= '<tr>';
//                $content .= '<td>'.$date.'</td>';
//                $content .= '<td>'.$account_number.'</td>';
//                $content .= '<td>'.$account_title.'</td>';
//                $content .= '<td>'.$jv_title.'</td>';
//                $content .= '<td class="align_right">'.$debit.'</td>';
//                $content .= '<td class="align_right">'.$credit.'</td>';
//            }
//
//
//        }
//
//        echo $content;
//    }//jv_summary ends here.
    function add_journal_voucher($dBlink ,$date, $jv_manual_id, $jv_title, $jv_description,$jv_type) {
		if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
			if($_SESSION['user_type'] != 'admin') {
				$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."' AND company_id='".$_SESSION['company_id']."'";
				$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
				$num_rows = mysql_num_rows($result_access);
				if($num_rows > 0) { 
					
				} else { 
					return 'You dont have access to this company!';
					exit();
				}
			}
		}
		$query = "INSERT INTO `journal_voucher` (
				`jv_id` ,
				`date` ,
				`jv_id_manual` ,
				`jv_title` ,
				`jv_description` ,
				`jv_type` ,
				`user_id` ,
				`company_id`
				)
				VALUES (
				NULL , '".$date."','".$jv_manual_id."', '".$jv_title."', '".$jv_description."', '".$jv_type."', '".$_SESSION['user_id']."', '".$_SESSION['company_id']."');";
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
				$jv_id = mysqli_insert_id($dBlink);
				return $jv_id;
	}//add jvs
	
	function add_transaction($dBlink ,$jv_id, $date, $account_id, $memo, $amount,$debit = 0,$credit = 0) {
//
					$query = "INSERT INTO `transactions` (
					`tr_id` ,
					`jv_id` ,
					`account_id` ,
					`date` ,
					`memo` ,
					`debit` ,
					`credit`
					)
					VALUES (
					NULL, '".$jv_id."', '".$account_id."', '".$date."', '".$memo."', '".$debit."', '".-$credit."'
					);";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
	}//add transaction ends here.
	
	function transaction_summary($dBlink ,$account_id, $from, $to) { 
		$query_op_bal = "SELECT SUM(debit), SUM(credit) from transactions WHERE account_id='".$account_id."' AND date < '".$from."'";
		$result_op_bal = mysqli_query($dBlink ,$query_op_bal) or die(mysql_error());
		$row_op_bal = mysqli_fetch_array($result_op_bal);
		$opening_balance = $row_op_bal['SUM(debit)']+$row_op_bal['SUM(credit)'];
		//opening balance ends here.
		
		$content = '<tr>
  				
  				<td>&nbsp;</td>
    			<td>&nbsp;</td>
			    <td>Opening balance</td>
    			<td class="align_right">&nbsp;</td>
    			<td class="align_right">&nbsp;</td>
			    <td class="align_right">'.number_format($opening_balance).'</td>
			    </tr>';
		$balance = $opening_balance;
		//opening balance row ends here.
		
		//getting transactions. 
		$query = "SELECT * from transactions WHERE account_id='".$account_id."' AND date between '".$from."' AND '".$to."' ORDER by tr_id ASC";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		while($row = mysqli_fetch_array($result)) {
			$balance = $balance+$row['debit']+$row['credit'];
			$content .= '<tr>';

			$content .= '<td>'.$row['jv_id'].'</td>';
			$content .= '<td>'.$row['date'].'</td>';
			$content .= '<td>'.$row['memo'].'</td>';
			$content .= '<td class="align_right">'.number_format($row['debit']).'</td>';
			$content .= '<td class="align_right">'.number_format($row['credit']).'</td>';
			$content .= '<td class="align_right">'.number_format($balance).'</td>';
			$content .= '</tr>';	
		}//transactions loop ends.
		
		echo $content;
		echo "<br>";
		echo "current balance : " .$balance. "";
	}//trasnactions summary ends here.

	
	function accounts_summary($dBlink ,$from, $to) {
		$account_query = "SELECT * from accounts WHERE company_id='".$_SESSION['company_id']."' ORDER by account_title ASC";
		$account_result = mysqli_query($dBlink ,$account_query) or die(mysql_error());
		
		$content = '';
		$total_op_bal = 0;
		$total_debit = 0;
		$total_credit = 0;
		
		while($account_row = mysqli_fetch_array($account_result)) {
			$query_op_bal = "SELECT SUM(debit), SUM(credit) from transactions WHERE account_id='".$account_row['account_id']."' AND date < '".$from."'";
			$result_op_bal = mysqli_query($dBlink ,$query_op_bal) or die(mysql_error());
			$row_op_bal = mysqli_fetch_array($result_op_bal);
			$opening_balance = $row_op_bal['SUM(debit)']+$row_op_bal['SUM(credit)'];
			//opening balance ends here.	
		$total_op_bal = $opening_balance+$total_op_bal;
		
		$query_trans = "SELECT SUM(debit), SUM(credit) from transactions WHERE account_id='".$account_row['account_id']."' AND date between '".$from."' AND '".$to."'";
		$result_trans = mysqli_query($dBlink ,$query_trans) or die(mysql_error());
		$row_trans = mysql_fetch_array($result_trans);
		//opening balance ends here.
		
		$content .= '<tr>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_id'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_number'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_title'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_type'].'</td>';
		$content .= '<td scope="col" class="align_right">'.number_format($opening_balance).'</td>';
		$content .= '<td scope="col" class="align_right">'.number_format($row_trans['SUM(debit)']).'</td>';
		$content .= '<td scope="col" class="align_right">'.number_format($row_trans['SUM(credit)']).'</td>';
		$content .= '<td scope="col" class="align_right">';
		$content .= number_format($opening_balance+$row_trans['SUM(debit)']+$row_trans['SUM(credit)']);
		$content .= '</td>';
		$content .= '</tr>';
		
		$total_debit = $total_debit+$row_trans['SUM(debit)'];
		$total_credit = $total_credit+$row_trans['SUM(credit)'];
		}		
		$content .= '<tr>';
		$content .= '<th class="align_right" colspan="4">Total</th>';
		$content .= '<th class="align_right">'.number_format($total_op_bal).'</th>';
		$content .= '<th class="align_right">'.number_format($total_debit).'</th>';
		$content .= '<th class="align_right">'.number_format($total_credit).'</th>';
		$content .= '<th class="align_right" colspan="4">'.number_format($total_debit+$total_credit).'</th>';
		echo $content;
	}//accounts summary ends here.
	
		function trial_balance($dBlink ,$from, $to) {
		$account_query = "SELECT * from accounts WHERE company_id='".$_SESSION['company_id']."' ORDER by account_type ASC";
		$account_result = mysqli_query($dBlink ,$account_query) or die(mysql_error());
		
		$content = '';
		$total_op_bal = 0;
		$total_debit = 0;
		$total_credit = 0;
		
		while($account_row = mysql_fetch_array($account_result)) {
		$query_trans = "SELECT SUM(debit), SUM(credit) from transactions WHERE account_id='".$account_row['account_id']."' AND date between '".$from."' AND '".$to."'";
		$result_trans = mysqli_query($dBlink ,$query_trans) or die(mysql_error());
		$row_trans = mysql_fetch_array($result_trans);
		//opening balance ends here.
		
		$content .= '<tr>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_id'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_number'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_title'].'</td>';
		$content .= '<td scope="col" class="align_left">'.$account_row['account_type'].'</td>';
		$content .= '<td scope="col" class="align_right">';
		if(($row_trans['SUM(debit)']+$row_trans['SUM(credit)']) < 0) { 
			$tr_type = 'CR';
		} else { 
			$tr_type = 'DR';
		}
		$content .= number_format($row_trans['SUM(debit)']+$row_trans['SUM(credit)']).$tr_type;
		$content .= '</td>';
		$content .= '</tr>';
		
		$total_debit = $total_debit+$row_trans['SUM(debit)'];
		$total_credit = $total_credit+$row_trans['SUM(credit)'];
		}		
		$content .= '<tr>';
		$content .= '<th class="align_right" colspan="4">Total</th>';
		$content .= '<th class="align_right" colspan="4">'.number_format($total_debit+$total_credit).'</th></tr>';
		echo $content;
	}//Trial Balance Ends here.


    public function detail_summary($dBlink ,$from, $to) {
//        echo $from;
//        ex/it;
//       SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND jv.jv_type = 'cpv'   ORDER BY jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type IN ('cpv', 'crv') ORDER BY jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";-->
//      $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type NOT IN ('cpv', 'crv') ORDER BY jv.jv_id";

        $query = "SELECT
                    jv.jv_id,
                    ac.account_number,
                    ac.account_title,
                    tr.memo,
                    jv.date,
                    jv.jv_title,
                    jv.jv_type,
                    tr.debit,
                    tr.credit
                FROM
                    transactions tr,
                    accounts ac,
                    journal_voucher jv
                WHERE
                    jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND
                     jv.date >= '$from' AND jv.date <= '$to'
                ORDER BY
                    tr.jv_id";
//        echo $query;
//        exit;

        $result = mysqli_query($dBlink ,$query) or die(mysqli_error($dBlink));

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<th >'.$date.'</th>';
                //$content .= '<td>'.$account_number.'</td>';
                $content .= '<th>'.$account_title.'</th>';
                $content .= '<th>'.$memo.'</th>';
//                $content .= '<td>'.$jv_title.'</td>';
                $content .= '<th>'.$jv_type.'</th>';
                $content .= '<th class="align_left">'.number_format($debit).'</th>';
                $content .= '<th class="align_left">'.number_format($credit).'</th>';
                $content .= '</tr>';

            }


        }

        echo $content;
        //echo '..............................................................................................';
//        $jv_count = Jvs::detail_jv($dBlink ,$from, $to);
//echo $jv_count;



    }//jv_summary ends here.

    public function detail_jv($dBlink ,$from, $to){

        $query = "SELECT
                    jv.jv_id,
                    ac.account_number,
                    ac.account_title,
                    tr.memo,
                    jv.date,
                    jv.jv_title,
                    jv.jv_type,
                    tr.debit,
                    tr.credit
                FROM
                    transactions tr,
                    accounts ac,
                    journal_voucher jv
                WHERE
                    jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND
                     jv.date >= '$from' AND jv.date <= '$to' AND jv.jv_type = 'jv'
                ORDER BY
                    ac.account_number";


        $result = mysqli_query($dBlink ,$query) or die(mysql_error());

        $content = '';

        $total = 0;
        while($row = mysqli_fetch_array($result)) {
            extract($row);
            if($date >= $from ||   $date <= $to){
                $content .= '<tr>';
                $content .= '<td >'.$date.'</td>';
                $content .= '<td>'.$account_number.'</td>';
//                $content .= '<td>'.$account_title.'</td>';
                $content .= '<td>'.$memo.'</td>';
//                $content .= '<td>'.$jv_title.'</td>';
//                $content .= '<td>'.$jv_type.'</td>';
                $content .= '<td class="align_left">'.number_format($debit).'</td>';
                $content .= '<td class="align_left">'.number_format($credit).'</td>';
            }


        }

        echo $content;
    }

     public function ps_summary($dBlink ,$from, $to) {
//        echo $from;
//        exit;
//       SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' ORDER BY jv.jv_id
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type = 'crv' AND jv.jv_type = 'cpv'   ORDER BY jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type IN ('cpv', 'crv') ORDER BY jv.jv_id";
//        $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id order by jv.jv_id";-->
//      $query = "select jv.jv_id , ac.account_number , ac.account_title , jv.date , jv.jv_title , tr.debit , tr.credit from transactions tr , accounts ac , journal_voucher jv where jv.jv_id = tr.jv_id and tr.account_id = ac.account_id AND date between '".$from."' AND '".$to."' order by jv.jv_id";
//        $query = "SELECT jv.jv_id, ac.account_number, ac.account_title, jv.date, jv.jv_title, jv.jv_type, tr.debit, tr.credit FROM transactions tr, accounts ac, journal_voucher jv WHERE jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type NOT IN ('cpv', 'crv') ORDER BY jv.jv_id";
        $crv_count = Jvs::get_crv_count($dBlink ,$from, $to);
        $cpv_count = Jvs::get_cpv_count($dBlink ,$from, $to);
        $brv_count = Jvs::get_brv_count($dBlink ,$from, $to);
        $bpv_count = Jvs::get_bpv_count($dBlink ,$from, $to);
        $jv_count = Jvs::get_jv_count($dBlink ,$from, $to);

//        $crv_count = get_crv_count($dBlink ,$from, $to);

       echo $cpv_count ;
       echo $crv_count ;
       echo $brv_count ;
       echo $bpv_count ;
       echo $jv_count ;
//       echo '<br>';
//       echo $cpv_count;







//        $content = '';
//        $content .= '<tr>';
//        $content .= '<td>'.$date.'</td>';
//        $content .= '<td>'.$account_number.'</td>';
//        $content .= '<td>'.$account_title.'</td>';
//        $content .= '<td>'.$memo.'</td>';
////                $content .= '<td>'.$jv_title.'</td>';
//        $content .= '<td>'.$jv_type.'</td>';
//        $content .= '<td class="align_left">'.$debit.'</td>';
//        $content .= '<td class="align_left">'.$credit.'</td>';
//        $content .= '</tr>';
//
//        $total = 0;
//        while($row = mysqli_fetch_array($result)) {
//            extract($row);
//            if($date >= $from ||   $date <= $to){
//                $content .= '<tr>';
//                $content .= '<td>'.$date.'</td>';
//                $content .= '<td>'.$account_number.'</td>';
//                $content .= '<td>'.$account_title.'</td>';
//                $content .= '<td>'.$memo.'</td>';
////                $content .= '<td>'.$jv_title.'</td>';
//                $content .= '<td>'.$jv_type.'</td>';
//                $content .= '<td class="align_left">'.$debit.'</td>';
//                $content .= '<td class="align_left">'.$credit.'</td>';
//                $content .= '</tr>';
//            }
//
//
//        }

//        echo $content;
    }//jv_summary ends here.

    public function get_crv_count($dBlink ,$from, $to){
        $crv_query = "SELECT
                        jv.jv_type,
                        floor(count(tr.tr_id)/2) as crv ,
                        SUM(tr.debit) as value
                        
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type='crv' AND jv.date >= '$from' AND jv.date <= '$to'
                    ORDER BY
                        jv.jv_id";

        $crv_result = mysqli_query($dBlink, $crv_query) or die(mysql_error());
        $content = '';
        while ($row = mysqli_fetch_array($crv_result)) {
            extract($row);
//            print_r($row);

            $content .= '<tr>';

            $content .= '<td >' . $jv_type . '</td>';
            $content .= '<td class="align_right">' . $crv . '</td>';
            //                $content .= '<td>'.$jv_title.'</td>';
            $content .= '<td class="align_right">' . $value . '</td>';

            $content .= '</tr>';

        }
        return $content;
    }
public function get_cpv_count($dBlink ,$from, $to)
{

    $cpv_query = "SELECT
                        jv.jv_type,
                        floor(count(tr.tr_id)/2) as cpv ,
                        SUM(tr.debit) as value
                        
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type='cpv' AND jv.date >= '$from' AND jv.date <= '$to'
                    ORDER BY
                        jv.jv_id";

    $cpv_result = mysqli_query($dBlink, $cpv_query) or die(mysql_error());
    $content = '';
    while ($row = mysqli_fetch_array($cpv_result)) {
        extract($row);
//        print_r($row);

        $content .= '<tr>';

        $content .= '<td >' . $jv_type . '</td>';
        $content .= '<td class="align_right">' . $cpv . '</td>';
        //                $content .= '<td>'.$jv_title.'</td>';
        $content .= '<td class="align_right">' . $value . '</td>';

        $content .= '</tr>';

    }
    return $content;
}
public function get_brv_count($dBlink ,$from, $to)
{

    $brv_query = "SELECT
                        jv.jv_type,
                        floor(count(tr.tr_id)/2) as brv ,
                        SUM(tr.debit) as value
                        
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type='brv' AND jv.date >= '$from' AND jv.date <= '$to'
                    ORDER BY
                        jv.jv_id";

    $brv_result = mysqli_query($dBlink, $brv_query) or die(mysql_error());
    $content = '';
    while ($row = mysqli_fetch_array($brv_result)) {
        extract($row);
//        print_r($row);

        $content .= '<tr>';
        if(!isset($jv_type)){
            $jv_type = 'brv';
        }
        $content .= '<td >' . $jv_type . '</td>';
        $content .= '<td class="align_right">' . $brv . '</td>';
        //                $content .= '<td>'.$jv_title.'</td>';
        $content .= '<td class="align_right">' . $value . '</td>';

        $content .= '</tr>';

    }
    return $content;
}
public function get_bpv_count($dBlink ,$from, $to)
{

    $bpv_query = "SELECT
                        jv.jv_type,
                        floor(count(tr.tr_id)/2) as bpv ,
                        SUM(tr.debit) as value
                        
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type='bpv' AND jv.date >= '$from' AND jv.date <= '$to'
                    ORDER BY
                        jv.jv_id";

    $bpv_result = mysqli_query($dBlink, $bpv_query) or die(mysql_error());
    $content = '';
    while ($row = mysqli_fetch_array($bpv_result)) {
        extract($row);
//        print_r($row);

        $content .= '<tr>';
        if(!isset($jv_type)){
            $jv_type = 'bpv';
        }
        $content .= '<td >' . $jv_type . '</td>';
        $content .= '<td class="align_right">' . $bpv . '</td>';
        //                $content .= '<td>'.$jv_title.'</td>';
        $content .= '<td class="align_right">' . $value . '</td>';

        $content .= '</tr>';

    }
    return $content;
}

//
public function get_jv_count($dBlink ,$from, $to)
{

    $vpv_query = "SELECT
                        jv.jv_type,
                        floor(count(tr.tr_id)/2) as bpv ,
                        SUM(tr.debit) as value
                        
                    FROM
                        transactions tr,
                        accounts ac,
                        journal_voucher jv
                    WHERE
                        jv.jv_id = tr.jv_id AND tr.account_id = ac.account_id AND jv.jv_type='jv' AND jv.date >= '$from' AND jv.date <= '$to'
                    ORDER BY
                        jv.jv_id";

    $vpv_result = mysqli_query($dBlink, $vpv_query) or die(mysql_error());
    $content = '';
    while ($row = mysqli_fetch_array($vpv_result)) {
        extract($row);
//        print_r($row);

        $content .= '<tr>';
        if(!isset($jv_type)){
            $jv_type = 'vpv';
        }
        $content .= '<td >' . $jv_type . '</td>';
        $content .= '<td class="align_right">' . $bpv . '</td>';
        //                $content .= '<td>'.$jv_title.'</td>';
        $content .= '<td class="align_right">' . $value . '</td>';

        $content .= '</tr>';

    }
    return $content;
}

//


}//class ends here.