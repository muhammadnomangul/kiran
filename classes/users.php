<?php
//users Class

class Users {
	public $user_id;
	public $first_name;
	public $last_name;
	public $date_of_birth;
	public $address1;
	public $address2;
	public $city;
	public $state;
	public $country;
	public $zip_code;
	public $mobile;
	public $phone;
	public $email;
	public $profile_image;
	public $description;
	public $status;
	public $user_type;
	
	function subscriber_options($dBlink) {
		$query = "SELECT * from users WHERE user_type='subscriber' ORDER by first_name ASC";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$options = '';
		while($row = mysqli_fetch_array($result)) {
			extract($row);
			$options .= '<option value="'.$row['user_id'].'">'.$row['user_id'].' | '.$row['first_name'].'</option>';
		}//while loop ends here.
		echo $options;
	}
	
	function update_user($user_id, $user_type_ses, $first_name, $last_name, $date_of_birth, $address1, $address2, $city, $state, $country, $zip_code, $mobile, $phone, $email, $password, $profile_image, $description, $status, $user_type) {
		if($user_type_ses == 'admin') { 
			if($password == '') {
			$query = 'UPDATE users SET
   	    			first_name = "'.$first_name.'",
					last_name = "'.$last_name.'",
					date_of_birth = "'.$date_of_birth.'",
					address1 = "'.$address1.'",
					address2 = "'.$address2.'",
					city = "'.$city.'",
					state = "'.$state.'",
					country = "'.$country.'",
					zip_code = "'.$zip_code.'",
					mobile = "'.$mobile.'",
					phone = "'.$phone.'",
					email = "'.$email.'",
					profile_image = "'.$profile_image.'",
					description = "'.$description.'",
					status = "'.$status.'",
					user_type = "'.$user_type.'"
			WHERE user_id="'.$user_id.'"';
			} else { 
			$query = 'UPDATE users SET
   	    			first_name = "'.$first_name.'",
					last_name = "'.$last_name.'",
					date_of_birth = "'.$date_of_birth.'",
					address1 = "'.$address1.'",
					address2 = "'.$address2.'",
					city = "'.$city.'",
					state = "'.$state.'",
					country = "'.$country.'",
					zip_code = "'.$zip_code.'",
					mobile = "'.$mobile.'",
					phone = "'.$phone.'",
					email = "'.$email.'",
					password = "'.md5($password).'",
					profile_image = "'.$profile_image.'",
					description = "'.$description.'",
					status = "'.$status.'",
					user_type = "'.$user_type.'"
			WHERE user_id="'.$user_id.'"';
			}
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());		  
			return 'User updated Successfuly!';

		} else { 
			return 'You dont have permission to update this user.';
		}
	}//update user ends here.
	
	function set_user($user_id, $user_type, $login_user) {
		 if($user_type == 'admin') { 
			$query = 'SELECT * from users WHERE user_id="'.$user_id.'"'; 
		 } else if($user_id == $login_user) { 
		 	$query = 'SELECT * from users WHERE user_id="'.$user_id.'"';
		 } else { 
		 	echo 'You are trying to do you are not allowed for.';
		 }
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysql_fetch_array($result);
		$this->user_id = $row['user_id'];
		$this->first_name = $row['first_name'];
		$this->last_name = $row['last_name'];
		$this->date_of_birth = $row['date_of_birth'];
		$this->address1 = $row['address1'];
		$this->address2 = $row['address2'];
		$this->city = $row['city'];
		$this->state = $row['state'];
		$this->country = $row['country'];
		$this->zip_code = $row['zip_code'];
		$this->mobile = $row['mobile'];
		$this->phone = $row['phone'];
		$this->email = $row['email'];
		$this->profile_image = $row['profile_image'];
		$this->description = $row['description'];
		$this->status = $row['status'];
		$this->user_type = $row['user_type'];
	}//level set ends here.
	

function list_users($user_type) {
		if($user_type == 'admin') { 
			$query = 'SELECT * from users ORDER by first_name ASC';
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$content = '';
			$count = 0;
			while($row = mysql_fetch_array($result)) { 
				extract($row);
				$count++;
				if($count%2 == 0) { 
					$class = 'even';
				} else { 
					$class = 'odd';
				}
				$content .= '<tr class="'.$class.'">';
				$content .= '<td>';
				$content .= $user_id;
				$content .= '</td><td>';
				$content .= $first_name;
				$content .= '</td><td>';
				$content .= $last_name;
				$content .= '</td><td>';
				$content .= $city;
				$content .= '</td><td>';
				$content .= $country;
				$content .= '</td><td>';
				$content .= $email;
				$content .= '</td><td>';
				$content .= ucfirst($status);
				$content .= '</td><td>';
				$content .= ucfirst($user_type);
				$content .= '</td><td>';
				$content .= '<form method="post" name="edit" action="manage_users.php">';
				$content .= '<input type="hidden" name="edit_user" value="'.$user_id.'">';
				$content .= '<input type="submit" value="Edit">';
				$content .= '</form>';
				$content .= '</td><td>';
				$content .= '<form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$content .= '<input type="hidden" name="delete_user" value="'.$user_id.'">';
				$content .= '<input type="submit" value="Delete">';
				$content .= '</form>';
				$content .= '</td>';
				$content .= '</tr>';
				unset($class);
			}//loop ends here.
			
		} else { 
			$content = 'You cannot view list of users.';
		}	
		echo $content;
	}//list_levels ends here.

	function delete_user($user_type, $user_id) {
		if($user_type == 'admin') {
			$query = 'DELETE from users WHERE user_id="'.$user_id.'"';
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$message = 'User was deleted successfuly!';	
		} else { 
			$message = 'You dont have access to delete this user.';
		}	
		return $message;
	}//delete level ends here.

function match_confirm_code($confirmation_code,$user_id){
			//Getting Confirmation Code from database.
			$query = "SELECT * from users WHERE user_id='".$user_id."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			if($row['activation_key'] == $confirmation_code){
				if($row['status'] == 'suspend'||$row['status'] == 'ban'){
					$message='Your account has been suspended. Please contact the administrator for help';
					
				} else {
					$status = 'activate';
					$query = 'UPDATE users SET status="'.$status.'",activation_key="" WHERE user_id="'.$user_id.'"';
					$row = mysqli_query($dBlink ,$query) or die(mysql_error());
			     	$message='Congratulations! You are activated successfuly now you can use email and password to login and use our services.';
				} 
			} else {
				  $message='Your account cannot be activated';
			}
			return $message;
}//function  close

function add_user($dBlink,$first_name, $last_name, $date_of_birth, $address1, $address2, $city, $state, $country, $zip_code, $mobile, $phone, $email, $password, $profile_image, $description, $status, $user_type) {
	//Check if user already exist
			$query = "SELECT * from users WHERE email='".$email."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			
			$num_user = mysqli_num_rows($result);
			
			if($num_user > 0) { 
				return 'User could not added Email <strong>'.$email.'</strong> already registered.';
				exit();
			}
			$registration_date = date('Y-m-d');
			$password_con = md5($password);
			
			//Running Query to add user.
			$query = "INSERT into users VALUES(NULL, '".$first_name."', '".$last_name."', '".$date_of_birth."', '".$address1."', '".$address2."', '".$city."', '".$state."', '".$country."', '".$zip_code."', '".$mobile."', '".$phone."', '".$email."', '".$password_con."', '".$profile_image."', '".$description."', '".$status."', '', '".date('Y-m-d')."', '".$user_type."')";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			//Email to user
			$site_url = get_option('site_url');
					
			$email_message = "Your account have been registered.<br />";
			$email_message .= "Please use the following details to sign in on our website";
			$email_message .= "<a href='".$site_url."'>Confirm Email Address</a>";
			$email_message .= "Email/Username: <strong>".$email."</strong>";
			$email_message .= "Password: <strong>".$password."</strong>";			
			
			$message = $email_message;
			$mailto = $email;
			//getting set email addresses from database.
			$from_email = get_option($dBlink,'email_from');
			$reply_to = get_option($dBlink,'email_to');
			
			$mailheaders = "From:".$from_email;
			$mailheaders .="Reply-To:".$reply_to;
			$from = $from_email;
			$subject = 'Registration Details';

			$headers = "FROM: ".$from;
					$semi_rand = md5(time());
					$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
			
					$headers .= "\nMIME-Version: 1.0\n" .
					"Content-Type: multipart/mixed;\n" .
					" boundary=\"{$mime_boundary}\"";
			
					$message .= "This is a multi-part message in MIME format.\n\n" .
					"--{$mime_boundary}\n" .
					"Content-Type:text/html; charset=\"iso-8859-1\"\n" .
					"Content-Transfer-Encoding: 7bit\n\n" .
					$message . "\n\n";
					$message .= "--{$mime_boundary}\n" .
					"Content-Type: {$fileatt_type};\n" .
					" name=\"{$filename}\"\n" .
					"Content-Transfer-Encoding: base64\n\n" .
					mail($mailto, $subject, $message, $headers);
	return 'User added successfuly details are sent to Email.'.$email;
}//add user function ends here.

function register_user($dBlink,$first_name, $last_name, $email, $password){
			//Check if user already exist
			$query = "SELECT * from users WHERE email='".$email."'";
			$result = mysqli_query($dBlink,$query) or die(mysql_error());
			
			$num_user = mysqli_num_rows($result);
			
			if($num_user > 0) { 
				return 'User could not added Email <strong>'.$email.'</strong> already registered.';
				exit();
			}
			$registration_date = date('Y-m-d');
			$password = md5($password);
			$activation_key = substr(md5(uniqid(rand(), true)), 16, 16);
			
			$user_type = "subscriber";
			$status = "deactivate";
			//adding user into database
			$query = "INSERT INTO users ".
					       "(user_id, first_name,last_name,email,password,activation_key,date_register,user_type,status) ".
       						"VALUES ".
       						"(NULL,'$first_name','$last_name','$email','$password','$activation_key','$registration_date','$user_type','$status')";
			$result = mysqli_query($dBlink,$query) or die(mysql_error());
			$user_id = mysqli_insert_id();
			//Email to user
//			$site_url = get_option('site_url');
			
					
//			$email_message = "Thank you for registration.<br />";
//			$email_message .= "Kindly click the link below to confirm your account and start using our services.<br />";
//			$email_message .= "<a href='".$site_url."login.php?confirmation_code=".$activation_key."&user_id=".$user_id."'>Confirm Email Address</a>";
//			$email_message .= "<br><br>Thank you again. Please contact us if you need any assistance.";
//
//			$message = $email_message;
//			$mailto = $email;
//			//getting set email addresses from database.
//			$from_email = get_option('email_from');
//			$reply_to = get_option('email_to');
//
//			$mailheaders = "From:".$from_email;
//			$mailheaders .="Reply-To:".$reply_to;
//			$from = $from_email;
//			$subject = 'Confirm your email id.';
//
//			$headers = "FROM: ".$from;
//					$semi_rand = md5(time());
//					$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
//
//					$headers .= "\nMIME-Version: 1.0\n" .
//					"Content-Type: multipart/mixed;\n" .
//					" boundary=\"{$mime_boundary}\"";
//
//					$message .= "This is a multi-part message in MIME format.\n\n" .
//					"--{$mime_boundary}\n" .
//					"Content-Type:text/html; charset=\"iso-8859-1\"\n" .
//					"Content-Transfer-Encoding: 7bit\n\n" .
//					$message . "\n\n";
//					$message .= "--{$mime_boundary}\n" .
//					"Content-Type: {$fileatt_type};\n" .
//					" name=\"{$filename}\"\n" .
//					"Content-Transfer-Encoding: base64\n\n" .
//					mail($mailto, $subject, $message, $headers);
	return 'Your registration is done. Now please check your email inbox to confirm your email address Thank you.';
	}//register_user ends here.
	
	function login_user($dBlink,$email, $password) {
		 $password = md5($password); //Converting input password to md5 to match in database.
		 
		 $query = "SELECT * from users WHERE email='".$email."'";
		 $result = mysqli_query($dBlink ,$query) or die(mysql_error());
		 $num_rows = mysqli_num_rows($result);
		 
		 if($num_rows > 0) { 
		 	$row = mysqli_fetch_array($result);
			
			if($row['password'] == $password) {
				if($row['status'] == 'deactivate') { 
					$message = 'Your account is not activated yet please confirm your Email address to activate your account!';
				} else if($row['status'] == 'activate'){
					extract($row);
					$this->user_id = $user_id; 
					$this->first_name = $first_name;
					$this->last_name = $last_name;
					$this->email = $email;
					$this->status = $status;
					$this->user_type = $user_type;
					$message = 1;
				} else { 
					$message = "Your are ban or suspended please contact administrator to login!";
				}
			} else { 
				$message = 'Password do not match!';
			}
			
		 } else { 
		 	$message = "Sorry but we could not find this email address.";
		 }
		 return $message;
	}
	
function forgot_user($email){
	 $query = "SELECT * from users WHERE email='".$email."'";
	 $result = mysqli_query($dBlink ,$query) or die(mysql_error());
	  $num_rows = mysql_num_rows($result);
		 
		 if($num_rows > 0) { 
		 	$row = mysql_fetch_array($result);
			$user_id =$row['user_id'];
		 } else {
			return 'The email address is not in our system';
			exit();
			}
	$activation_key = substr(md5(uniqid(rand(), true)), 16, 16);
	$query = 'UPDATE users SET activation_key="'.$activation_key.'" WHERE email="'.$email.'"';
	$result = mysqli_query($dBlink ,$query) or die(mysql_error());

	$site_url = get_option('site_url');
	$email_message = "Reset your password.<br />";
	$email_message .= "Kindly click the link below to reset your password.<br />";
	$email_message .= "<a href='".$site_url."forgot.php?confirmation_code=".$activation_key."&user_id=".$user_id."'>Confirm Email Address</a>";
	$email_message .= "<br><br>Thank you again. Please contact us if you need any assistance.";			
	$message = $email_message;
	$mailto = $email;
	//getting set email addresses from database.
	$from_email = get_option('email_from');
	$reply_to = get_option('email_to');
			
	$mailheaders = "From:".$from_email;
	$mailheaders .="Reply-To:".$reply_to;
	$from = $from_email;
	$subject = 'Reset your Password.';

	$headers = "FROM: ".$from;
	$semi_rand = md5(time());
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
			
	$headers .= "\nMIME-Version: 1.0\n" .
	"Content-Type: multipart/mixed;\n" .
	" boundary=\"{$mime_boundary}\"";
			
	$message .= "This is a multi-part message in MIME format.\n\n" .
	"--{$mime_boundary}\n" .
	"Content-Type:text/html; charset=\"iso-8859-1\"\n" .
	"Content-Transfer-Encoding: 7bit\n\n" .
	$message . "\n\n";
	$message .= "--{$mime_boundary}\n" .
	"Content-Type: {$fileatt_type};\n" .
	" name=\"{$filename}\"\n" .
	"Content-Transfer-Encoding: base64\n\n" .
	mail($mailto, $subject, $message, $headers);
	
	return 'Please check your email and proceed to reset your password.';
	}//forgot password function endsh ere.
	
	function reset_pass_user($user_id,$confirmation_code,$new_pass){
		$query = "SELECT * from users WHERE user_id='".$user_id."'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysql_fetch_array($result);
		
		$new_pass = md5($new_pass);
		if($confirmation_code==$row['activation_key']){
				$query = 'UPDATE users SET password="'.$new_pass.'",activation_key="" WHERE user_id="'.$user_id.'"';
				$row = mysqli_query($dBlink ,$query) or die(mysql_error());
				$message ="Your Password has been reset please use new password to login.";
			} else { 
				$message ="Your activation key is expired and password cannot be reset.";
			}
			return $message;
		}//reset password function ends here.

function edit_profile($dBlink,$user_id, $first_name, $last_name, $date_of_birth, $address1, $address2, $city, $state, $country, $zip_code, $mobile, $phone, $email, $password, $profile_image, $description) {
		if($password == '') {
			$query = 'UPDATE users SET
   	    			first_name = "'.$first_name.'",
					last_name = "'.$last_name.'",
					date_of_birth = "'.$date_of_birth.'",
					address1 = "'.$address1.'",
					address2 = "'.$address2.'",
					city = "'.$city.'",
					state = "'.$state.'",
					country = "'.$country.'",
					zip_code = "'.$zip_code.'",
					mobile = "'.$mobile.'",
					phone = "'.$phone.'",
					email = "'.$email.'",
					profile_image = "'.$profile_image.'",
					description = "'.$description.'"
			WHERE user_id="'.$user_id.'"';
			} else { 
			$query = 'UPDATE users SET
   	    			first_name = "'.$first_name.'",
					last_name = "'.$last_name.'",
					date_of_birth = "'.$date_of_birth.'",
					address1 = "'.$address1.'",
					address2 = "'.$address2.'",
					city = "'.$city.'",
					state = "'.$state.'",
					country = "'.$country.'",
					zip_code = "'.$zip_code.'",
					mobile = "'.$mobile.'",
					phone = "'.$phone.'",
					email = "'.$email.'",
					password = "'.md5($password).'",
					profile_image = "'.$profile_image.'",
					description = "'.$description.'"
			WHERE user_id="'.$user_id.'"';
			}
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());		  
			return 'User updated Successfuly!';
	}//update user ends here.	
	
	function get_total_users($dBlink,$condition) {
		if($_SESSION['user_type'] == 'admin') {
			if($condition == 'all') { 
				$query = "SELECT * from users";
			} else { 
				$query = "SELECT * from users WHERE status='".$condition."'";
			}
			
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$num_rows = mysqli_num_rows($result);
			echo $num_rows;
		} else { 
			echo 'You cannot view this list.';
		}
	}//prints total registered users.
			
}//class ends here.