<?php
//user levels Class

class Userlevel {
	public $level_name;
	public $level_description;
	public $level_page;
	
	function set_level($dBlink ,$level_id) { 
		$query = 'SELECT * from user_level WHERE level_id="'.$level_id.'"';
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysqli_fetch_array($result);
		$this->level_name = $row['level_name'];
		$this->level_description = $row['level_description'];
		$this->level_page = $row['level_page'];
	}//level set ends here.
	
	function update_user_level($dBlink ,$level_id, $level_name, $level_description, $level_page) { 
		$query = 'UPDATE user_level SET
				  level_name = "'.$level_name.'",
				  level_description = "'.$level_description.'",
				  level_page = "'.$level_page.'"
				   WHERE level_id="'.$level_id.'"';
				  
		$result = mysqli_query($dBlink ,$dBlink ,$query) or die(mysql_error());		  
		return 'User level updated Successfuly!';
	}//update user level ends here.
	
	function add_user_level($dBlink ,$level_name, $level_description, $level_page) { 
		//checking if level already exist.
		$query = "SELECT * from user_level WHERE level_name='".$level_name."'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$num_rows = mysql_num_rows($result);
		
		if($num_rows > 0) { 
			$message = 'You cannot add duplicate user levels.';
		} else { 
			$level_page = stripslashes($level_page);
			$query = "INSERT into user_level VALUES(NULL, '".$level_name."', '".$level_description."', '".$level_page."')";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$message = 'User level added successfuly!';
		}
		return $message;
	}//add_user_level ends here.
	
	function list_levels($dBlink ,$user_type) {
		if($user_type == 'admin') { 
			$query = 'SELECT * from user_level ORDER by level_name ASC';
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$content = '';
			$count = 0;
			while($row = mysql_fetch_array($result)) { 
				extract($row);
				$count++;
				if($count % 2 == 0) { 
					$class = 'even';
				} else { 
					$class = 'odd';
				}
				$content .= '<tr class="'.$class.'">';
				$content .= '<td>';
				$content .= $level_id;
				$content .= '</td><td>';
				$content .= $level_name;
				$content .= '</td><td>';
				$content .= $level_description;
				$content .= '</td><td>';
				$content .= $level_page;
				$content .= '</td><td>';
				$content .= '<form method="post" name="edit" action="manage_user_level.php">';
				$content .= '<input type="hidden" name="edit_level" value="'.$level_id.'">';
				$content .= '<input type="submit" value="Edit">';
				$content .= '</form>';
				$content .= '</td><td>';
				$content .= '<form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$content .= '<input type="hidden" name="delete_level" value="'.$level_id.'">';
				$content .= '<input type="submit" value="Delete">';
				$content .= '</form>';
				$content .= '</td>';
				$content .= '</tr>';
				unset($class);
			}//loop ends here.
			
		} else { 
			$content = 'You cannot view list of levels.';
		}	
		echo $content;
	}//list_levels ends here.
	
	function delete_level($dBlink ,$user_type, $level_id) {
		if($user_type == 'admin') {
			$query = 'DELETE from user_level WHERE level_id="'.$level_id.'"';
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$message = 'Level was deleted successfuly!';	
		} else { 
			$message = 'You dont have access to delete this user level.';
		}	
		return $message;
	}//delete level ends here.
	
	function userlevel_options($dBlink ,$user_type) {
		$query = 'SELECT * from user_level ORDER by level_name ASC';
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
        $options = '';
		if($user_type != '') { 
			while($row = mysqli_fetch_array($result)) {
				if($user_type == $row['level_name']) {
				$options .= '<option selected="selected" value="'.$row['level_name'].'">'.ucfirst($row['level_name']).'</option>';
				} else { 
				$options .= '<option value="'.$row['level_name'].'">'.ucfirst($row['level_name']).'</option>';
				}
			}
		} else { 
			while($row = mysqli_fetch_array($result)) {
				$options .= '<option value="'.$row['level_name'].'">'.ucfirst($row['level_name']).'</option>';
			}
		}
		echo $options;	
	}//return user level options for select
	
	function get_level_info($dBlink ) { 
		if($_SESSION['user_type'] == 'admin') { 
				$table = '';
				$table .= '<tr>';
				$table .= '<td>Admin</td>';
				$table .= '<td>dashboard.php</td>';
				$query = "SELECT * from users WHERE user_type='admin'";
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
				$num_rows = mysql_num_rows($result);
				$table .= '<td>'.$num_rows.'</td>';
				$table .= '</tr>';
			
			$query = "SELECT * from user_level ORDER by level_name ASC";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			
			while($row = mysql_fetch_array($result)) { 
				$table .= '<tr>';
				$table .= '<td>'.ucfirst($row['level_name']).'</td>';
				$table .= '<td>'.$row['level_page'].'</td>';
				$query_users = "SELECT * from users WHERE user_type='".$row['level_name']."'";
				$result_users = mysqli_query($dBlink ,$query_users) or die(mysql_error());
				$num_rows = mysql_num_rows($result_users);
				$table .= '<td>'.$num_rows.'</td>';
				$table .= '</tr>';
			}
			echo $table;
		} else { 
			echo 'You cannot view this list.';
		}
	}//get user level info ends here.
}//class ends here.