<?php
//Company Class

class Company {
	public $company_manual_id;
	public $company_name;
	public $business_type;
	public $address1;
	public $address2;
	public $city;
	public $state;
	public $country;
	public $zip_code;
	public $phone;
	public $email;
	public $company_logo;
	public $description;
	public $currency_symbol;
	
	function dashboard_companies($dBlink) { 
		if($_SESSION['user_type'] == 'admin') { 
			$query = "SELECT * from companies ORDER by company_name ASC";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$content = '';
			while($row = mysqli_fetch_array($result)) {
				extract($row);
				$content .= '<div class="info_box userinfo alignleft">';
                $content .= '<h3>'.$company_name.'</h3>';
                $content .= '<hr />';
                $content .= '<table width="100%" cellpadding="2" cellspacing="0" border="0">';
                $content .= '<tr>';
					$content .= '<th>Account Name</th>';
					$content .= '<th>Type</th>';
					$content .= '<th>Balance</th>';
					$content .= '</tr>';
				//accounts query ends here.
				$accounts_query = 'SELECT * from accounts WHERE company_id="'.$company_id.'"';
				$accounts_result = mysqli_query($dBlink ,$accounts_query) or die(mysql_error());
				while($account_row = mysqli_fetch_array($accounts_result)) {
					extract($account_row);
					$content .= '<tr>';
					$content .= '<td>'.$account_title.'</td>';
					$content .= '<td>'.$account_type.'</td>';
					//Balance Query
					$balance_query = 'SELECT SUM(debit), SUM(credit) from transactions WHERE account_id="'.$account_id.'"';
					$balance_result = mysqli_query($dBlink ,$balance_query) or die(mysql_error());
					$balance_row = mysqli_fetch_array($balance_result);
					$balance = $balance_row['SUM(debit)']+$balance_row['SUM(credit)'];
					//balance Query ends here.
					$content .= '<td>'.number_format($balance).'</td>';
					$content .= '</tr>';
				}
				$content .= '</table>';
                $content .= '</div><!--users info ends here.-->';
			}
		} else { 
			$query = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."'";
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$content = '';
			while($row = mysql_fetch_array($result)) {
			$query_company = "SELECT * from companies WHERE company_id='".$row['company_id']."' ORDER BY company_name ASC";
			$result_company = mysqli_query($dBlink ,$query_company) or die(mysql_error());
			while($row_company = mysql_fetch_array($result_company)) { 
				extract($row_company);
				$content .= '<div class="info_box userinfo alignleft">';
                $content .= '<h3>'.$company_name.'</h3>';
                $content .= '<hr />';
                $content .= '<table width="100%" cellpadding="2" cellspacing="0" border="0">';
                $content .= '<tr>';
				$content .= '<th>Account Name</th>';
				$content .= '<th>Type</th>';
				$content .= '<th>Balance</th>';
				$content .= '</tr>';
				//accounts query ends here.
				$accounts_query = 'SELECT * from accounts WHERE company_id="'.$company_id.'"';
				$accounts_result = mysqli_query($dBlink ,$accounts_query) or die(mysql_error());
				while($account_row = mysql_fetch_array($accounts_result)) { 
					extract($account_row);
					$content .= '<tr>';
					$content .= '<td>'.$account_title.'</td>';
					$content .= '<td>'.$account_type.'</td>';
					//Balance Query
					$balance_query = 'SELECT SUM(debit), SUM(credit) from transactions WHERE account_id="'.$account_id.'"';
					$balance_result = mysqli_query($dBlink ,$balance_query) or die(mysql_error());
					$balance_row = mysql_fetch_array($balance_result);
					$balance = $balance_row['SUM(debit)']+$balance_row['SUM(credit)'];
					//balance Query ends here.
					$content .= '<td>'.number_format($balance).'</td>';
					$content .= '</tr>';
				}
				$content .= '</table>';
                $content .= '</div><!--users info ends here.-->';	
			}
			}//loop company access ends here.
		}//query ends here.
		echo $content;
	}//companies info ends here.
	
	function delete_company($dBlink ,$company_id) { 
		if($_SESSION['user_type'] == 'admin') { 
			$query_account = "SELECT * from accounts WHERE company_id='".$company_id."'";
			$result_account = mysqli_query($dBlink ,$query_account) or die(mysql_error());
			$num_rows = mysql_num_rows($result_account);
			if($num_rows > 0) { 
				return "This company have accounts related to it. Please delete related accounts first to delete this company.";
			} else { 
				$del_query = mysqli_query($dBlink ,'DELETE from companies WHERE company_id="'.$company_id.'"') or die(mysql_error());
				return 'Company was deleted successfuly.';
			}
		} else { 
			return 'You have no permission to delete company.';
		}
	}//delete Account function ends here.
	
	function company_name($dBlink ,$company_id) { 
		$query = 'SELECT * from companies WHERE company_id="'.$company_id.'"';
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysqli_fetch_array($result);
		return $row['company_name'];
	}//company_info ends here.
		
	function set_company($dBlink ,$company_id) { 
		if($_SESSION['user_type'] != 'admin') {
			$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."' AND company_id='".$company_id."'";
			$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
			$row_num = mysql_num_rows($result_access);
			if($row_num < 0) { 
			echo 'You have no access to this company.';
			exit();
			}
		}
		$query = "SELECT * from companies WHERE company_id='".$company_id."'"; 
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		if(mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
			extract($row);	
			$this->company_manual_id = $company_manual_id;
			$this->company_name = $company_name;
			$this->business_type = $business_type;
			$this->address1 = $address1;
			$this->address2 = $address2;
			$this->city = $city;
			$this->state = $state;
			$this->country = $country;
			$this->zip_code = $zip_code;
			$this->phone = $phone;
			$this->email = $email;
			$this->company_logo = $company_logo;
			$this->description = $description;
			$this->currency_symbol = $currency_symbol;
		} else { 
			echo 'This company does not exist or You cant access this company.';
		}
		
	}//level set ends here.
	
	function update_company($dBlink ,$company_id, $company_manual_id, $company_name, $business_type, $address1, $address2, $city, $state, $country, $zip_code, $phone, $email, $company_logo, $description, $currency_symbol) {
		if($_SESSION['user_type'] != 'admin') {
			exit();
			}//checks admin user.
		$query = 'UPDATE companies SET
			company_manual_id="'.$company_manual_id.'",
			company_name="'.$company_name.'",
			business_type="'.$business_type.'",
			address1="'.$address1.'",
			address2="'.$address2.'",
			city="'.$city.'",
			state="'.$state.'",
			country="'.$country.'",
			zip_code="'.$zip_code.'",
			phone="'.$phone.'",
			email="'.$email.'",
			company_logo="'.$company_logo.'",
			description="'.$description.'",
			currency_symbol="'.$currency_symbol.'"
			WHERE company_id='.$company_id.'
			';	
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		return 'Company was updated successfuly!';
		}//update_company function ends here.
	
	function add_company($dBlink ,$company_manual_id, $company_name, $business_type, $address1, $address2, $city, $state, $country, $zip_code, $phone, $email, $company_logo, $description, $currency_symbol) { 
		//check manual id if already exist.
//		$query = "SELECT * from companies WHERE company_manual_id='".$company_manual_id."'";
//		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
//		$num_rows = mysqli_num_rows($result);
//
//		echo $company_logo;
//		exit;

//		if($num_rows > 0) {
//			return 'Please chose different manual unique id. The id '.$company_manual_id.' already exists.';
//			exit();
//		} else {
			$query = 'INSERT into companies
			(company_id, company_manual_id, company_name, business_type, address1, address2, city, state, country, zip_code, phone, email, company_logo, description, user_id, currency_symbol)
			VALUES(NULL, "'.$company_manual_id.'", "'.$company_name.'", "'.$business_type.'", "'.$address1.'", "'.$address2.'", "'.$city.'", "'.$state.'", "'.$country.'", "'.$zip_code.'", "'.$phone.'", "'.$email.'", "'.$company_logo.'", "'.$description.'", "'.$_SESSION['user_id'].'", "'.$currency_symbol.'")';
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			return 'Company added successfuly.';
//		}
	}//add_company ends here.
	
	function list_companies($dBlink ) {
        $company_logo = '';
			if($_SESSION['user_type'] == 'admin') { 
				$query = 'SELECT * from companies ORDER by company_name ASC';
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$content = '';
			$count = 0;
			while($row = mysqli_fetch_array($result)) {
				extract($row);
				$count++;
				if($count%2 == 0) { 
					$class = 'even';
				} else { 
					$class = 'odd';
				}
				if($company_logo != '') { 
					$company_logo = '<img src="'.$company_logo.'" height="40" width="40" />';
				}
				$content .= '<tr class="'.$class.'">';
				$content .= '<td>';
				$content .= $company_manual_id;
				$content .= '</td><td>';
				$content .= $company_name;
				$content .= '</td><td>';
				$content .= $business_type;
				$content .= '</td><td>';
				$content .= $city;
				$content .= '</td><td>';
				$content .= $phone;
				$content .= '</td><td>';
				$content .= $email;
				$content .= '</td><td>';
				$content .= $company_logo;
				$content .= '</td><td>';
				$content .= $currency_symbol;
				$content .= '</td><td>';
				$content .= '<form method="post" name="view_accounts" action="accounts.php">';
				$content .= '<input type="hidden" name="company_id" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Accounts">';
				$content .= '</form>';
				$content .= '</td>';
				if(partial_access($dBlink,'admin')) { $content .= '<td><form method="post" name="edit" action="manage_company.php">';
				$content .= '<input type="hidden" name="edit_company" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Edit">';
				$content .= '</form>';
				$content .= '</td><td>';
				$content .= '<form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$content .= '<input type="hidden" name="delete_company" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Delete">';
				$content .= '</form>';
				$content .= '</td>'; }
				$content .= '</tr>'; 
				unset($class);
			}//loop ends here.
			} else { 
				$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."'";
				$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
				$content = '';
				while($row_access = mysqli_fetch_array($result_access)) {
				$query = 'SELECT * from companies WHERE company_id="'.$row_access['company_id'].'"';
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			$count = 0;
			while($row = mysqli_fetch_array($result)) {
				extract($row);
				$count++;
				if($count%2 == 0) { 
					$class = 'even';
				} else { 
					$class = 'odd';
				}
				if($company_logo != '') { 
					$company_logo = '<img src="'.$company_logo.'" height="40" width="40" />';
				}
				$content .= '<tr class="'.$class.'">';
				$content .= '<td>';
				$content .= $company_manual_id;
				$content .= '</td><td>';
				$content .= $company_name;
				$content .= '</td><td>';
				$content .= $business_type;
				$content .= '</td><td>';
				$content .= $city;
				$content .= '</td><td>';
				$content .= $phone;
				$content .= '</td><td>';
				$content .= $email;
				$content .= '</td><td>';
				$content .= $company_logo;
				$content .= '</td><td>';
				$content .= $currency_symbol;
				$content .= '</td><td>';
				$content .= '<form method="post" name="view_accounts" action="accounts.php">';
				$content .= '<input type="hidden" name="company_id" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Accounts">';
				$content .= '</form>';
				$content .= '</td>';
				if(partial_access('admin')) { $content .= '<td><form method="post" name="edit" action="manage_company.php">';
				$content .= '<input type="hidden" name="edit_company" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Edit">';
				$content .= '</form>';
				$content .= '</td><td>';
				$content .= '<form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$content .= '<input type="hidden" name="delete_company" value="'.$company_id.'">';
				$content .= '<input type="submit" value="Delete">';
				$content .= '</form>';
				$content .= '</td>'; }
				$content .= '</tr>'; 
				unset($class);
			}//loop ends here.
				}
				
			} //if else ends here.
			
		echo $content;
	}//list_levels ends here.
	
	function company_options($dBlink) {
        $options = '';
		$query = 'SELECT * from companies ORDER by company_name ASC';
		$result = mysqli_query($dBlink ,$query) or die(mysqli_error());
		
			while($row = mysqli_fetch_array($result)) {
				$options .= '<option value="'.$row['company_id'].'">'.$row['company_manual_id'].' | '.ucfirst($row['company_name']).'</option>';
			}
		echo $options;
	}//company options
}//company class ends here.