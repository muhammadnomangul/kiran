<?php
//Account Class

class Account {
	public $account_id;
	public $account_number;
	public $account_title;
	public $account_type;
	public $memo;
	public $company_id;
	public $cnic;
	public $contact;
	public $address;

	function delete_account($dBlink ,$account_id) { 
		if($_SESSION['user_type'] == 'admin') { 
			$query_jv = "SELECT * from transactions WHERE account_id='".$account_id."'";
			$retult_jv = mysqli_query($dBlink ,$query_jv) or die(mysql_error());
			$num_rows = mysqli_num_rows($retult_jv);
			if($num_rows > 0) { 
				return "This account have transactions. Please delete related JV's first by checking ledger then you can delete this account.";
			} else { 
				$del_query = mysqli_query($dBlink ,'DELETE from accounts WHERE account_id="'.$account_id.'"') or die(mysql_error());
				return 'Account was deleted successfuly.';
			}
		} else { 
			return 'You have no permission to delete account.';
		}
	}//delete Account function ends here.
	
	function update_account($dBlink ,$account_id, $account_number, $account_title, $account_type, $memo,$cnic,$contact,$address) {
		$account_check = "SELECT * from accounts where account_id='".$account_id."' AND company_id='".$_SESSION['company_id']."'";
		$account_check_result = mysqli_query($dBlink ,$account_check) or die(mysql_error());
		$account_check_rows = mysqli_num_rows($account_check_result);
		
		if($account_check_rows > 0) {
			$query = "UPDATE accounts SET
				account_number='".$account_number."',
				account_title='".$account_title."',
				account_type='".$account_type."',
				cnic='".$cnic."',
				contact='".$contact."',
				address='".$address."',
				memo='".$memo."'
				WHERE account_id='".$account_id."'
			";
//            echo $query;
//            exit;
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			return 'Account updated successfuly.';
		}//if account relates same company user is signed
	}//update account function ends here.
	
	function set_account($dBlink ,$account_id) { 
		$query = "SELECT * from accounts WHERE account_id='".$account_id."' AND company_id='".$_SESSION['company_id']."'";
		$result = mysqli_query($dBlink ,$query) or die(mysql_error());
		$row = mysqli_fetch_array($result);
		extract($row);
		
		$this->account_id = $account_id;
		$this->account_number = $account_number;
		$this->account_title = $account_title;
		$this->account_type = $account_type;
		$this->memo = $memo;
		$this->company_id = $company_id;		
		$this->cnic = $cnic;
		$this->contact = $contact;
		$this->address = $address;
	}//set account ends here.
	
	function list_accounts($dBlink) { 
		if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
		if($_SESSION['user_type'] == 'admin') { 
			$query = "SELECT * from accounts WHERE company_id='".$_SESSION['company_id']."' ORDER by account_title ASC";
		}else { 
				$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."' AND company_id='".$_SESSION['company_id']."'";
				$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
				$access_num = mysql_num_rows($result_access);
				if($access_num > 0) { 
					$query = "SELECT * from accounts WHERE company_id='".$_SESSION['company_id']."' ORDER by account_title ASC";
				} else { 
					echo 'You cannot access this company.';
					exit();
				}
			}
	} else { 
		echo 'Please select company to list accounts.';
		exit();
	}	
	$options = '';
	$result = mysqli_query($dBlink ,$query) or die(mysql_error());
			while($row = mysqli_fetch_array($result)) {
				extract($row);
				$balance_query = "SELECT * from transactions WHERE account_id='".$account_id."'";
				$balance_result = mysqli_query($dBlink ,$balance_query) or die(mysql_error());
				$balance = 0;
				while($row_balance = mysqli_fetch_array($balance_result)) {
					$balance = $balance+$row_balance['debit']+$row_balance['credit'];
				}
				$options .= '<tr>';
				$options .= '<td>'.$account_id.'</td>';
				$options .= '<td>'.$account_number.'</td>';
				$options .= '<td>'.$account_title.'</td>';
				$options .= '<td>'.$account_type.'</td>';
				if($balance < 0) {
				$options .= '<td style="color:red;">'.number_format($balance).' '.$_SESSION['currency'].'</td>';
				} else { 
				$options .= '<td>'.number_format($balance).' '.$_SESSION['currency'].'</td>';
				}
				$options .= '<td><form method="post" target="_blank" name="delete" action="reports/account_ledger.php">';
				$options .= '<input type="hidden" name="ledger_account" value="'.$account_id.'">';
				$options .= '<input type="submit" value="Ledger">';
				$options .= '</form></td>';
				if(partial_access($dBlink,'admin')) {
				$options .= '<td><form method="post" name="delete" action="manage_accounts.php">';
				$options .= '<input type="hidden" name="edit_account" value="'.$account_id.'">';
				$options .= '<input type="submit" value="Edit">';
				$options .= '</form></td>';
				$options .= '<td><form method="post" name="delete" onsubmit="return confirm_delete();" action="">';
				$options .= '<input type="hidden" name="delete_account" value="'.$account_id.'">';
				$options .= '<input type="submit" value="Delete">';
				$options .= '</form></td>';
				}
				$options .= '</tr>';
			} 
			echo $options;
	}//list_Accounts function ends here.
	
	function add_account($dBlink ,$account_number, $account_title, $account_type, $memo,$cnic,$contact,$address) {
		if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
			if($_SESSION['user_type'] != 'admin') {
				$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."' AND company_id='".$_SESSION['company_id']."'";
				$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
				$num_rows = mysql_num_rows($result_access);
				if($num_rows > 0) { 
					
				} else { 
					return 'You dont have access to this company!';
					exit();
				}
			}
			$query = "INSERT into accounts(account_number, account_title, account_type, memo, company_id,cnic,contact,address)
				VALUES('".$account_number."','".$account_title."','".$account_type."','".$memo."','".$_SESSION['company_id']."','".$cnic."','".$contact."','".$address."')";
//			echo $query;
//			exit;
			$result = mysqli_query($dBlink ,$query) or die(mysql_error());
				return 'Account was added successfuly!';
		} else { 
			return 'Please select a company first.';
		}
	}//add account ends here.
	
	function account_options($dBlink ) { 
		if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
			if($_SESSION['user_type'] != 'admin') {
				$query_access = "SELECT * from company_access WHERE user_id='".$_SESSION['user_id']."' AND company_id='".$_SESSION['company_id']."'";
				$result_access = mysqli_query($dBlink ,$query_access) or die(mysql_error());
				$num_rows = mysql_num_rows($result_access);
				if($num_rows > 0) { 
					
				} else { 
					return 'You dont have access to this company!';
					exit();
				}
			}
			$query = "SELECT * from accounts WHERE company_id='".$_SESSION['company_id']."' ORDER by account_title ASC";
				$result = mysqli_query($dBlink ,$query) or die(mysql_error());
				$options = '';
				while($row = mysqli_fetch_array($result)) {
					extract($row);
//					$options .= '<option value="'.$account_id.'">'.$account_number.' | '.$account_title.'</option>';
					$options .= '<option value="'.$account_id.'">'.$account_title.'</option>';
				}//loop ends here.
				echo $options;
		} else { 
			return 'Please select a company first.';
		}
	}
}//Accounts class ends here.