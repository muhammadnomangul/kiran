<?php
include('../system_load.php');
//user Authentication.
authenticate_user($dBlink,'subscriber');

$new_company = new Company;
//new account object/
$new_account = new Account;
//new journal voucher object
$new_jvs = new Jvs;

if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
    $_SESSION['company_name'] = $new_company->company_name($dBlink,$_SESSION['company_id']);
    $new_company->set_company($dBlink,$_SESSION['company_id']);
} else {
    HEADER('LOCATION: company.php?message=Please select a company.');
}//check if company is selected.
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>General Ledger</title>

    <link href="style.css" media="all" rel="stylesheet" type="text/css" />

    <link href="../css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

    <link href="../css/style.css" media="all" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8">
        $(function() {
            $(".datepick").datepicker({
                inline: true,
                dateFormat: 'yy-mm-dd',
            });
        });
    </script>
</head>

<body>
<?php if(!isset($_POST['tr_from']) && !isset($_POST['tr_to'])) { ?>
    <div class="transaction_range">
        <h2>Select statement period</h2>
        <p>Please select date range to check accounts transaction summary.</p>
        <form name="date" action="jv_report.php"     method="post">
            <table cellpadding="4" border="0">
                <tr>
                    <th>From:</th>
                    <th>To:</th>
                </tr>
                <tr>
                    <td><input type="text" name="tr_from" class="datepick" readonly="readonly" value="<?php echo date('Y-m-d',strtotime("-3 Months")); ?>" /></td>
                    <td><input type="text" name="tr_to" class="datepick" readonly="readonly" value="<?php echo date('Y-m-d'); ?>" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Go" /></td>
                </tr>
            </table>
        </form>
    </div>
    <?php exit(); } ?>
</body>
</html>
