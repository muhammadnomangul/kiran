<?php
	include('../system_load.php');

	//user Authentication.
	authenticate_user($dBlink,'subscriber');
	
	$new_company = new Company;
	//new account object/
	$new_account = new Account;
	//new journal voucher object
	$new_jvs = new Jvs;
	
	if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') { 
		$_SESSION['company_name'] = $new_company->company_name($dBlink,$_SESSION['company_id']);
	} else { 
		HEADER('LOCATION: company.php?message=Please select a company.');
	}//check if company is selected.
	
	//if user not coming directly.
	if(isset($_POST['ledger_account']) && $_POST['ledger_account'] != '') { 
		//you can access this form.
		$new_company->set_company($dBlink,$_SESSION['company_id']);
		//set_account
		$new_account->set_account($dBlink,$_POST['ledger_account']);
	} else { 
		HEADER('LOCATION: ../accounts.php?message=Please select account Ledger');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>General Ledger</title>

<link href="style.css" media="all" rel="stylesheet" type="text/css" />

<link href="../css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
<script src="../js/jquery-1.10.2.min.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

<link href="../css/style.css" media="all" rel="stylesheet" type="text/css" /> 


<script type="text/javascript" charset="utf-8">
	$(function() {
		$(".datepick").datepicker({
			inline: true,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
</head>

<body>
<?php if(!isset($_POST['tr_from']) && !isset($_POST['tr_to'])) { ?>
<div class="transaction_range">
	<h2>Select statement period</h2>
    <p>Please select date range to check accounts transaction summary.</p>
    <form name="date" action="" method="post">
    <table cellpadding="4" border="0">
    	<tr>
        	<th>From:</th>
            <th>To:</th>
        </tr>
        <tr>
        	<td><input type="text" name="tr_from" class="datepick" readonly="readonly" value="<?php echo date('Y-m-d',strtotime("-3 Months")); ?>" /></td>
            <td><input type="text" name="tr_to" class="datepick" readonly="readonly" value="<?php echo date('Y-m-d'); ?>" /></td>
        </tr>
        <input type="hidden" name="ledger_account" value="<?php echo $_POST['ledger_account']; ?>" />
       	<tr>
        	<td colspan="2"><input type="submit" value="Go" /></td>
        </tr>
    </table>
    </form>
</div>
<?php exit(); } ?>
<div align="center" class="company_info">
	<?php if($new_company->company_logo != '') { 
        echo "<img src='../".$new_company->company_logo."' height='50px' class='company_logo' align='left'>";
    } ?>
    <h2><?php echo $new_company->company_name; ?></h2>
<!--    <p>--><?php //echo $new_company->address1; ?><!-- --><?php //echo $new_company->address2; ?><!-- --><?php //echo $new_company->city; ?><!-- --><?php //echo $new_company->state; ?><!-- --><?php //echo $new_company->country; ?><!-- --><?php //echo $new_company->zip_code; ?><!--<br />Email: --><?php //echo $new_company->email; ?><!-- Phone: --><?php //echo $new_company->phone; ?><!--</p>	-->
    <div style="clear:both;"></div>
    </div><!--company_info ends here.-->
    
<div id="table_div" align="center">
<div style="margin:auto; width:800px; margin-top:15px; padding-bottom:15px;">
<h1>Transactions Summary</h1>
</div>

<div class="account_info" style="clear:both; text-align:left; width:800px; margin:auto; margin-top:10px;">
    <strong>Account Number:</strong>
    <?php echo $new_account->account_number; ?> &nbsp;
    &nbsp;
    <strong>Account Title:</strong>
    <?php echo $new_account->account_title; ?>
    <br />

    <strong>Account Type:</strong>
    <?php echo $new_account->account_type; ?>

    &nbsp;&nbsp
    <strong>Memo:</strong>
    <?php echo $new_account->memo; ?><br />


    <strong>CNIC :</strong>
    <?php echo $new_account->cnic; ?>
    <br />

    <strong>Contact #:</strong>
    <?php echo $new_account->contact; ?>

    &nbsp;&nbsp
    <strong>Address:</strong>
    <?php echo $new_account->address; ?><br />

<strong></strong>
    <?php// echo date('Y-m-d'); ?>
    <strong>Statement Period:</strong>

    <?php echo $_POST['tr_from'].' - '.$_POST['tr_to']; ?>
<!--<strong> Current Balance :: </strong>--><?php //echo $new_account->account_type; ?>

</div><!--account_info -->
<div class="clearIt"></div>
<table width="800" align="center" border="0" cellspacing="0" cellpadding="5px">
  <tr>

    <th scope="col" class="align_left">V ID</th>
    <th scope="col" class="align_left">Date</th>
    <th scope="col" width="400px" class="align_left">Narration</th>
    <th scope="col" class="align_right">Debit</th>
    <th scope="col" class="align_right">Credit</th>
    <th scope="col" class="align_right">Balance</th>
  </tr>
<?php $new_jvs->transaction_summary($dBlink,$new_account->account_id, $_POST['tr_from'], $_POST['tr_to']); ?>


</table>
<br /><br />
<!--<p align="center">This is computer generated statement does not need signature.</p>-->
</div><!--table_div-->
</body>
</html>
