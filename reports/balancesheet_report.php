<?php

include('../system_load.php');
//user Authentication.
authenticate_user($dBlink,'subscriber');

$new_company = new Company;
//new account object/
$new_account = new Account;
//new journal voucher object
$new_jvs = new Jvs;

if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
    $_SESSION['company_name'] = $new_company->company_name($dBlink,$_SESSION['company_id']);
    $new_company->set_company($dBlink,$_SESSION['company_id']);
} else {
    HEADER('LOCATION: company.php?message=Please select a company.');
}//check if company is selected.
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>General Ledger</title>

    <link href="style.css" media="all" rel="stylesheet" type="text/css" />

    <link href="../css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

    <link href="../css/style.css" media="all" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8">
        $(function() {
            $(".datepick").datepicker({
                inline: true,
                dateFormat: 'yy-mm-dd',
            });
        });
    </script>
</head>

<body>

<div align="center" class="company_info">
    <?php if($new_company->company_logo != '') {
        echo "<img src='../".$new_company->company_logo."' height='50px' class='company_logo' align='left'>";
    } ?>
    <h2><?php echo $new_company->company_name; ?></h2>
    <p><?php echo $new_company->address1; ?> <?php echo $new_company->address2; ?> <?php echo $new_company->city; ?> <?php echo $new_company->state; ?> <?php echo $new_company->country; ?> <?php echo $new_company->zip_code; ?><br />Email: <?php echo $new_company->email; ?> Phone: <?php echo $new_company->phone; ?></p>
    <div style="clear:both;"></div>
</div><!--company_info ends here.-->

<div id="table_div" align="center">
    <div style="margin:auto; width:800px; margin-top:15px; padding-bottom:15px;">
        <h1>All  Vouchers</h1>
    </div>
    <?php

    ?>
    <div class="account_info" style="clear:both; text-align:left; width:800px; margin:auto; margin-top:10px;">
        <strong>Today:</strong> <?php echo date('Y-m-d'); ?>
        <br>
        <strong>Statement Period:</strong> <?php echo $_POST['tr_from'].' - '.$_POST['tr_to']; ?>
        <br>
    </div><!--account_info -->
    <div class="clearIt"></div>
    <table width="800" align="center" border="0" cellspacing="0" cellpadding="5px">
        <!--        <tr>-->
        <!--            <th scope="col" class="align_left">JV ID</th>-->
        <!--            <th scope="col" class="align_left">Date</th>-->
        <!--            <th scope="col" class="align_left">Manual ID</th>-->
        <!--            <th scope="col" class="align_left">Title</th>-->
        <!--            <th scope="col" width="150px" class="align_left">Description</th>-->
        <!--            <th scope="col"  class="align_left">Type</th>-->
        <!--            <th scope="col" class="align_left">Posted By</th>-->
        <!--            <th scope="col" class="align_right">Amount</th>-->
        <!--        </tr>-->
        <tr>
            <th scope="col" class="align_left">Date</th>
            <th scope="col" class="align_left">Account</th>
            <th scope="col" class="align_left">Account Title</th>
            <th scope="col" class="align_left">Description</th>
            <th scope="col" class="align_center">type</th>

            <th scope="col" class="align_right">Debit</th>
            <th scope="col" class="align_right">Credit</th>
        </tr>
        <?php $new_jvs->balancesheet_summary($dBlink,$_POST['tr_from'], $_POST['tr_to']); ?>
        <tr>
            <th scope="col" class="align_left"></th>
            <th scope="col" class="align_left"></th>
            <th scope="col" class="align_left"></th>
            <th scope="col" class="align_left"></th>
            <th scope="col" class="align_right">Debit=<?php  $new_jvs->cpv_debit($dBlink); ?></th>
            <th scope="col" class="align_right">Credit=<?php  $new_jvs->cpv_credit($dBlink); ?></th>
        </tr>
    </table>
    <br /><br />
    <p align="center">This is computer generated statement does not need signature.</p>
</div><!--table_div-->
</body>
</html>
