<?php

include('system_load.php');
//This loads system.
//user Authentication.
authenticate_user($dBlink,'subscriber');
$jv_id =  get_id($dBlink);
//    echo $jv_id;
//    exit;
$new_company = new Company;
//new account object/
$new_account = new Account;
//new journal voucher object
$new_jvs = new Jvs;

if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
    $_SESSION['company_name'] = $new_company->company_name($dBlink ,$_SESSION['company_id']);
} else {
    HEADER('LOCATION: company.php?message=Please select a company.');
}//check if company is selected.

//add Journal Voucher PRocessing here.
if(isset($_POST['add_jv']) && $_POST['add_jv'] == '1') {
    extract($_POST);

    if($date == '') {
        $message = 'Date is required.';
    } else if($jv_title == '') {
        $message = 'Journal Voucher title is required.';
    }
    $balance = 0;
    $debit_balance = 0;
    $credit_balance = 0;
    foreach($debit_amount as $debit_amnt) {
        $debit_balance = $debit_balance+$debit_amnt;
    }
    foreach($credit_amount as $credit_amnt) {
        $credit_balance = $credit_balance+$credit_amnt;
    }
    if($debit_balance-$credit_balance != 0) {
        $message = "Balance is not equal or zero. Please re enter.";
    } else if(sizeof($credit_amount) || sizeof($debit_amount) != sizeof($account_id)) {
        $message = "Accounts or Amounts does not match. Re Enter please.";
    }

    //jv processing here with saving jv id.
    $jv_id = $new_jvs->add_journal_voucher($dBlink ,$date, $jv_manual_id, $jv_title, $jv_description,$jv_type);

    if($jv_id == '' || $jv_id == 0){} else {
        $debit_counter = 0;
        $credit_counter = 0;

        while($debit_counter < sizeof($debit_amount)) {

            $new_jvs->add_transaction($dBlink ,$jv_id, $date, $debit_account_id[$debit_counter], $debit_memo[$debit_counter], $debit_amount[$debit_counter],$debit_amount[$debit_counter],0);
            $debit_counter++;
        }
        while($credit_counter < sizeof($credit_amount)) {

            $new_jvs->add_transaction($dBlink ,$jv_id, $date, $credit_account_id[$credit_counter], $credit_memo[$credit_counter], $credit_amount[$credit_counter],0,$credit_amount[$credit_counter]);
            $credit_counter++;
        }
        HEADER('LOCATION: manage_jvs.php?message=Your entry was succesfuly entered.');
    }
}//end of JV Processing.



$page_title = $_SESSION['company_name']; //You can edit this to change your page title.
require_once("includes/header.php"); //including header file.
?>
    <style type="text/css">
        .transactions table { border-collapse: collapse; }
        .transactions table td, .transactions table th { border: 1px solid #CCCCCC; padding: 5px; }

        .transactions textarea, .transactions input[type=text] {height:28px;}

        .transactions {margin-top:30px;}
        .transactions table {
            margin-top: 8px;
            display: inline;
            margin-left: 20px;}
        .transactions strong small {margin-left:345px; color:green;}
        #items th { background: #eee; }

        #debit_amnt, #credit_amnt, #balance_amnt {font-weight:bold;}
        textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delme:hover { background-color:#EEFF88; }

        .delete-wpr { position: relative; }
        .delme { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }
    </style>

    <div class="admin_wrap">
        <?php require_once('includes/sidebar.php'); ?>
        <div class="alignleft rightcontent">
            <?php
            //display message if exist.
            if(isset($message) && $message != '') {
                echo '<div class="alert-box">';
                echo $message;
                echo '</div>';
            } else if(isset($_GET['message']) && $_GET['message'] != '') {
                echo '<div class="alert-box">';
                echo $_GET['message'];
                echo '</div>';
            }
            ?>
            <h2 class="alignleft"><?php if(isset($_POST['edit_jv'])){ echo 'Edit Journal Voucher'; } else { echo 'Adjustment Vouchers';} ?></h2>
            <div class="clear"></div><!--clear float-->
            <form name="add_jvs" id="add_jv" action="" method="post">
                <div class="journal">
                    <table width="680px" border="0" cellpadding="5">
                        <tr>
                            <th width="95px">Date*</th>
                            <td><input type="text" name="date" id="datepicker" required="required" readonly="readonly" value="<?php echo date("Y-m-d"); ?>" /></td>
                            <th width="95px">JV Manual ID</th>
                            <td><input type="text" name="jv_manual_id" value="<?php echo $jv_id; ?>" placeholder="Journal Voucher Manual Id" /></td>
                        </tr>
                        <input type="hidden" name="jv_type" value="avs" >
<!--                        <tr>-->
<!--                            <th>JV Title*</th>-->
<!--                            <td><input type="text" name="jv_title" placeholder="Voucher Title" required="required"  /></td>-->
<!--                            <th>JV Description</th>-->
<!--                            <td><textarea name="jv_description" placeholder="Voucher Description"></textarea></td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <th>JV Type</th>-->
<!--                            <td>-->
<!--                                <!--                                 <input type="text" name="jv_type" placeholder="Voucher Type" required="required"  />-->
<!--                                <select class="jv_type" name="jv_type" required="required">-->
<!--                                    <option value="">Select jv type</option>-->
<!--                                    <option value="cpv">Cash Payment Voucher </option>-->
<!--                                    <option value="crv">Cash Received Voucher </option>-->
<!--                                    <option value="bpv">Bank Payment Voucher</option>-->
<!--                                    <option value="vpv">V payment Voucher</option>-->
<!--                                    <!--                                    --><?php ////$new_account->account_options($dBlink ); ?>
<!--                                </select>-->
<!--                            </td>-->
<!--                        </tr>-->

                    </table>
                </div><!--journal Ends here.-->
                <div class="transactions">
                    <strong>Transactions <small></small></strong>
                    <table id="items1" style="float: left;">
                        <tr>
                            <th width="">Debit Transactions *</th>

                        </tr>
                        <tr>
                            <th width="">Account*</th>
                            <th width="">Memo</th>
                            <th>Amount*</th>
                        </tr>

                        <tr class="item-row1">
                            <td class="item-name"><div class="delete-wpr">
                                    <select class="account_id" name="debit_account_id[]" required="required">
                                        <option value="">Select Account</option>
                                        <?php $new_account->account_options($dBlink ); ?>
                                    </select>
                                </div></td>
                            <td class="description"><textarea id="memo1" name="debit_memo[]" placeholder="Transaction Description"></textarea></td>
                            <td>
                                <input type="text" id="debit_amount" name="debit_amount[]" class="debit_amount" placeholder="Amount" required="required" />
                                <input type="hidden" name="debit[]" id="debit_ttl" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <a id="addrow1" href="javascript:void(0);" title="Add a row">
                                    Add a row
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right">
                                Debit: <span id="debit_amnt">0.00</span><br />

                            </td>


                        </tr>

                    </table>
                    <table id="items2" width="">
                        <tr>
                            <th width="">Credit Transactions *</th>

                        </tr>
                        <tr>
                            <th width="">Account*</th>
                            <th width="">Memo</th>
                            <th>Amount*</th>

                        </tr>
                        <tr class="item-row2">
                            <td class="item-name"><div class="delete-wpr">
                                    <select class="account_id" name="credit_account_id[]" required="required">
                                        <option value="">Select Account</option>
                                        <?php $new_account->account_options($dBlink ); ?>
                                    </select>
                                </div></td>
                            <td class="description"><textarea id="memo2" name="credit_memo[]" placeholder="Transaction Description"></textarea></td>
                            <td>
                                <input type="text" id="credit_amount" name="credit_amount[]" class="credit_amount" placeholder="Amount" required="required" />
                                <input type="hidden" name="credit[]"  id="credit_ttl" value="" />
                            </td>

                        </tr>
                        <tr>
                            <td colspan="3">
                                <a id="addrow2" href="javascript:void(0);" title="Add a row">
                                    Add a row
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right">
                                Credit: <span id="credit_amnt">0.00</span> <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right">

                                Balance:  <span id="balance_amnt">0.00</span><br />
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="add_jv" value="1" />
                    <input type="submit" value="Add entry" />
            </form>
        </div><!--transactions div-->

    </div>
    <div class="clear"></div><!--clear Float-->
    </div><!--admin wrap ends here.-->


    <script type="text/javascript">
        $(document).ready(function() {
            //update total function.
            $('#memo1').change(function() {
                $('#memo2').val($(this).val());
            });
            $('#debit_amount').change(function() {
                $('#credit_amount').val($(this).val());
            });
            //calculations.
            $('#items1').on('change', '.debit_amount', function(){
                if(isNaN($(this).val())) {
                    $(this).val('');
                    alert('Please enter number.');
                }
                update_balance();
            });
            $('#items2').on('change', '.credit_amount', function(){
                if(isNaN($(this).val())) {
                    $(this).val('');
                    alert('Please enter number.');
                }
                update_balance();
            });
            //Remove row.
            $('#items1').on('click', '.delme', function() {
                $(this).parents('.item-row1').remove();
                update_balance();
            });
            $('#items2').on('click', '.delme', function() {
                $(this).parents('.item-row2').remove();
                update_balance();
            });
            //add row add here.
            $("#addrow1").click(function(e){
                e.preventDefault();
                console.log('here');
                $('<tr class="item-row1"><td class="item-name"><div class="delete-wpr"><select class="account_id" name="debit_account_id[]" required="required"><option value="">Select Account</option><?php $new_account->account_options($dBlink); ?></select><a class="delme" href="javascript:;" title="Remove row">X</a></div></td><td class="description"><textarea name="debit_memo[]" placeholder="Transaction Description"></textarea></td><td><textarea name="debit_amount[]" placeholder="Amount" id="debit_amount" class="debit_amount" required="required"></textarea></td></tr>').insertAfter($(".item-row1:last"));
            });
            $("#addrow2").click(function(e){
                e.preventDefault();
                console.log('here');
                $('<tr class="item-row2"><td class="item-name"><div class="delete-wpr"><select class="account_id" name="credit_account_id[]" required="required"><option value="">Select Account</option><?php $new_account->account_options($dBlink); ?></select><a class="delme" href="javascript:;" title="Remove row">X</a></div></td><td class="description"><textarea name="credit_memo[]" placeholder="Transaction Description"></textarea></td><td><textarea name="credit_amount[]" placeholder="Amount" id="credit_amount" class="credit_amount" required="required"></textarea></td></tr>').insertAfter($(".item-row2:last"));
            });
            $('#add_jv').submit(function(e) {
                $('.amount').each(function() {
                    if($(this).val().length == 0) {
                        error = 1;
                    } else {
                        error = 0;
                    }
                });
                $('.account_id').each(function() {
                    if($(this).val().length == 0) {
                        error1 = 1;
                    } else {
                        error1 = 0;
                    }
                });
                if($('#balance_amnt').html() != "0") {
                    alert("Error: Voucher Balance is not 0(zero)");
                    return false;
                } else if(error != 0) {
                    alert("All Amount fields are required.");
                    return false;
                } else if(error1 != 0) {
                    alert("You have to select all accounts.");
                    return false;
                }
            });
        });

        function update_balance() {
            debit_amnt = 0;
            credit_amnt = 0;
            balance_amnt = 0;
            i = 1;
            c = 1;

            $('.debit_amount').each(function(i){
                de_amount = parseInt($(this).val());
                if(!isNaN(de_amount)) {
                    debit_amnt = de_amount+debit_amnt;
                    console.log('debit amount = ', de_amount);
                    $('#debit_amnt').html(debit_amnt);
                    $('#credit_amnt').html(credit_amnt);
                    $('#balance_amnt').html(debit_amnt-credit_amnt);

                }
            });
            $('.credit_amount').each(function(c){
                cr_amount = parseInt($(this).val());
                if(!isNaN(cr_amount)) {
                    credit_amnt = cr_amount+credit_amnt;
                    console.log('credit amount = ', credit_amnt);
                    $('#debit_amnt').html(debit_amnt);
                    $('#credit_amnt').html(credit_amnt);
                    $('#balance_amnt').html(debit_amnt-credit_amnt);

                }
            });

            $('#credit_ttl').val(credit_amnt);
            $('#debit_ttl').val(debit_amnt);

        }//function update_balance Ends here.
    </script>
<?php require_once("includes/footer.php"); ?>