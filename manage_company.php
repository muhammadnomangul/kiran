<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user($dBlink,'admin');
	
	$new_company = new Company;
	
	//Profile Image Processing.
	if(isset($_FILES['company_logo']) && $_FILES['company_logo'] != '') { 
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["company_logo"]["name"]);
		$extension = end($temp);
		
		if ((($_FILES["company_logo"]["type"] == "image/gif")
		|| ($_FILES["company_logo"]["type"] == "image/jpeg")
		|| ($_FILES["company_logo"]["type"] == "image/jpg")
		|| ($_FILES["company_logo"]["type"] == "image/pjpeg")
		|| ($_FILES["company_logo"]["type"] == "image/x-png")
		|| ($_FILES["company_logo"]["type"] == "image/png"))
		&& ($_FILES["company_logo"]["size"] < 2048000)
		&& in_array($extension, $allowedExts)) {
 			 if ($_FILES["company_logo"]["error"] > 0) {
    			$message = "Return Code: " . $_FILES["company_logo"]["error"];
    	} else 	{
			$phrase = substr(md5(uniqid(rand(), true)), 16, 16);
	  if (file_exists("upload/" .$phrase.$_FILES["company_logo"]["name"])) {
	      $message = $_FILES["company_logo"]["name"] . " already exists. ";
      } else {
		  move_uploaded_file($_FILES["company_logo"]["tmp_name"],
		  "upload/".$phrase.str_replace(' ', '-',$_FILES["company_logo"]["name"]));
		  $company_logo = "upload/".$phrase.str_replace(' ', '-', $_FILES["company_logo"]["name"]);
//		  echo $company_logo;
//		  exit;
	  } //if file not exist already.
	  
    } //if file have no error
  }//if file type is alright.
} //if image was uploaded processing.
/*Image processing ends here.*/

//User update company but don't change company logo. setting old logo 
if(isset($_POST['edit_company']) && $_POST['edit_company'] != '' && isset($_POST['update_company'])) { 
	if(isset($company_logo)) { 
		$company_logo = $company_logo;
	} else { 
		if(isset($_POST['already_logo'])) { 
			$company_logo = $_POST['already_logo'];
		} else { 
			$company_logo = '';
		}
	}//logo setting on update ends here.
	extract($_POST);
		if($company_name == '') { 
			$message = 'Company name is required!';
		} else if($company_manual_id == '') { 
			$message = 'Company unique manual id is required!';
		} else if($currency_symbol == '') { 
			$message = 'Currency symbol is required.';
		}  else {
		$message = $new_company->update_company($dBlink ,$edit_company, $company_manual_id, $company_name, $business_type, $address1, $address2, $city, $state, $country, $zip_code, $phone, $email, $company_logo, $description, $currency_symbol);
		}
}//edit company query ends here.

if(isset($_POST['edit_company']) && $_POST['edit_company'] != '') { 
		$new_company->set_company($dBlink ,$_POST['edit_company']);
	}//setting company data if editing.

//add company processing.
	if(isset($_POST['add_company']) && $_POST['add_company'] == '1') { 
		extract($_POST);
		if($company_name == '') { 
			$message = 'Company name is required!';
		} else if($company_manual_id == '') { 
			$message = 'Company unique manual id is required!';
		} else if($currency_symbol == '') { 
			$message = 'Currency symbol is required.';
		}  else {
		        if(!isset($company_logo)){
                    $company_logo = '';
                }
		$message = $new_company->add_company($dBlink ,$company_manual_id, $company_name, $business_type, $address1, $address2, $city, $state, $country, $zip_code, $phone, $email, $company_logo, $description, $currency_symbol);
		}
	}//add Company processing ends here.
	
	$page_title = "Manage Company"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft"><?php if(isset($_POST['edit_company'])){ echo 'Edit Company'; } else { echo 'Add New Company';} ?></h2>
                	<div class="clear"></div><!--clear float-->
                    <form action="<?php $_SERVER['PHP_SELF']?>" id="add_company" name="user" method="post" enctype="multipart/form-data">
                    <table cellpadding="10" cellspacing="0" width="100%" border="0">
                    	<tr>
                        	<td width="150">Company Name*</td>
                            <td><input type="text" name="company_name" placeholder="Company Name" value="<?php echo $new_company->company_name; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Company Unique Manual Id*</td>
                            <td><input type="text" name="company_manual_id" placeholder="Company unique manual id" value="<?php echo $new_company->company_manual_id; ?>" required="required" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Business Type</td>
                            <td><input type="text" name="business_type" placeholder="Company business type" value="<?php echo $new_company->business_type; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Address 1</td>
                            <td><textarea name="address1" placeholder="Address line 1"><?php echo $new_company->address1; ?></textarea></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Address 2</td>
                            <td><textarea name="address2" placeholder="Address line 2"><?php echo $new_company->address2; ?></textarea></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">City</td>
                            <td><input type="text" name="city" placeholder="City" value="<?php echo $new_company->city; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">State/Province</td>
                            <td><input type="text" name="state" placeholder="State or Province" value="<?php echo $new_company->state; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Country</td>
                            <td><input type="text" name="country" placeholder="Your Country" value="<?php echo $new_company->country; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Zip code</td>
                            <td><input type="text" name="zip_code" placeholder="Your zip code" value="<?php echo $new_company->zip_code; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Phone</td>
                            <td><input type="text" name="phone" placeholder="Your phone number" value="<?php echo $new_company->phone; ?>" /></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Email</td>
                            <td><input type="text" name="email" placeholder="Your email address" value="<?php echo $new_company->email; ?>" /></td>
                        </tr>
             
                        <tr>
                        	<td width="150">Company Logo</td>
                            <td>
                            	<input type="file" name="company_logo" placeholder="Your profile image" />
                            	<?php
									if(isset($new_company->company_logo) && $new_company->company_logo != '') {
										echo '<a href="'.$new_company->company_logo.'" target="_blank"><img src="'.$new_company->company_logo.'" height="80" /></a><input type="hidden" name="already_logo" value="'.$new_company->company_logo.'">';	
									}
								?>
                            </td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Description</td>
                            <td><textarea name="description" placeholder="Company Description"><?php echo $new_company->description; ?></textarea></td>
                        </tr>
                        
                        <tr>
                        	<td width="150">Currency Symbol*</td>
                            <td><input type="text" name="currency_symbol" placeholder="Currency Symbol e.g $, SR" value="<?php echo $new_company->currency_symbol; ?>" required="required" /></td>
                        </tr>
                        
					  <?php 
						if(isset($_POST['edit_company'])){ 
							echo '<input type="hidden" name="edit_company" value="'.$_POST['edit_company'].'" />';
							echo '<input type="hidden" name="update_company" value="1" />'; 
						} else { 
							echo '<input type="hidden" name="add_company" value="1" />';
						} ?>
                        <tr>
                        	<td>&nbsp;</td>
                            <td><input type="submit" value="<?php if(isset($_POST['edit_company'])){ echo 'Update Company'; } else { echo 'Add Company';} ?>" /></td>
                        </tr>
                    </table>
                    </form>
                    <script type="text/javascript">
						$(document).ready(function() {
						// validate the register form
					$("#add_company").validate();
						});
                    </script>
                </div>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php require_once("includes/footer.php"); ?>