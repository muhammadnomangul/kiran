<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user($dBlink,'subscriber');
	//creating company object.
	$new_company = new Company;
	
	if(isset($_GET['message']) && $_GET['message'] != '') { 
		$message = 'Please select your company.';
	}//Message ends here select company
	
	//delete company if exist.
	if(isset($_POST['delete_company']) && $_POST['delete_company'] != '') { 
		$message = $new_company->delete_company($dBlink ,$_POST['delete_company']);
	}//delete account.
		
	$page_title = "Company"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
	?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft">Manage Companies</h2>
                	<?php if(partial_access($dBlink,'admin')) { ?><a href="manage_company.php" class="alignleft addnew">Add New</a><?php } ?>
                 	<div class="clear"></div><!--clear float-->
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="wc_table" width="100%">
                        <thead>
                            <tr>
                                <th>Manual ID</th>
                                <th>Company Name</th>
                                <th>Business Type</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Currency</th>
                                <th>Accounts</th>
                                <?php if(partial_access($dBlink,'admin')) { ?><th>Edit</th>
                                <th>Delete</th><?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                           <?php echo $new_company->list_companies($dBlink ); ?>
                        </tbody>
                    </table>
                 </div>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php
	require_once("includes/footer.php");
?>