<?php
	include('system_load.php');
	//This loads system.
	authenticate_user('subscriber');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Subscriber Page</title>
</head>

<body>
<!--YOU CAN PUT THIS IN ANY STYLE YOU NEED THIS GIVES LOGOUT, EDIT PROFILE LINKS-->
<div class="alignright">
            Welcom <?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?> | <a href="edit_profile.php?user_id=<?php echo $_SESSION['user_id']; ?>">Edit Profile</a> | <a href="dashboard.php?logout=1">Logout</a>
</div>
<!--END OF MANAGE PROFILE LOGOUT-->

<h1>Subscriber Page</h1>
<p>This is a default page for subscriber user level, Subscriber user level is deafault level on registration. This page is only accessable when user is signed in and his access level is subscriber.</p>
<h2>How to make more subscriber pages</h2>
<p>You can put the following code in top of any page that will become a subscriber accessable page. Note: admin can access all pages of all levels.</p>
<pre>
&lt;?php
	include('system_load.php');
	//This loads system.
	authenticate_user('subscriber');
?&gt;		
</pre>
<h2>How to make a page accessable to all loged in users.</h2>
<p>You can use the following code in start of your document that will make your page to accessable all loged in users but only when they are signed in.</p>
<pre>
&lt;?php
	include('system_load.php');
	//This loads system.
	authenticate_user('all');
?&gt;		
</pre>
<h2>Partial Access</h2>
<pre>
&lt;?php if(partial_access('admin')): ?&gt;	
	&lt;p&gt;You are admin&lt;/p&gt;
&lt;?php elseif(partial_access('subscriber')): ?&gt;
	&lt;p&gt;You are subscriber.&lt;/p&gt;
&lt;?php elseif(partial_access('all')): ?&gt;
	&lt;p&gt;You are loged in user.&lt;/p&gt;
&lt;?php else: ?&gt; 
	&lt;p&gt;You are not loged in user.&lt;/p&gt;
&lt;?php endif; ?&gt;
</pre>
<p>Working Example below</p>
<?php if(partial_access('admin')): ?>	
	<p>You are admin</p>
<?php elseif(partial_access('subscriber')): ?>
	<p>You are subscriber.</p>
<?php elseif(partial_access('all')): ?>
	<p>You are loged in user.</p>
<?php else: ?> 
	<p>You are not loged in user.</p>
<?php endif; ?>
</body>
</html>