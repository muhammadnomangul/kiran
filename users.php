<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user($dBlink,'admin');
	
	$new_user = new Users;
	
	//Delete user.
	if(isset($_POST['delete_user']) && $_POST['delete_user'] != '') { 
		$message = $new_user->delete_user($dBlink ,$_SESSION['user_type'], $_POST['delete_user']); 
	}//delete ends here.
		
	$page_title = "Administrator"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
	?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft">Manage Users</h2>
                	<a href="manage_users.php" class="alignleft addnew">Add New</a>
                 	<div class="clear"></div><!--clear float-->
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="wc_table" width="100%">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>User Type</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $new_user->list_users($dBlink ,$_SESSION['user_type']); ?>
                        </tbody>
                    </table>
                 </div>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php
	require_once("includes/footer.php");
?>