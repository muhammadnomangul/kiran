-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 10, 2019 at 10:48 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `accounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_number` varchar(200) NOT NULL,
  `account_title` varchar(200) NOT NULL,
  `account_type` varchar(200) NOT NULL,
  `memo` varchar(600) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `cnic` int(11) DEFAULT NULL,
  `contact` int(11) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_id`, `account_number`, `account_title`, `account_type`, `memo`, `company_id`, `cnic`, `contact`, `address`) VALUES
(167, 'm ', 'M/s Hudaibia papers mart ', 'Expense', 'Bux board purchased matters ', 7, NULL, 0, ''),
(166, 'm ', 'M/s Bao Bashir Bux Board ', 'Expense', 'Hard bux baord purchased billing matters ', 7, NULL, 0, ''),
(165, 'p', 'Paper & Box Board ', 'Expense', 'paper purchased matters', 7, NULL, 0, ''),
(164, 'b ', 'Binder - Faisal Shafiq ', 'Expense', 'binding matters ', 7, NULL, 0, ''),
(163, 'b ', 'Binding Charges ', 'Expense', 'Binding matters ', 7, NULL, 0, ''),
(162, 'b ', 'Binder - New Ijaz', 'Expense', 'Binding bills voucher', 7, NULL, 0, ''),
(160, 'a', 'Veh # LRR - 9684', 'Expense', 'Mileage expenses', 7, NULL, 0, ''),
(159, 'e ', 'Emp AC Tanveer Khalid', 'Expense', '35202-452145-3, Rahwali catt ,0335-4433957', 7, NULL, 0, ''),
(158, 'd', 'Discounts Kiran 2019', 'Expense', 'Discount on bonuses and others ', 7, NULL, 0, ''),
(157, 'b ', 'Binder Shahid Old', 'Expense', 'Binding books', 7, NULL, 0, ''),
(156, 'U', 'Utility Electricity 06 11142 0615201 U Godown', 'Expense', 'Electricity bills payment', 7, NULL, 0, ''),
(155, 'a', 'Emp ML M Asim khan', 'Expense', 'Marketing department', 7, NULL, 0, ''),
(154, 'a', 'Adjustment Exp', 'Expense', 'Adjustment amount', 7, NULL, 0, ''),
(153, 'e ', 'Emp Mo Naveed Iqbal Sahiwal ', 'Expense', 'Marketing employee', 7, NULL, 0, ''),
(152, 's', 'Sales Return', 'Expense', 'sales return to schools, parties ', 7, NULL, 0, ''),
(151, 'a', 'Advertisement Exp', 'Expense', 'advertisement expences', 7, NULL, 0, ''),
(150, 's', 'Emp SG Muhammad Khalil ', 'Expense', 'security', 7, NULL, 0, ''),
(149, 's', 'Emp M khalil SG', 'Expense', 'securtity', 7, NULL, 0, ''),
(148, 'b ', 'Bank UBL 52529 K P', 'Expense', 'banking transactions', 7, NULL, 0, ''),
(147, 'b ', 'Binder Ali Akbar ', 'Expense', 'Binding books ', 7, NULL, 0, ''),
(146, 'e ', 'Emp ML M Afzal ', 'Expense', 'Marketing Officer ', 7, NULL, 0, ''),
(145, 's', 'Special Incentives ', 'Expense', 'Special incentives ', 7, NULL, 0, ''),
(144, 'b ', 'Binder Mairaj Deen ', 'Expense', 'binding books & others ', 7, NULL, 0, ''),
(143, 'U', 'Utility Electricity ', 'Expense', 'Electricity billing ', 7, NULL, 0, ''),
(142, 'U', 'Utility PTCL # 042 37151652', 'Expense', 'PTCL land line billing ', 7, NULL, 0, ''),
(141, 'w', 'Watch Man', 'Expense', 'Security ', 7, NULL, 0, ''),
(140, 'b ', 'Binder Lahore book binder - Shahid Mela Raam ', 'Expense', 'binding books work', 7, NULL, 0, ''),
(139, 'R', 'Research & Development Exp ', 'Expense', 'R & D work', 7, NULL, 0, ''),
(138, 'U', 'Employees U Fone Numbers', 'Expense', 'U fone ', 7, NULL, 0, ''),
(137, 'e ', 'Emp R&D Madam Sehar Naveed Butt ', 'Expense', 'R & D Deptt', 7, NULL, 0, ''),
(136, 'e ', 'Emp PIES Akhtar Ali ', 'Expense', 'PIES department', 7, NULL, 0, ''),
(135, 'e ', 'Entertainment', 'Expense', 'All kind of refreshment', 7, NULL, 0, ''),
(134, 'e ', 'Emp IT M Waqas ', 'Expense', 'I T works', 7, NULL, 0, ''),
(133, 'e ', 'Emp SG M Akram ', 'Expense', 'Security guard', 7, NULL, 0, ''),
(132, 's', 'Sajjad Building Contractor 2019', 'Expense', 'Building contractor', 7, NULL, 0, ''),
(131, 'Sajjad Building Contractor 2019', 's', 'Expense', 'building contractor ', 7, NULL, 0, ''),
(130, 'Binder Shahid old', 'b', 'Expense', '', 7, NULL, 0, ''),
(129, 'Binder Lahore book binder - Shahid Mela Raam ', 'b', 'Expense', '', 7, NULL, 0, ''),
(128, 'Muhammad Afzal ', 'm', 'Expense', '', 7, NULL, 0, ''),
(127, 'Rehmat khan S/o Hazrat khan (Helpar)', 'r', 'Expense', '', 8, NULL, 0, ''),
(126, 'Electricity # 11144 0144700U', 'e', 'Expense', '', 7, NULL, 0, ''),
(125, 'Chief Executive Account ', 'c ', 'Expense', '', 7, NULL, 0, ''),
(124, '4 ', 'Asia khushi', 'Expense', 'Tele Marketing Department', 7, NULL, 0, ''),
(123, '3 ', 'Copy and Register ', 'Revenue', 'Cash rcvd against sales of copy/register', 7, NULL, 0, ''),
(122, '2 ', 'Receipt kiran ', 'Revenue', 'All cash receipt', 7, NULL, 0, ''),
(161, 'e ', 'Emp PD Tahir Waheed Butt ', 'Expense', 'Employee', 7, NULL, 0, ''),
(120, '3.1', 'Online Sundry', 'Revenue', 'All Online Received from Banks Accounts', 7, NULL, 0, ''),
(119, '2.10', 'Car Parking', 'Expense', 'Car Parking Expense', 7, NULL, 0, ''),
(118, '2.8', 'Kiran Expense', 'Expense', 'All Expense Approve By C/E', 7, NULL, 0, ''),
(117, '2.6', 'Charity & Donation', 'Expense', 'All Charity and Donation and Zakat Etc.', 7, NULL, 0, ''),
(116, '2.4', 'Carriage & Freight', 'Expense', 'All about Carriage ', 7, NULL, 0, ''),
(115, '2.3', 'Directors Drawings', 'Expense', 'All about Chief Executive', 7, NULL, 0, ''),
(114, '1', 'Cashier', 'Asset', 'All Cash Dealings', 8, NULL, 0, ''),
(113, '2.1', 'Vehicle LRR - 9684', 'Expense', 'Repairing and Maintainance of Office Bike', 8, NULL, 0, ''),
(112, '2.2', 'Employee Ufone', 'Expense', 'Ufone Offical Number Balance ', 7, NULL, 0, ''),
(111, '2.2', 'Zong Business SMS Service', 'Expense', 'SMS Activation ', 7, NULL, 0, ''),
(110, '2.1', 'Treveling Expense', 'Expense', 'All Conveynce ', 7, NULL, 0, ''),
(109, '2', 'Tracing & Pasting', 'Expense', 'Tracing and pasting of Books', 7, NULL, 0, ''),
(108, '1.1', 'Cash Receipt', 'Asset', 'Cash Received ', 7, NULL, 0, ''),
(107, '1', 'Cashier', 'Asset', 'Cash', 7, NULL, 0, ''),
(168, '21223', 'dfwer', 'Asset', 'dfsdf', 7, 5345345, 4345, 'sdferfwefewrf');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_manual_id` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `business_type` varchar(100) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `address2` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `company_logo` varchar(500) NOT NULL,
  `description` varchar(600) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `currency_symbol` varchar(100) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `company_manual_id`, `company_name`, `business_type`, `address1`, `address2`, `city`, `state`, `country`, `zip_code`, `phone`, `email`, `company_logo`, `description`, `user_id`, `currency_symbol`) VALUES
(11, '123', 'sd', 'software company', 'sd', 'dfv', 'ds', 'sdf', 'dsf', '000', '03354433957', 'waqas_said@yahoo.com', '', 'dsc', '1', '$'),
(9, '26', 'hydra', 'sa', 's', 's', 's', 's', 's', '54000', '03354433957', 'nomangul934@gmail.com', '', 'sxc', '1', 'dfd'),
(10, '26', 'hydra', 'sa', 's', 's', 's', 's', 's', '54000', '03354433957', 'nomangul934@gmail.com', '', 'sxc', '1', 'dfd'),
(8, '2', 'Life Guard Printers', 'Producion & Publishing', 'Animal and Science Vatrinaty University Tape Road Lahore', 'Animal and Science Vatrinaty University Tape Road Lahore', 'Lahore', 'punjab', 'Pakistan', '54000', '03354433957', 'nomangul934@gmail.com', '', '41', '1', '454'),
(7, '1', 'Kiran Publications', 'Publishing', 'Sultani Muhallah Kabeer Street Urdu Bazar Lahore', 'Sultani Muhallah Kabeer Street Urdu Bazar Lahore', 'Lahore', 'Punjab', 'Pakistan ', '54000', '03354433957', 'tanveerkhalidranjha990@gmail.com', '', 'RS', '1', 'Rs');

-- --------------------------------------------------------

--
-- Table structure for table `company_access`
--

DROP TABLE IF EXISTS `company_access`;
CREATE TABLE IF NOT EXISTS `company_access` (
  `access_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `journal_voucher`
--

DROP TABLE IF EXISTS `journal_voucher`;
CREATE TABLE IF NOT EXISTS `journal_voucher` (
  `jv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `jv_id_manual` varchar(100) DEFAULT NULL,
  `jv_title` varchar(100) DEFAULT NULL,
  `jv_description` varchar(200) DEFAULT NULL,
  `jv_type` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`jv_id`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal_voucher`
--

INSERT INTO `journal_voucher` (`jv_id`, `date`, `jv_id_manual`, `jv_title`, `jv_description`, `jv_type`, `user_id`, `company_id`) VALUES
(80, '2019-06-23', '79', '', '', 'cpv', 1, 7),
(79, '2019-06-23', '78', '', '', 'cpv', 1, 7),
(78, '2019-06-23', '77', '', '', 'cpv', 1, 7),
(77, '2019-06-23', '76', '', '', 'cpv', 1, 7),
(76, '2019-06-23', '75', '', '', 'cpv', 1, 7),
(75, '2019-06-23', '74', '', '', 'cpv', 1, 7),
(74, '2019-06-23', '73', '', '', 'cpv', 1, 7),
(73, '2019-06-23', '72', '', '', 'cpv', 1, 7),
(72, '2019-06-23', '71', '', '', 'cpv', 1, 7),
(71, '2019-06-23', '70', '', '', 'cpv', 1, 7),
(70, '2019-06-23', '69', '', '', 'cpv', 1, 7),
(69, '2019-06-23', '68', '', '', 'cpv', 1, 7),
(68, '2019-06-23', '67', '', '', 'cpv', 1, 7),
(67, '2019-06-23', '66', '', '', 'crv', 1, 7),
(66, '2019-06-23', '65', '', '', 'crv', 1, 7),
(65, '2019-06-22', '64', '', '', '', 1, 7),
(64, '2019-06-22', '63', '', '', '', 1, 7),
(63, '2019-06-22', '62', '', '', 'cpv', 1, 8),
(62, '2019-06-22', '61', '', '', 'cpv', 1, 7),
(61, '2019-06-22', '60', '', '', 'cpv', 1, 7),
(60, '2019-06-22', '59', '', '', 'cpv', 1, 7),
(59, '2019-06-22', '58', '', '', 'crv', 1, 7),
(58, '2019-06-22', '57', '', '', 'crv', 1, 7),
(57, '2019-06-22', '56', '', '', 'crv', 1, 7),
(56, '2019-06-13', '55', 'jv', 'fd', 'vpv', 1, 7),
(55, '2019-06-13', '54', '', '', 'cpv', 1, 7),
(54, '2019-06-13', '53', '', '', 'cpv', 1, 7),
(53, '2019-06-13', '52', '', '', 'cpv', 1, 7),
(52, '2019-06-13', '51', '', '', 'cpv', 1, 7),
(51, '2019-06-13', '50', '', '', 'cpv', 1, 7),
(50, '2019-06-13', '49', '', '', 'cpv', 1, 8),
(49, '2019-06-13', '48', '', '', 'cpv', 1, 7),
(48, '2019-06-13', '47', '', '', 'cpv', 1, 7),
(47, '2019-06-13', '46', '', '', 'cpv', 1, 7),
(46, '2019-06-13', '45', '', '', 'crv', 1, 7),
(45, '2019-06-13', '44', '', '', 'crv', 1, 7),
(44, '2019-06-13', '43', '', '', 'crv', 1, 7),
(43, '2019-06-13', '', '', '', 'crv', 1, 7),
(81, '2019-06-23', '80', '', '', 'cpv', 1, 7),
(82, '2019-06-23', '81', '', '', 'cpv', 1, 7),
(83, '2019-06-23', '82', '', '', 'cpv', 1, 7),
(84, '2019-06-23', '83', '', '', 'cpv', 1, 7),
(85, '2019-06-23', '84', '', '', '', 1, 7),
(86, '2019-06-23', '85', '', '', 'cpv', 1, 7),
(88, '2019-06-25', '86', '', '', 'crv', 1, 7),
(89, '2019-06-25', '88', '', '', 'crv', 1, 7),
(90, '2019-06-25', '89', '', '', 'crv', 1, 7),
(91, '2019-06-25', '90', '', '', 'cpv', 1, 7),
(100, '2019-06-26', '99', '', '', 'crv', 1, 7),
(93, '2019-06-25', '92', '', '', 'cpv', 1, 7),
(94, '2019-06-25', '93', '', '', 'cpv', 1, 7),
(95, '2019-06-25', '94', '', '', 'cpv', 1, 7),
(96, '2019-06-25', '95', '', '', 'cpv', 1, 7),
(99, '2019-06-25', '96', '', '', 'jv', 1, 7),
(101, '2019-06-26', '100', '', '', 'crv', 1, 7),
(102, '2019-06-26', '101', '', '', 'crv', 1, 7),
(103, '2019-06-26', '102', '', '', 'cpv', 1, 7),
(104, '2019-06-26', '103', '', '', 'cpv', 1, 7),
(105, '2019-06-26', '104', '', '', 'cpv', 1, 7),
(106, '2019-06-26', '105', '', '', 'cpv', 1, 7),
(107, '2019-06-26', '106', '', '', 'cpv', 1, 7),
(108, '2019-06-26', '107', '', '', 'jv', 1, 7),
(110, '2019-06-27', '108', '', '', 'crv', 1, 7),
(111, '2019-06-27', '110', '', '', 'crv', 1, 7),
(112, '2019-06-27', '111', '', '', 'cpv', 1, 7),
(113, '2019-06-27', '112', '', '', 'cpv', 1, 7),
(114, '2019-06-27', '113', '', '', 'cpv', 1, 7),
(115, '2019-06-27', '114', '', '', 'jv', 1, 7),
(116, '2019-06-28', '115', '', '', 'crv', 3, 7),
(117, '2019-06-28', '116', '', '', 'crv', 3, 7),
(118, '2019-06-28', '117', '', '', 'cpv', 3, 7),
(119, '2019-06-28', '118', '', '', 'cpv', 3, 7),
(120, '2019-06-28', '119', '', '', 'cpv', 3, 7),
(121, '2019-06-28', '120', '', '', 'jv', 3, 7),
(122, '2019-06-28', '121', '', '', 'jv', 3, 7),
(128, '2019-06-28', '127', '', '', 'jv', 3, 7),
(124, '2019-06-28', '123', '', '', 'jv', 3, 7),
(125, '2019-06-28', '124', '', '', 'jv', 3, 7),
(127, '2019-06-28', '126', '', '', 'jv', 3, 7),
(129, '2019-06-28', '128', '', '', 'jv', 3, 7),
(130, '2019-06-28', '129', '', '', 'jv', 3, 7),
(131, '2019-06-28', '130', '', '', 'jv', 3, 7),
(132, '2019-06-28', '131', '', '', 'jv', 3, 7),
(133, '2019-06-28', '132', '', '', 'jv', 3, 7),
(134, '2019-06-28', '133', '', '', 'jv', 3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(500) NOT NULL,
  `option_value` varchar(500) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`option_id`, `option_name`, `option_value`) VALUES
(1, 'site_url', 'http://127.0.0.1/webful-general-ledger-accounting/script/'),
(2, 'site_name', 'accounts'),
(3, 'email_from', 'nomangul934@gmail.com'),
(4, 'email_to', 'nomangul934@gmail.com'),
(5, 'installation', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `tr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jv_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `memo` varchar(400) DEFAULT NULL,
  `debit` decimal(10,2) DEFAULT NULL,
  `credit` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`tr_id`)
) ENGINE=MyISAM AUTO_INCREMENT=268 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`tr_id`, `jv_id`, `account_id`, `date`, `memo`, `debit`, `credit`) VALUES
(125, 63, 114, '2019-06-22', 'Adv/salary m/o june-19', '0.00', '-2090.00'),
(124, 63, 127, '2019-06-22', 'Adv/salary m/o june-19', '2090.00', '0.00'),
(123, 62, 107, '2019-06-22', 'Paid electricity bill M/o june - 19', '0.00', '-863.00'),
(122, 62, 126, '2019-06-22', 'Paid electricity bill M/o june - 19', '863.00', '0.00'),
(121, 61, 107, '2019-06-22', 'Paid to C/e', '0.00', '-13000.00'),
(120, 61, 125, '2019-06-22', 'Paid to C/e', '13000.00', '0.00'),
(119, 60, 107, '2019-06-22', 'Adv/salary M/o june-19', '0.00', '-5000.00'),
(118, 60, 124, '2019-06-22', 'Adv/salary M/o june-19', '5000.00', '0.00'),
(117, 59, 123, '2019-06-22', 'Cash rcvd against sales of copy/register', '0.00', '-3450.00'),
(116, 59, 107, '2019-06-22', 'Cash rcvd against sales of copy/register', '3450.00', '0.00'),
(115, 58, 122, '2019-06-22', 'Cash rcvd from NSP ', '0.00', '-4375.00'),
(114, 58, 107, '2019-06-22', 'Cash rcvd from NSP ', '4375.00', '0.00'),
(113, 57, 122, '2019-06-22', 'Cash rcvd from NSP \r\n\r\n', '0.00', '-28785.00'),
(112, 57, 107, '2019-06-22', 'Cash rcvd from NSP \r\n\r\n', '28785.00', '0.00'),
(111, 56, 121, '2019-06-13', 'Online Received on dt. 29.05.19 from Naveed Iqbal Sahiwal', '0.00', '-8270.00'),
(110, 56, 120, '2019-06-13', 'Online Received on dt. 29.05.19 from Naveed Iqbal Sahiwal', '8270.00', '0.00'),
(109, 55, 107, '2019-06-13', 'Paid for C/E Car Paking Monthly Expense', '0.00', '-1200.00'),
(108, 55, 119, '2019-06-13', 'Paid for C/E Car Paking Monthly Expense', '1200.00', '0.00'),
(107, 54, 107, '2019-06-13', 'Paid to Police Man at NSP', '0.00', '-3000.00'),
(106, 54, 118, '2019-06-13', 'Paid to Police Man at NSP', '3000.00', '0.00'),
(105, 53, 107, '2019-06-13', 'Paid Charity at NSP', '0.00', '-1000.00'),
(104, 53, 117, '2019-06-13', 'Paid Charity at NSP', '1000.00', '0.00'),
(103, 52, 107, '2019-06-13', 'Paid Carriage for different LND Kitabcha as GP List Attached', '0.00', '-500.00'),
(102, 52, 116, '2019-06-13', 'Paid Carriage for different LND Kitabcha as GP List Attached', '500.00', '0.00'),
(101, 51, 107, '2019-06-13', 'Purchase 9 Cold Drink for C/E', '0.00', '-198.00'),
(100, 51, 115, '2019-06-13', 'Purchase 9 Cold Drink for C/E', '198.00', '0.00'),
(99, 50, 114, '2019-06-13', 'Paid to Mr. Ahmad Faraz as fuel exp for official working of different days as per detailed attached', '0.00', '-300.00'),
(98, 50, 113, '2019-06-13', 'Paid to Mr. Ahmad Faraz as fuel exp for official working of different days as per detailed attached', '300.00', '0.00'),
(97, 49, 107, '2019-06-13', 'Paid for Ufone Balace of diffrent employees as detailed attached', '0.00', '-6000.00'),
(96, 49, 112, '2019-06-13', 'Paid for Ufone Balace of diffrent employees as detailed attached', '6000.00', '0.00'),
(95, 48, 107, '2019-06-13', 'Paid for Zong Businees SMS Service No 3120436162', '0.00', '-2000.00'),
(94, 48, 111, '2019-06-13', 'Paid for Zong Businees SMS Service No 3120436162', '2000.00', '0.00'),
(93, 47, 107, '2019-06-13', 'Paid to Mr. Javed for Official to Godown', '0.00', '-60.00'),
(92, 47, 110, '2019-06-13', 'Paid to Mr. Javed for Official to Godown', '60.00', '0.00'),
(91, 46, 108, '2019-06-13', 'Cash Received from Copy and Register ', '0.00', '0.00'),
(90, 46, 107, '2019-06-13', 'Cash Received from Copy and Register ', '0.00', '0.00'),
(89, 45, 108, '2019-06-13', 'Cash Received from Account and Parties ', '0.00', '-1032340.00'),
(88, 45, 107, '2019-06-13', 'Cash Received from Account and Parties ', '1032340.00', '0.00'),
(87, 44, 108, '2019-06-13', 'Cash Received from Nsp ', '0.00', '-46045.00'),
(86, 44, 107, '2019-06-13', 'Cash Received from Nsp ', '46045.00', '0.00'),
(85, 43, 108, '2019-06-13', 'Cash Received from Venus ', '0.00', '0.00'),
(84, 43, 107, '2019-06-13', 'Cash Received from Venus ', '0.00', '0.00'),
(126, 64, 128, '2019-06-22', 'Special incentives ', '2000.00', '0.00'),
(127, 64, 122, '2019-06-22', 'Special incentives ', '0.00', '-2000.00'),
(128, 65, 129, '2019-06-22', 'Bill of binding P/o 121', '14914.00', '0.00'),
(129, 65, 130, '2019-06-22', 'Bill of binding P/o 121', '0.00', '-14914.00'),
(130, 66, 107, '2019-06-23', 'cash rcvd from Nsp ', '22570.00', '0.00'),
(131, 66, 122, '2019-06-23', 'cash rcvd from Nsp ', '0.00', '-22570.00'),
(132, 67, 107, '2019-06-23', 'cash rcvd from parties ', '13620.00', '0.00'),
(133, 67, 122, '2019-06-23', 'cash rcvd from parties ', '0.00', '-13620.00'),
(134, 68, 132, '2019-06-23', 'Advance paid to Mr Sajjad ', '10000.00', '0.00'),
(135, 68, 107, '2019-06-23', 'Advance paid to Mr Sajjad ', '0.00', '-10000.00'),
(136, 69, 133, '2019-06-23', 'Adv/salary m/o june-19', '3000.00', '0.00'),
(137, 69, 107, '2019-06-23', 'Adv/salary m/o june-19', '0.00', '-3000.00'),
(138, 70, 134, '2019-06-23', 'Adv/salary m/o june-19', '3000.00', '0.00'),
(139, 70, 107, '2019-06-23', 'Adv/salary m/o june-19', '0.00', '-3000.00'),
(140, 71, 135, '2019-06-23', 'Paid to Mr iqbal ', '286.00', '0.00'),
(141, 71, 107, '2019-06-23', 'Paid to Mr iqbal ', '0.00', '-286.00'),
(142, 72, 136, '2019-06-23', 'Adv/salary M/o June-19', '10000.00', '0.00'),
(143, 72, 107, '2019-06-23', 'Adv/salary M/o June-19', '0.00', '-10000.00'),
(144, 73, 137, '2019-06-23', 'Adv/salary M/o June-19', '10000.00', '0.00'),
(145, 73, 107, '2019-06-23', 'Adv/salary M/o June-19', '0.00', '-10000.00'),
(146, 74, 135, '2019-06-23', 'Paid for one juice \r\n', '50.00', '0.00'),
(147, 74, 107, '2019-06-23', 'Paid for one juice \r\n', '0.00', '-50.00'),
(148, 75, 138, '2019-06-23', 'Paid for #960,931', '1200.00', '0.00'),
(149, 75, 107, '2019-06-23', 'Paid for #960,931', '0.00', '-1200.00'),
(150, 76, 139, '2019-06-23', 'Purchased computer book GP#8298', '915.00', '0.00'),
(151, 76, 107, '2019-06-23', 'Purchased computer book GP#\r\n8298', '0.00', '-915.00'),
(152, 77, 140, '2019-06-23', 'advance paid ', '15000.00', '0.00'),
(153, 77, 107, '2019-06-23', 'advance paid ', '0.00', '-15000.00'),
(154, 78, 116, '2019-06-23', 'paid carriage for GP#8618\r\n', '1560.00', '0.00'),
(155, 78, 107, '2019-06-23', 'paid carriage for GP#8618\r\n', '0.00', '-1560.00'),
(156, 79, 141, '2019-06-23', 'paid to Tariq ', '200.00', '0.00'),
(157, 79, 107, '2019-06-23', 'paid to Tariq ', '0.00', '-200.00'),
(158, 80, 116, '2019-06-23', 'Paid carriage for GP#10411\r\n', '1900.00', '0.00'),
(159, 80, 107, '2019-06-23', 'Paid carriage for GP#10411\r\n', '0.00', '-1900.00'),
(160, 81, 118, '2019-06-23', 'Paid eidi to SG \r\n', '2000.00', '0.00'),
(161, 81, 107, '2019-06-23', 'Paid eidi to SG \r\n', '0.00', '-2000.00'),
(162, 82, 142, '2019-06-23', 'Paid PTCL bill', '710.00', '0.00'),
(163, 82, 107, '2019-06-23', 'Paid PTCL bill', '0.00', '-710.00'),
(164, 83, 143, '2019-06-23', 'paid electricity bill ', '13372.00', '0.00'),
(165, 83, 107, '2019-06-23', 'paid electricity bill ', '0.00', '-13372.00'),
(166, 84, 144, '2019-06-23', 'Paid ledger balance ', '36000.00', '0.00'),
(167, 84, 107, '2019-06-23', 'Paid ledger balance ', '0.00', '-36000.00'),
(168, 85, 145, '2019-06-23', 'special incentives to Mr Mfzal ', '19150.00', '0.00'),
(169, 85, 146, '2019-06-23', 'special incentives to Mr Mfzal ', '0.00', '-19150.00'),
(170, 86, 147, '2019-06-23', 'Paid ledger balance ', '28030.00', '0.00'),
(171, 86, 107, '2019-06-23', 'Paid ledger balance ', '0.00', '-28030.00'),
(175, 88, 148, '2019-06-25', 'Chq # 86725206 dt 25.06.19 withdrawl', '0.00', '-950000.00'),
(174, 88, 107, '2019-06-25', 'Chq # 86725206 dt 25.06.19 withdrawl', '950000.00', '0.00'),
(176, 89, 107, '2019-06-25', 'cash rcvd from NSP', '11075.00', '0.00'),
(177, 89, 122, '2019-06-25', 'cash rcvd from NSP', '0.00', '-11075.00'),
(178, 90, 107, '2019-06-25', 'cash rcvd from NSP ', '31520.00', '0.00'),
(179, 90, 122, '2019-06-25', 'cash rcvd from NSP ', '0.00', '-31520.00'),
(180, 91, 115, '2019-06-25', 'Purchased 10 milk packs ', '300.00', '0.00'),
(181, 91, 107, '2019-06-25', 'Purchased 10 milk packs ', '0.00', '-300.00'),
(198, 100, 107, '2019-06-26', 'Chq # 86725207 dt 26.06.19 withdrawl\r\n', '950000.00', '0.00'),
(184, 93, 149, '2019-06-25', 'Adv/salary M/o june-19\r\n', '20000.00', '0.00'),
(185, 93, 107, '2019-06-25', 'Adv/salary M/o june-19\r\n', '0.00', '-20000.00'),
(186, 94, 151, '2019-06-25', 'Paid ad exp', '4300.00', '0.00'),
(187, 94, 107, '2019-06-25', 'Paid ad exp', '0.00', '-4300.00'),
(188, 95, 152, '2019-06-25', 'sales return to Mr M Ajmal', '11500.00', '0.00'),
(189, 95, 107, '2019-06-25', 'sales return to Mr M Ajmal', '0.00', '-11500.00'),
(190, 96, 153, '2019-06-25', 'paid ledger balance ', '13000.00', '0.00'),
(191, 96, 107, '2019-06-25', 'paid ledger balance ', '0.00', '-13000.00'),
(197, 99, 154, '2019-06-25', 'AMOUNT ADJUSTED', '0.00', '-725.00'),
(196, 99, 153, '2019-06-25', 'AMOUNT ADJUSTED', '725.00', '0.00'),
(199, 100, 148, '2019-06-26', 'Chq # 86725207 dt 26.06.19 withdrawl\r\n', '0.00', '-950000.00'),
(200, 101, 107, '2019-06-26', 'Cash rcvd from NSP ', '23140.00', '0.00'),
(201, 101, 122, '2019-06-26', 'Cash rcvd from NSP ', '0.00', '-23140.00'),
(202, 102, 107, '2019-06-26', 'Cash rcvd from NSP ', '15000.00', '0.00'),
(203, 102, 122, '2019-06-26', 'Cash rcvd from NSP ', '0.00', '-15000.00'),
(204, 103, 155, '2019-06-26', 'Adv/salary ', '5000.00', '0.00'),
(205, 103, 107, '2019-06-26', 'Adv/salary ', '0.00', '-5000.00'),
(206, 104, 135, '2019-06-26', 'Paid for 3 dahi bhallay for C/E office', '210.00', '0.00'),
(207, 104, 107, '2019-06-26', 'Paid for 3 dahi bhallay for C/E office', '0.00', '-210.00'),
(208, 105, 146, '2019-06-26', 'Adv/salary ', '10000.00', '0.00'),
(209, 105, 107, '2019-06-26', 'Adv/salary ', '0.00', '-10000.00'),
(210, 106, 156, '2019-06-26', 'Paid electricity bill ', '7267.00', '0.00'),
(211, 106, 107, '2019-06-26', 'Paid electricity bill ', '0.00', '-7267.00'),
(212, 107, 157, '2019-06-26', 'Paid ledger balance ', '5400.00', '0.00'),
(213, 107, 107, '2019-06-26', 'Paid ledger balance ', '0.00', '-5400.00'),
(214, 108, 157, '2019-06-26', 'Amount adjusted', '85.00', '0.00'),
(215, 108, 107, '2019-06-26', 'Amount adjusted', '0.00', '-85.00'),
(218, 110, 107, '2019-06-27', 'cash rcvd from nsp', '12575.00', '0.00'),
(219, 110, 122, '2019-06-27', 'cash rcvd from nsp', '0.00', '-12575.00'),
(220, 111, 107, '2019-06-27', 'cash rcvd from nsp', '6530.00', '0.00'),
(221, 111, 122, '2019-06-27', 'cash rcvd from nsp', '0.00', '-6530.00'),
(222, 112, 116, '2019-06-27', 'paid carriage', '2720.00', '0.00'),
(223, 112, 107, '2019-06-27', 'paid carriage', '0.00', '-2720.00'),
(224, 113, 135, '2019-06-27', 'paid refreshment charges \r\n', '120.00', '0.00'),
(225, 113, 107, '2019-06-27', 'paid refreshment charges \r\n', '0.00', '-120.00'),
(226, 114, 159, '2019-06-27', 'Adv/salary', '1500.00', '0.00'),
(227, 114, 107, '2019-06-27', 'Adv/salary', '0.00', '-1500.00'),
(228, 115, 158, '2019-06-27', 'Dis B#295006 25.05.19\r\nyameen BD mandi bahaudin', '1638.00', '0.00'),
(229, 115, 122, '2019-06-27', 'Dis B#295006 25.05.19\r\nyameen BD mandi bahaudin', '0.00', '-1638.00'),
(230, 116, 107, '2019-06-28', 'Cash rcvd from NSP', '12500.00', '0.00'),
(231, 116, 122, '2019-06-28', 'Cash rcvd from NSP', '0.00', '-12500.00'),
(232, 117, 107, '2019-06-28', 'Cash rcvd from NSP', '32510.00', '0.00'),
(233, 117, 122, '2019-06-28', 'Cash rcvd from NSP', '0.00', '-32510.00'),
(234, 118, 160, '2019-06-28', 'Paid fuel expnces', '560.00', '0.00'),
(235, 118, 107, '2019-06-28', 'Paid fuel expnces', '0.00', '-560.00'),
(236, 119, 115, '2019-06-28', 'Paid for C/E handsfree', '250.00', '0.00'),
(237, 119, 107, '2019-06-28', 'Paid for C/E handsfree', '0.00', '-250.00'),
(238, 120, 161, '2019-06-28', 'Adv/salary M/oJune-19\r\n', '50000.00', '0.00'),
(239, 120, 107, '2019-06-28', 'Adv/salary M/oJune-19\r\n', '0.00', '-50000.00'),
(240, 121, 162, '2019-06-28', 'Reversal bills posted', '329427.00', '0.00'),
(241, 121, 163, '2019-06-28', 'Reversal bills posted', '0.00', '-329427.00'),
(242, 122, 162, '2019-06-28', 'Bill of binding of Physics practical Note Book U/M 9th  P/O # 11B/17 qty 15740 @9', '141660.00', '0.00'),
(243, 122, 163, '2019-06-28', 'Bill of binding of Physics practical Note Book U/M 9th  P/O # 11B/17 qty 15740 @9', '0.00', '-141660.00'),
(254, 128, 163, '2019-06-28', 'Bill of binding of Chem Practical Note book U/M 9th P/o # 14B/17 qty 15930 @ 16 now rectifed', '254880.00', '0.00'),
(246, 124, 162, '2019-06-28', 'Bill of binding of Chem Practical Note book U/M 9th P/o#14b/17 qty 15930@9', '143370.00', '0.00'),
(247, 124, 163, '2019-06-28', 'Bill of binding of Chem Practical Note book U/M 9th P/o#14b/17 qty 15930@9', '0.00', '-143370.00'),
(248, 125, 162, '2019-06-28', 'Bill of binding of Chem Practical copy note Book E/M 10th P/o#5b/17 qty 4933@9\r\n', '44397.00', '0.00'),
(249, 125, 163, '2019-06-28', 'Bill of binding of Chem Practical copy note Book E/M 10th P/o#5b/17 qty 4933@9\r\n', '0.00', '-44397.00'),
(253, 127, 162, '2019-06-28', 'Bill of binding of Phy Note book U/M 9th P/o # 11B/17 qty 15740 @ 16 now rectified', '0.00', '-251840.00'),
(252, 127, 162, '2019-06-28', 'Bill of binding of Phy Note book U/M 9th P/o # 11B/17 qty 15740 @ 16 now rectified', '251840.00', '0.00'),
(255, 128, 162, '2019-06-28', 'Bill of binding of Chem Practical Note book U/M 9th P/o # 14B/17 qty 15930 @ 16 now rectifed', '0.00', '-254880.00'),
(256, 129, 163, '2019-06-28', 'Bill of binding fo Chem Practical note book U/M 10th p/o#5b/17 qty 4933 @ 16 now rectified', '78928.00', '0.00'),
(257, 129, 162, '2019-06-28', 'Bill of binding fo Chem Practical note book U/M 10th p/o#5b/17 qty 4933 @ 16 now rectified', '0.00', '-78928.00'),
(258, 130, 163, '2019-06-28', 'Bill of binding of cent percent Chem 10th qty 9875 @ 2.925', '28884.00', '0.00'),
(259, 130, 164, '2019-06-28', 'Bill of binding of cent percent Chem 10th qty 9875 @ 2.925', '0.00', '-28884.00'),
(260, 131, 163, '2019-06-28', 'Bill of binding of cent percent Maths sci 10th qty 2880 @ 3.6', '10368.00', '0.00'),
(261, 131, 164, '2019-06-28', 'Bill of binding of cent percent Maths sci 10th qty 2880 @ 3.6', '0.00', '-10368.00'),
(262, 132, 163, '2019-06-28', 'Bill of binding of cent percent Home 10th qty 2850 @ 1.8', '5130.00', '0.00'),
(263, 132, 164, '2019-06-28', 'Bill of binding of cent percent Home 10th qty 2850 @ 1.8', '0.00', '-5130.00'),
(264, 133, 165, '2019-06-28', 'Bill of 25 pkts of Bux board @ 935 GP#8134', '23700.00', '0.00'),
(265, 133, 166, '2019-06-28', 'Bill of 25 pkts of Bux board @ 935 GP#8134', '0.00', '-23700.00'),
(266, 134, 165, '2019-06-28', 'Bill of 165 reams of Art paper @ 4135 against Inv#7059 dt 11.04.19', '68600.00', '0.00'),
(267, 134, 167, '2019-06-28', 'Bill of 165 reams of Art paper @ 4135 against Inv#7059 dt 11.04.19', '0.00', '-68600.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `date_of_birth` date NOT NULL,
  `address1` varchar(200) NOT NULL,
  `address2` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `profile_image` varchar(500) NOT NULL,
  `description` varchar(600) NOT NULL,
  `status` varchar(100) NOT NULL,
  `activation_key` varchar(100) NOT NULL,
  `date_register` date NOT NULL,
  `user_type` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `date_of_birth`, `address1`, `address2`, `city`, `state`, `country`, `zip_code`, `mobile`, `phone`, `email`, `password`, `profile_image`, `description`, `status`, `activation_key`, `date_register`, `user_type`) VALUES
(1, 'noman', 'gul', '2018-09-03', 'fsrrf', 'vdfv', 'rfrf', 'vdfv', 'rdver', 'vdff', '23234234', '3343', 'nomangul934@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', 'ferfer', 'activate', '', '2018-09-26', 'admin'),
(2, 'noman', 'gul', '1994-11-29', 'dnwekn', 'enwinf', 'inw', 'nwion', 'inw', '12', '232332', '233', 'nomangul934@live.com', '827ccb0eea8a706c4c34a16891f84e7b', 'upload/180834a25fc8a414Bread_in_the_Hellenic_Republic.jpg', 'sdncsjdkc', 'activate', '', '2018-09-30', 'admin'),
(3, 'faraz', 'waheed', '1994-03-03', 'urdu', 'urdu', 'lahore', 'punjab', 'pakistan', '54000', '03218857882', '0218354', 'farazwaheed123@gmail.com', '00a85d88d1f4896c330e3b3833c1ed57', 'upload/bfc4ed6e97497ff3180834a25fc8a414Bread_in_the_Hellenic_Republic.jpg', 'sdbijfebhc', 'activate', '', '2019-06-14', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

DROP TABLE IF EXISTS `user_level`;
CREATE TABLE IF NOT EXISTS `user_level` (
  `level_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(200) NOT NULL,
  `level_description` varchar(600) NOT NULL,
  `level_page` varchar(100) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
