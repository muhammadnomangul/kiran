<?php
/**
 * Created by PhpStorm.
 * User: TechEsthete
 * Date: 9/29/2018
 * Time: 10:08 PM
 */
include('system_load.php');
if(isset($_POST['add_user'])) {
    $add_user = $_POST['add_user'];
    if($add_user == 1){
        extract($_POST);
        if($first_name == '') {
            $message = 'First name is required!';
        } else if($email == '') {
            $message = 'Email is required!';
        } else if($password == ''){
            $message = 'Password Cannot be empty!';
        } else if($_POST['privacy_policy'] != '1') {
            $message = 'Please accept our Privacy Policy!';
        } else {
            $newObj=new Users;
            $message = $newObj->register_user($dBlink,$first_name, $last_name, $email, $password);
            echo $message;

//            HEADER('LOCATION: register.php?message='.$message);
        }//validation ends here.
    }//form processing ends here.
}//isset user register add user.
