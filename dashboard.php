<?php
	include('system_load.php');
	//Including this file we load system.
	
	/*
	Logout function if called.
	*/
	if(isset($_GET['logout']) && $_GET['logout'] == 1) { 
		session_destroy();
		HEADER('LOCATION: index.php');
	} //Logout done.
	
	//user Authentication.
	authenticate_user($dBlink,'all');
	
	$new_user = new Users;//New user object.
	$new_level = new Userlevel;
	$new_company = new Company;
	//company ends here.
	$page_title = "Dashboard"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
	?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
           
                <div class="alignleft rightcontent">
                    <h2 class="alignleft">System Information</h2>
                        <div class="clear"></div><!--clear float-->
                        <?php if(partial_access($dBlink,'admin')) { ?>
                        <div class="info_box userinfo alignleft">
                        	<h3>Users info</h3>
                            <hr />
                            <strong>Total Users:</strong> <?php $new_user->get_total_users($dBlink ,'all');?> <br />
                            <strong>Active Users:</strong> <?php $new_user->get_total_users($dBlink ,'activate');?> <br />
                            <strong>Deactive Users:</strong> <?php $new_user->get_total_users($dBlink ,'deactivate');?> <br />
                            <strong>Ban Users:</strong> <?php $new_user->get_total_users($dBlink ,'ban');?> <br />
                            <strong>Suspend Users:</strong> <?php $new_user->get_total_users($dBlink ,'suspend');?> <br />
                            <p>You can manage users by going to users management <a href="users.php">Manage Users</a></p>
                        </div><!--users info ends here.-->
                        <?php } ?>

                        <?php $new_company->dashboard_companies($dBlink ); ?>
                        
                </div><!--rightcontent ends here.-->
            </div><!--admin wrap ends here.-->
                        
<?php
	require_once("includes/footer.php");
?>