<?php
	include('system_load.php');
	//This loads system.
	
	//user Authentication.
	authenticate_user($dBlink,'admin');
	//creating company object.
	$new_company = new Company;
	//creating user object.
	$new_user = new Users;
	//new user access company object. 
	$new_company_access = new CompanyAccess;
	//add access
	if(isset($_POST['user_id']) && isset($_POST['company_id'])) { 
		if($_POST['user_id'] == '' && $_POST['company_id'] == '') { 
			$message = 'Company id and user id required. Please select.';
		} else { 
			$message =  $new_company_access->add_company_access($dBlink ,$_POST['user_id'], $_POST['company_id']);
		}
	}//add company access ends here.
	//delete access
	if(isset($_POST['delete_access']) && $_POST['delete_access'] != '') { 
		$message = $new_company_access->delete_access($dBlink ,$_POST['delete_access']);
	}
	//delete access ends here.	
	$page_title = "Company"; //You can edit this to change your page title.
	require_once("includes/header.php"); //including header file.
	?>
			<div class="admin_wrap">
            	<?php require_once('includes/sidebar.php'); ?>
                <div class="alignleft rightcontent">
                	<?php
					//display message if exist.
						if(isset($message) && $message != '') { 
							echo '<div class="alert-box">';
							echo $message;
							echo '</div>';
						}
					?>
                	<h2 class="alignleft">Manage Companies Access</h2>
                    <div class="clear"></div><!--clear float-->
                    <h3>Grand Access</h3>
                    <form name="grand_access" id="grand_access" action="" method="post">
                    <table cellpadding="10" border="0">
                    	<tr>
                        	<th>Select User</th>
                            <th>Select Company</th>
                        </tr>
                        <tr>
                        	<td>
                            	<select name="user_id" required="required">
                                	<option value="">Select User</option>
                                    <?php $new_user->subscriber_options($dBlink ); ?>
                                </select>
                            </td>
                            <td>
                            	<select name="company_id" required="required">
                                	<option value="">Select Company</option>
                                    <?php $new_company->company_options($dBlink ); ?>
                                </select>
                            </td>
                            <tr>
                            	<td><input type="submit" value="Grant Access" /></td>
                                <td>&nbsp;</td>
                            </tr>
                        </tr>
                    </table>
                    </form>
                    <br />
					<br />
					<table cellpadding="0" cellspacing="0" border="0" class="display" id="wc_table" width="100%">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Company Access</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $new_company_access->list_company_access($dBlink ); ?>
                        </tbody>
                    </table>
                 </div>
                  <script type="text/javascript">
						$(document).ready(function() {
						// validate the register form
					$("#grand_access").validate();
						});
                    </script>
                <div class="clear"></div><!--clear Float-->
            </div><!--admin wrap ends here.-->
                        
<?php
	require_once("includes/footer.php");
?>