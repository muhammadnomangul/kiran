<?php
	include('system_load.php');
	//This loads system.	
//	global $dBlink;
	$new_user = new Users; //creating user object.
	
	//Activation of user confirm email id
	if(isset($_GET['confirmation_code']) && $_GET['confirmation_code'] != '' && $_GET['user_id'] != '') {
		$confirmation_code = $_GET['confirmation_code'];
		$user_id = $_GET['user_id'];
		$message = $new_user->match_confirm_code($dBlink ,$confirmation_code,$user_id);
	}
	
	if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') { 
		HEADER('LOCATION: dashboard.php');
	} //If user is loged in redirect to specific page.
	
	if(isset($_POST['login']) && $_POST['login']==1) { 
		extract($_POST);
		if($email == '') { 
			$message = 'Email cannot be empty!';
		} else if($password == '') { 
			$message = 'Password cannot be empty!';
		}//validation ends here.
		
		$message = $new_user->login_user($dBlink,$email, $password);
		if($message == 1) { 
			$_SESSION['user_id'] = $new_user->user_id;
			$_SESSION['first_name'] = $new_user->first_name;
			$_SESSION['last_name'] = $new_user->last_name;
			$_SESSION['email'] = $new_user->email;
			$_SESSION['status'] = $new_user->status;
			$_SESSION['user_type'] = $new_user->user_type;
			$message = 'Login successful!';
			redirect_user($dBlink,$new_user->user_type); //Checks authentication and redirect user as per his/her level.
			
		}//setting session variables if user loged in successful!
	}//login process ends here if form submits
	
	$page_title = "Login Page!"; //You can edit this to change your page title.
	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<style type="text/css" title="currentStyle">
	@import "css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#wc_table').dataTable();
	} );
	function confirm_delete() { 
		var del = confirm('Do you really want to delete this record?');
		if(del == true) { 
			return true;
		} else { 
			return false;
		}
	}//delete_confirmation ends here.
</script>
</head>
<body>
    
    <?php
		if(isset($message) && $message != '') { 
			echo '<div class="alert-box">';
			echo $message;
			echo '</div>';
		}
		if(isset($_GET['message']) && $_GET['message'] != '') { 
			echo '<div class="alert-box">';
			echo $_GET['message'];
			echo '</div>';
		}
	?>
    
    	<div class="form_wrapper">
        	<h1><?php echo $page_title; ?></h1>
            <form action="<?php $_SERVER['PHP_SELF']?>" id="login_form" name="login" method="post">
            <table cellpadding="10" width="100%" cellspacing="0" border="0">
                <tr>
                    <td><input type="text" name="email" placeholder="Email*" required="required" /></td>
                </tr>
                <tr>
                    <td><input type="password" name="password" placeholder="Password*" required="required"/></td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="keep_login" /> Keep me sign in</td>
                </tr>
	             <input type="hidden" value="1" name="login" />
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
   
            </table>
            </form>
            <script>
				$(document).ready(function() {
					// validate the register form
					$("#login_form").validate();
				});
            </script>

        </div><!--//form_wrapper-->
        <div class="aligncenter">
            Forgot Password? <a href="forgot.php">Recover password</a><br />
        	Not a member yet? <a href="register.php">Sign up</a>
        </div>
</div><!--//wc_wrapper--> 
</body>
</html>