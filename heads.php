<?php
include('system_load.php');
//This loads system.

//user Authentication.
authenticate_user($dBlink,'subscriber');
//creating company object.
$new_company = new Company;
//new account object/
$new_account = new Account;

if(isset($_POST['company_id']) && $_POST['company_id'] != '') {
    $_SESSION['company_id'] = $_POST['company_id'];
}

if(isset($_SESSION['company_id']) && $_SESSION['company_id'] != '') {
    $new_company->set_company($dBlink ,$_SESSION['company_id']);
    $_SESSION['company_name'] = $new_company->company_name;
    $_SESSION['currency'] = $new_company->currency_symbol;
} else {
    HEADER('LOCATION: company.php?message=Please select a company.');
}
//delete account if exist.
if(isset($_POST['delete_account']) && $_POST['delete_account'] != '') {
    $message = $new_account->delete_account($dBlink ,$_POST['delete_account']);
}//delete account.

$page_title = $_SESSION['company_name']; //You can edit this to change your page title.
require_once("includes/header.php"); //including header file.
?>
    <div class="admin_wrap">
        <?php require_once('includes/sidebar.php'); ?>
        <div class="alignleft rightcontent">
            <?php
            if(isset($_GET['message']) && $_GET['message'] != '') {
                echo '<div class="alert-box">';
                echo $_GET['message'];
                echo '</div>';
            }
            //display message if exist.
            if(isset($message) && $message != '') {
                echo '<div class="alert-box">';
                echo $message;
                echo '</div>';
            }
            ?>
            <h2 class="alignleft">Manage Accounts heads</h2>
            <a href="manage_accounts.php" class="alignleft addnew">Add New</a>
            <div class="clear"></div><!--clear float-->
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="wc_table" width="100%">
                <thead>
                <tr>
                    <th>Account ID</th>
                    <th>Account Number</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Balance</th>
                    <th>Ledger</th>
                    <?php if(partial_access($dBlink,'admin')) { ?><th>Edit</th>
                        <th>Delete</th> <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php $new_account->list_accounts($dBlink ); ?>
                </tbody>
            </table>
        </div>
        <div class="clear"></div><!--clear Float-->
    </div><!--admin wrap ends here.-->

<?php
require_once("includes/footer.php");
?>